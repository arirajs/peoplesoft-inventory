//
//  PS_CART_COUNT_D_HIST+CoreDataProperties.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 1/27/21.
//  Copyright © 2021 Ariraj Shanmugavelu. All rights reserved.
//
//

import Foundation
import CoreData


extension PS_CART_COUNT_D_HIST {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PS_CART_COUNT_D_HIST> {
        return NSFetchRequest<PS_CART_COUNT_D_HIST>(entityName: "PS_CART_COUNT_D_HIST")
    }

    @NSManaged public var business_unit: String?
    @NSManaged public var last_dttm_update: String?
    @NSManaged public var inv_cart_id: String?
    @NSManaged public var last_oprid: String?
    @NSManaged public var inv_item_id: String?
    @NSManaged public var compartment: String?
    @NSManaged public var days_old: Int16

}
