
//  cartAudit.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 5/18/20.
//  Copyright © 2020 Ariraj Shanmugavelu. All rights reserved.
//


import Foundation
import UIKit

struct CartAuditModel {
    
    var businessUnit:String?
    var inv_cart_id:String?
    var last_oprid:String?
    var last_dttm_update:String?
    var cartcount:String?
    var synctops:String?
    /*
    init(businessUnit:String,inv_cart_id:String,last_oprid:String,last_dttm_update:String,cartcount:String,synctops:String) {
        self.businessUnit = businessUnit
        self.inv_cart_id = inv_cart_id
        self.last_oprid = last_oprid
        self.last_dttm_update = last_dttm_update
        self.cartcount = cartcount
        self.synctops = synctops
    }
    */
}
