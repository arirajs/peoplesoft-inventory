//
//  tbl_ps_inv_detail.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 5/9/20.
//  Copyright © 2020 Ariraj Shanmugavelu. All rights reserved.
//

import Foundation
import UIKit

class tbl_ps_inv_detail: NSObject {
    var avg_cart_usage: String?
    var business_unit: String?
    var cart_count_id: String?
    var cart_count_qty: String?
    var cart_count_qty_reset: String?
    var cart_count_status: String?
    var cart_replen_opt: String?
    var compartment: String?
    var count_order: String?
    var count_required: String?
    var defaultMessage: String?
    var descr60: String?
    var include_flg: String?
    var inv_cart_id: String?
    var inv_item_id: String?
    var last_dttm_update: String?
    var last_oprid: String?
    var mfg_id: String?
    var mfg_itm_id: String?
    var qty_maximum: String?
    var qty_optimal: String?
    var sufficient_stock: String?
    var syncdttm: String?
    var syncid: String?
    var unit_of_measure: String?
    var updated_flag: String?
    var days_old: Int16 = 0
}
