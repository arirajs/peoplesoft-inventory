//
//  tbl_ps_inv_header.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 5/9/20.
//  Copyright © 2020 Ariraj Shanmugavelu. All rights reserved.
//

import Foundation
import UIKit

class tbl_inv_header: NSObject {
    var avg_dflt_option: String?
    var business_unit: String?
    var cart_count_id: String?
    var cart_count_status: String?
    var cart_group: String?
    var defaultMessage: String?
    var descr: String?
    var flex_field_c1_a: String?
    var flex_field_c1_b: String?
    var flex_field_c1_c: String?
    var flex_field_c1_d: String?
    var flex_field_c2: String?
    var flex_field_c4: String?
    var flex_field_c6: String?
    var flex_field_c8: String?
    var flex_field_c10_a: String?
    var flex_field_c10_b: String?
    var flex_field_c10_c: String?
    var flex_field_c10_d: String?
    var flex_field_c30_a: String?
    var flex_field_c30_b: String?
    var flex_field_c30_c: String?
    var flex_field_c30_d: String?
    var flex_field_n12_a: String?
    var flex_field_n12_b: String?
    var flex_field_n12_c: String?
    var flex_field_n12_d: String?
    var flex_field_n15_a: String?
    var flex_field_n15_b: String?
    var flex_field_n15_c: String?
    var flex_field_n15_d: String?
    var flex_field_s15_a: String?
    var flex_field_s15_b: String?
    var flex_field_s15_c: String?
    var flex_field_s15_d: String?
    var inv_cart_id: String?
    var oprid: String?
    var par_count_id: String?
    var qty_option: String?
    var qty_option_updated: String?
    var round_option: String?
    var status_descr: String?
    var syncdttm: String?
    var synced: String?
    var syncid: String?
    var days_old: String?
    // var cartDetails: [tbl_ps_inv_detail] = []
}
