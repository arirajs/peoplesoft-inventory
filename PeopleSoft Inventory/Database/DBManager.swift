//
//  DBManager.swift
//  FMDBTut
//
//  Created by Gabriel Theodoropoulos on 07/10/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

import UIKit

class DBManager: NSObject {

    static let shared: DBManager = DBManager()
    
    let databaseFileName = "PeopleSoft.db"
    
    var pathToDatabase: String!
    
    var database: FMDatabase!
    
    
    override init() {
        super.init()
        
        let documentsDirectory = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathToDatabase = documentsDirectory.appending("/\(databaseFileName)")
    }
    
     
    func openDatabase() -> Bool {
        if database == nil {
            if FileManager.default.fileExists(atPath: pathToDatabase) {
                database = FMDatabase(path: pathToDatabase)
            }
        }
        
        if database != nil {
            if database.open() {
                return true
            }
        }
        
        return false
    }
    
    
    // ----------------
     //  func loadMovie(businessUnit:String,inv_cart_id:String, completionHandler: (_ cartAuditModel: CartAuditModel?) -> Void) {
    func loadCurrentSyncCartAudit(businessUnit:String,inv_cart_id:String) ->(CartAuditModel?) {
           var cartAuditModel: CartAuditModel!
        
           if  openDatabase() {
               let query = "select * from PS_CART_AUDIT where businessUnit = ? and inv_cart_id = ?"
        
               do {
                   let results = try database.executeQuery(query,values: [businessUnit,inv_cart_id])
        
                   if results.next() {
                       cartAuditModel = CartAuditModel(businessUnit: results.string(forColumn: "businessUnit"),
                                                       inv_cart_id: results.string(forColumn: "inv_cart_id"),
                                                       last_oprid: results.string(forColumn: "last_oprid"),
                                                       last_dttm_update: results.string(forColumn: "last_dttm_update"),
                                                       cartcount: results.string(forColumn: "cartcount"),
                                                       synctops: results.string(forColumn: "synctops"))
                           
                   }
                   else {
                       print( database.lastError())
                   }
               }
               catch {
                   print(error.localizedDescription)
               }
        
            database.close()
           }
        
        //   completionHandler(cartAuditModel)
        return cartAuditModel
       }
       // ----------------
    
    //MARK:- insert data into table
    
    func InsertData(_ cartAudit:CartAuditModel)  {
     if openDatabase() {
        do{
            // try  sharedInstance.database!.executeUpdate("INSERT or REPLACE INTO PS_CART_AUDIT(businessUnit,inv_cart_id ,last_oprid,last_dttm_update,cartcount,synctops) VALUES(?,?,?,?,?,?)", withArgumentsIn: [cartAudit.businessUnit,cartAudit.inv_cart_id ,cartAudit.last_oprid,cartAudit.last_dttm_update,cartAudit.cartcount,cartAudit.synctops])
            
            let  insertError =  try  database.executeUpdate("INSERT INTO PS_CART_AUDIT(businessUnit,inv_cart_id ) VALUES(?,?)", withArgumentsIn: [cartAudit.businessUnit!,cartAudit.inv_cart_id!])
            print(insertError.description)
            print(cartAudit.businessUnit!,cartAudit.inv_cart_id!)
        }
        catch{
            print("Insert Data error: \(error.localizedDescription)")
        }
        
        database.close()
        //   return (isInserted != nil)
        
    }
    }
    
    func updateCartAudit(business_unit:String,inv_cart_id:String,lastupdated:String,last_oprid:String) {
          if openDatabase() {
              let query = "update PS_CART_AUDIT set last_oprid = ?, last_dttm_update=? ,synctops = 'Y' where businessUnit = ? and inv_cart_id=?"
              
              do {
                  try database.executeUpdate(query, values: [last_oprid, lastupdated, business_unit,inv_cart_id])
              }
              catch {
                  print(error.localizedDescription)
              }
              
              database.close()
          }
      }
    
}
