//
//  FMDBDatabaseModel.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 5/18/20.
//  Copyright © 2020 Ariraj Shanmugavelu. All rights reserved.
//
/*
import Foundation
import UIKit

let sharedInstance = FMDBDatabaseModel()

var cartAuditvar = CartAuditModel()


class FMDBDatabaseModel: NSObject  {
    
    var database:FMDatabase? = nil
    
    
    
    class func getInstance() -> FMDBDatabaseModel
    {
        if (sharedInstance.database == nil)
        {
            sharedInstance.database = FMDatabase(path: UtilSQLLite.getPath(fileName: "PeopleSoft.db"))
        }
        return sharedInstance
    }
    
    
    
    //MARK:- insert data into table
    
    func InsertData(_ cartAudit:CartAuditModel)  {
        sharedInstance.database!.open()
        do{
            // try  sharedInstance.database!.executeUpdate("INSERT or REPLACE INTO PS_CART_AUDIT(businessUnit,inv_cart_id ,last_oprid,last_dttm_update,cartcount,synctops) VALUES(?,?,?,?,?,?)", withArgumentsIn: [cartAudit.businessUnit,cartAudit.inv_cart_id ,cartAudit.last_oprid,cartAudit.last_dttm_update,cartAudit.cartcount,cartAudit.synctops])
            
            var  insertError =  try  sharedInstance.database!.executeUpdate("INSERT INTO PS_CART_AUDIT(businessUnit,inv_cart_id ) VALUES(?,?)", withArgumentsIn: [cartAudit.businessUnit!,cartAudit.inv_cart_id!])
            print(insertError.description)
            print(cartAudit.businessUnit!,cartAudit.inv_cart_id!)
        }
        catch{
            print("Insert Data error: \(error.localizedDescription)")
        }
        
        sharedInstance.database!.close()
        //   return (isInserted != nil)
        
    }
    
    
    func selectCartAudit(businessUnit:String,inv_cart_id:String) -> CartAuditModel {
        
        
        var storeSynctpPC:String?
        
        sharedInstance.database!.open()
        let query = "select * from PS_CART_AUDIT where businessUnit = ? and inv_cart_id = ?"
        
        do {
            let results = try sharedInstance.database!.executeQuery(query, values: [businessUnit,inv_cart_id])
            
            if results.next() {
                
                
                cartAuditvar.businessUnit? =  String(results.string(forColumn: "businessUnit")!)
                cartAuditvar.inv_cart_id? = String(results.string(forColumn: "inv_cart_id")!)
                cartAuditvar.last_oprid? = String(results.string(forColumn: "last_oprid")!)
                cartAuditvar.last_dttm_update? = String( results.string(forColumn: "last_dttm_update")!)
                cartAuditvar.cartcount? = String((results.string(forColumn: "cartcount"))!)
                
                
                
                 print("storeSynctpPC0\(storeSynctpPC)")
                
                if let safesynctops = results.string(forColumn: "synctops"){
                    cartAuditvar.synctops? = safesynctops
                    storeSynctpPC = safesynctops
                }else{
                    cartAuditvar.synctops? = "Nil"
                    storeSynctpPC = "Nil"
                }
 
                print(results.string(forColumn: "synctops"))
                print("storeSynctpPC1 \(storeSynctpPC)")
              }
                
            else {
                print(self.database!.lastError())
            }
        }
        catch {
            print(error.localizedDescription)
        }
        
        sharedInstance.database?.close()
        
        print("storeSynctpPC \(storeSynctpPC)")
        return cartAuditvar
    }
    
    // ----------------
    func loadMovie(businessUnit:String,inv_cart_id:String, completionHandler: (_ cartAuditModel: CartAuditModel?) -> Void) {
        var cartAuditModel: CartAuditModel!
     
        if sharedInstance.database!.open() {
            let query = "select * from PS_CART_AUDIT where businessUnit = ? and inv_cart_id = ?"
     
            do {
                let results = try sharedInstance.database!.executeQuery(query,values: [businessUnit,inv_cart_id])
     
                if results.next() {
                    cartAuditModel = CartAuditModel(businessUnit: results.string(forColumn: "businessUnit"),
                                                    inv_cart_id: results.string(forColumn: "inv_cart_id"),
                                                    last_oprid: results.string(forColumn: "last_oprid"),
                                                    last_dttm_update: results.string(forColumn: "last_dttm_update"),
                                                    cartcount: results.string(forColumn: "cartcount"),
                                                    synctops: results.string(forColumn: "synctops"))
                        
                }
                else {
                    print( sharedInstance.database?.lastError())
                }
            }
            catch {
                print(error.localizedDescription)
            }
     
             sharedInstance.database?.close()
        }
     
        completionHandler(cartAuditModel)
    }
    // ----------------
    func updateCartAudit(business_unit:String,inv_cart_id:String,lastupdated:String,last_oprid:String) {
        if sharedInstance.database!.open() {
            let query = "update PS_CART_AUDIT set last_oprid = ?, last_dttm_update=? ,synctops = 'Y' where businessUnit = ? and inv_cart_id=?"
            
            do {
                try sharedInstance.database!.executeUpdate(query, values: [last_oprid, lastupdated, business_unit,inv_cart_id])
            }
            catch {
                print(error.localizedDescription)
            }
            
            sharedInstance.database!.close()
        }
    }
    
}
*/
