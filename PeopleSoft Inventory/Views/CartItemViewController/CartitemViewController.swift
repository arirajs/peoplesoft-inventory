//
//  CartitemViewController.swift
//  PeopleSoft Inventory 2018 V1
//
//  Created by Ariraj Shanmugavelu on 2/28/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import UIKit
import CoreData
 
class CartitemViewController: UIViewController,UISearchBarDelegate,NSFetchedResultsControllerDelegate,reloadonlycurrentrowForParcountID
{

    @IBOutlet weak var SearchBar: UISearchBar!
    
    @IBOutlet weak var TableView: UITableView!
    
    var businessUnitTable:[PS_BUSINESS_UNIT]? = nil
    
    var cartAuditModelval: CartAuditModel!
    
    var count:Int = 0
    var countReturn:Int = 0
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    
    var itemUpdatedflag:String?
    // search Bar Variables
    var isSearching = false
    
    var syncedtoPS:String!
    
    var CurrentOprid:String?
     
    var newParcountid:String?
    
    var indexPathsToReloadParcountid = [IndexPath]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TableView.delegate = self
        TableView.dataSource = self
        
        //        //UI Navigation Bar
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.white;
        
        //        self.navigationController?.navigationBar.setBackgroundImage(UIImage(contentsOfFile: "papers.co-vq84-note-7-blue-galaxy-circle-abstract-pattern-33-iphone6-wallpaper"), for: UIBarMetrics.default)
        //        self.navigationController?.navigationBar.shadowImage = UIImage(contentsOfFile: "papers.co-vq84-note-7-blue-galaxy-circle-abstract-pattern-33-iphone6-wallpaper")
        //        self.navigationController?.navigationBar.isTranslucent = true
        
        // Scanner
        //       self.ipcDevice.addDelegate(self)
        //       self.ipcDevice.connect()
        
        // Search Bar
        SearchBar.delegate = self
        // SearchBar.returnKeyType = UIReturnKeyType.done
        SearchBar.placeholder = "Search by Cart Description"
        
        
        
        SearchBar.tintColor = UIColor.white
        SearchBar.barTintColor = UIColor.white
        
        
        initializeFetchedResultsController()
        self.TableView.backgroundColor = UIColor.white
        TableView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = false
        TableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("viewDidAppear")
//        TableView.reloadRows(at: indexPathsToReloadParcountid, with: .automatic)
        
//        TableView.reloadData()
//        TableView.beginUpdates()
//        TableView.reloadRows(at: [IndexPath], with: .none)
//        TableView.endUpdates()
        
    }
    
    func initializeFetchedResultsController() {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_HEADER")
        let business_unit_Sort = NSSortDescriptor(key: "business_unit", ascending: true)
        let cart_count_id_Sort = NSSortDescriptor(key: "inv_cart_id", ascending: true)
        request.sortDescriptors = [business_unit_Sort, cart_count_id_Sort]
        
        request.predicate = NSPredicate(format: "business_unit != nil")
        
        let handler =  CoreDataHelper().getContext()
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: handler, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self as NSFetchedResultsControllerDelegate
        
        do {
            try fetchedResultsController.performFetch()
            TableView.reloadData()
            //  let kRowsCount = fetchedResultsController.sections?[0].numberOfObjects
            //  print(fetchedResultsController.sections?[0].numberOfObjects)
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "todetailCart" {
            let dest = segue.destination as! BuCartItemsViewController
            dest.cartCountHeaderTable = sender as? PS_INV_HEADER
            
            dest.parcountdelegate  = self
        }
    }
}

extension CartitemViewController:UITableViewDelegate,UITableViewDataSource{
    
     func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_HEADER")
        
        let business_unit = NSSortDescriptor(key: "business_unit", ascending: true)
        let inv_cart_id = NSSortDescriptor(key: "inv_cart_id", ascending: true)
        
        request.sortDescriptors = [business_unit, inv_cart_id]
        
        if searchText.isEmpty{
            //  request.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@ ", cartCountHeaderTable.business_unit!, cartCountHeaderTable.inv_cart_id! )
            request.predicate = NSPredicate(format: "business_unit != nil")
        }else
        {
            // request.predicate = NSPredicate(format: "inv_cart_id contains[c] %@ ",searchText )
            request.predicate = NSPredicate(format: "(inv_cart_id contains[c] %@ OR descr contains[c] %@) ",searchText,searchText )
        }
        
        let handler =   CoreDataHelper().getContext()
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: handler, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self as NSFetchedResultsControllerDelegate
        
        do {
            try fetchedResultsController.performFetch()
            //  let kRowsCount = fetchedResultsController.sections?[0].numberOfObjects
            //  print(fetchedResultsController.sections?[0].numberOfObjects)
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
        TableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard  let sections = fetchedResultsController.sections   else {
            fatalError("No sections in fetchedResultsController")
        }
        let sectionInfo = sections[section]
        // print(sectionInfo.numberOfObjects)
        return sectionInfo.numberOfObjects
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        cell.backgroundColor = .clear
    //    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! BusinessCartTableViewCell
        
        // let businessUnitTable1 = businessUnitTable![indexPath.row]
        // cell.setBUCartValue(businessUnitTable: businessUnitTable1)
        cell.selectionStyle = .none
        
        let Cartheaderitem = fetchedResultsController.object(at: indexPath) as! PS_INV_HEADER
        
        cell.businessUnitLbl?.text = Cartheaderitem.business_unit
        cell.locationLbl?.text = Cartheaderitem.inv_cart_id
        cell.LocDescrLbl?.text = Cartheaderitem.descr
        cell.LocGroupLbl?.text = Cartheaderitem.cart_group
        cell.ParcountIDLbl?.text = Cartheaderitem.par_count_id
        cell.StatusLbl?.text = Cartheaderitem.status_descr
        
        
        //  let CartAuditReturn =   sharedInstance.selectCartAudit(businessUnit: Cartheaderitem.business_unit!, inv_cart_id: Cartheaderitem.inv_cart_id!)
        /*
         if let buid = Cartheaderitem.business_unit ,  let cartid = Cartheaderitem.inv_cart_id {
         cartAuditModelval =  DBManager.shared.loadCurrentSyncCartAudit(businessUnit: buid, inv_cart_id: cartid)
         }
         
         
         if let tempsync = self.cartAuditModelval?.synctops{
         syncedtoPS = tempsync ?? "N"
         }else{
         syncedtoPS = "N"
         }
         */
        
        cell.ParcountIDLbl?.text =  getParCountid(BussUnit: Cartheaderitem.business_unit!, invcart: Cartheaderitem.inv_cart_id!)
        
//        print("Cartheaderitem.synced = \(Cartheaderitem.synced)")
        
        if Cartheaderitem.synced == nil {
           // print("cartsynced" + getParCountid(BussUnit: Cartheaderitem.business_unit!, invcart: Cartheaderitem.inv_cart_id!))
            
            cell.lastupdatedOprid.text = ""
            countReturn = getCartCountedORnot(BussUnit: Cartheaderitem.business_unit!, invcart: Cartheaderitem.inv_cart_id!)
            if getlastUpdatedOprid(BussUnit: Cartheaderitem.business_unit!, invcart: Cartheaderitem.inv_cart_id!) != "" && countReturn == 0 {
                
                cell.lastupdatedOprid.text  =  getlastUpdatedOprid(BussUnit: Cartheaderitem.business_unit!, invcart: Cartheaderitem.inv_cart_id!)
                
//                print("Cartheaderitem.business_unit! = \(Cartheaderitem.business_unit!)")
                
                if let safelastupdatVal = cell.lastupdatedOprid.text {
//                    print("safelastupdatVal.count= \(safelastupdatVal.count)")
                    if safelastupdatVal.count > 1 {
                        cell.Synced.image = UIImage(named: "upload_success")
              //  print(getParCountid(BussUnit: Cartheaderitem.business_unit!, invcart: Cartheaderitem.inv_cart_id!))
                //        cell.ParcountIDLbl?.text = "test" + getParCountid(BussUnit: Cartheaderitem.business_unit!, invcart: Cartheaderitem.inv_cart_id!)
                    }else{
                        cell.Synced.image = nil
                    }
                }
            }else{
                if (Cartheaderitem.business_unit != nil && Cartheaderitem.inv_cart_id != nil) {
                    countReturn = getCartCountedORnot(BussUnit: Cartheaderitem.business_unit!, invcart: Cartheaderitem.inv_cart_id!)
                    
//                    print("countReturn = \(countReturn) / Cartheaderitem.qty_option_updated  =\(Cartheaderitem.qty_option_updated) ")
                 //   cell.ParcountIDLbl?.text = "test2" + getParCountid(BussUnit: Cartheaderitem.business_unit!, invcart: Cartheaderitem.inv_cart_id!)
                    
                    if (countReturn > 0 || Cartheaderitem.qty_option_updated == "Y") {
                        cell.Synced.image = UIImage(named: "icons8-cloud_cross-1")
                    }else{
                        cell.Synced.image = nil
                    }
                }
            }
        }else{
            
           
            
            if Cartheaderitem.synced == "Y"  // || syncedtoPS == "Y"
            {
                let oprdDefn = fetchOPRIDDefaults()
                for i in oprdDefn! {
                //    print("oprid = \(i.oprid!)")
                    CurrentOprid = i.oprid!
                }
                
                let todaysDate:Date = Date()
                let dateFormatter:DateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                var todayString:String = dateFormatter.string(from: todaysDate)
//                print("todayString = \(todayString)")
                
                cell.lastupdatedOprid.text  =  "\(CurrentOprid!)  / \( todayString)"
                cell.ParcountIDLbl?.text =  getParCountid(BussUnit: Cartheaderitem.business_unit!, invcart: Cartheaderitem.inv_cart_id!)
                tableView.reloadRows(at: [indexPath], with: .automatic)
              //  tableView.reloadData()
                cell.Synced.image = UIImage(named: "upload_success")
            
            }
            
            if Cartheaderitem.synced == "N"  // || syncedtoPS == "Y"
            {
                tableView.reloadRows(at: [indexPath], with: .automatic)
                cell.lastupdatedOprid.text  = "You are OFFLINE Please try later"
                cell.Synced.image = UIImage(named: "icons8-cloud_cross-1")
            }
            
            
            
            //                    else{
            //
            //                    if (Cartheaderitem.business_unit != nil && Cartheaderitem.inv_cart_id != nil) {
            //                        countReturn = getCartCountedORnot(BussUnit: Cartheaderitem.business_unit!, invcart: Cartheaderitem.inv_cart_id!)
            //                        // print("countReturn = \(countReturn)")
            //                        if countReturn > 0 {
            //                            cell.Synced.image = UIImage(named: "icons8-cloud_cross-1")
            //                        }else{
            //                            cell.Synced.image = nil
            //                        }
            //                    }
            //                }
        }
        
        
        /*
         if let safeoprid = self.cartAuditModelval.last_oprid , let safelastupdate = self.cartAuditModelval.last_dttm_update {
         let safelastupdatefirst10 = String(safelastupdate.prefix(10))
         print("safelastupdatefirst10 = \(safelastupdatefirst10)")
         if safelastupdatefirst10.count > 1 {
         cell.lastupdatedOprid.text = "\(safeoprid)  / \(safelastupdatefirst10)"
         cell.Synced.image = UIImage(named: "upload_success")
         }else{
         cell.lastupdatedOprid.text = ""
         }
         
         }else{
         cell.lastupdatedOprid.text = ""
         }
         */
        
        
        //   cell.backgroundColor = UIColor.gray
        //   cell.contentView.backgroundColor = UIColor.clear
        //        cell.contentView.layer.cornerRadius = 10
        //        cell.contentView.layer.shadowColor = UIColor.black.cgColor
        //        cell.contentView.layer.shadowRadius = 3
        //        cell.contentView.layer.shadowOpacity = 1.0
        //        cell.contentView.clipsToBounds = true
        //        cell.contentView.layer.masksToBounds = false
        
        cell.contentView.layer.cornerRadius = 10
        cell.contentView.layer.shadowColor = UIColor.black.cgColor
        cell.contentView.layer.shadowRadius = 3
        cell.contentView.layer.shadowOpacity = 1.0
        cell.contentView.clipsToBounds = true
        
        return cell
    }
    
    
    
    func getlastUpdatedOprid(BussUnit: String, invcart: String) -> (String) {
        var opridandtime = ""
//        print("\(BussUnit) = \(invcart)")
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    //    let fetchRequest =  NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_DETAILS")
        let fetchRequest =  NSFetchRequest<NSFetchRequestResult>(entityName: "PS_CART_COUNT_H_HIST")
        
        fetchRequest.fetchLimit = 1
        
        fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@ ",BussUnit, invcart )
        
  //      let sortDescriptor = NSSortDescriptor(key: "last_dttm_update", ascending: false)
        let sortDescriptor = NSSortDescriptor(key: "demand_date", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        do {
            let fetchDetails = try context.fetch(fetchRequest) as! [PS_CART_COUNT_H_HIST]
            let max = fetchDetails.first
            
            
    //        let maxTimestamp =  (max?.value(forKey: "last_dttm_update"))
            let maxTimestamp =  (max?.value(forKey: "demand_date"))
            let oprid =  (max?.value(forKey: "last_oprid"))
            if  ((maxTimestamp) != nil) {
                
                let date = maxTimestamp as! String
                let first10 = String(date.prefix(10))
                opridandtime = "\(oprid!)  / \(first10)"
                
            }
            
        } catch _ {
            
        }
        return opridandtime
    }
     
    
    func getCartCountedORnot(BussUnit: String, invcart: String) -> (Int){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_DETAILS")
        
        
        fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@ AND updated_flag = %@",BussUnit, invcart,"Y")
        
        do {
            count = try context.count(for:fetchRequest)
            
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
            
        }
        // print("count = \(count)")
        return count
    }
    
    
    func getParCountid(BussUnit: String, invcart: String) -> (String){
       
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
     //   let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_HEADER")
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_CART_COUNT_H_HIST")
       
        
        fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@",BussUnit,invcart)
        
        do {
          
         let fetchhdrDetails = try context.fetch(fetchRequest) // as! [PS_INV_HEADER]
            
            for data in fetchhdrDetails as! [NSManagedObject]{
            //    print(data.value(forKey: "par_count_id") as? String)
            //    newParcountid = data.value(forKey: "par_count_id") as? String
                newParcountid = data.value(forKey: "cart_count_id") as? String
            }
             
         } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
 //       print("newParcountid = \(String(describing: newParcountid!))")
        return newParcountid! as String
    }
    
    func reloadtableviewcurrentrowForParcountID(offlineORonlineFlag:String ) {
        print("reloadtableviewcurrentrowForParcountID")
        print("offlineORonlineFlag = \(offlineORonlineFlag)")
        TableView.reloadRows(at: indexPathsToReloadParcountid, with: .automatic)
      
    }

    // For Tableview Cell Animation
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        // let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 20, 0)
        // cell.layer.transform = transform
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            // cell.layer.transform = CATransform3DIdentity
 
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        indexPathsToReloadParcountid = [indexPath]
        print("indexPathsToReloadParcountid = \(indexPathsToReloadParcountid)")
        let FetchedCartheaderitem = fetchedResultsController.object(at: indexPath) // as! CartCountHeaderTable
        performSegue(withIdentifier: "todetailCart", sender: FetchedCartheaderitem)
        
    }
    
    // Swipe to reset Code
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deletecart = resetAction(at: indexPath)
        
        let config = UISwipeActionsConfiguration(actions: [deletecart])
        config.performsFirstActionWithFullSwipe = false
        return config
    }
    
    func resetAction(at indexPath:IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "Reset") { (action, view, complete) in
            
            //Oct21 Reset the Synced image
            let indexPath = IndexPath(item: indexPath.row, section: 0)
            //Oct21
            let cell = (self.TableView.cellForRow(at: IndexPath(row: indexPath.row, section: 0)))! as! BusinessCartTableViewCell
            cell.Synced.image = nil
            
            // cell.Synced.image
            
            let inv_Header = self.fetchedResultsController.object(at: indexPath) as! PS_INV_HEADER
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            let fetchRequest_HEADER = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_HEADER")
            
            fetchRequest_HEADER.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@ ", inv_Header.business_unit!, inv_Header.inv_cart_id!)
            
            print("inv_Header.business_unit! = \(inv_Header.business_unit!) : \(inv_Header.inv_cart_id!)")
            do {
                let results_H = try context.fetch(fetchRequest_HEADER) as? [NSManagedObject]
                
                if results_H!.count != 0 { // Atleast one was returned
                    var i = 0
                    for data in results_H as! [NSManagedObject]{
                        // print(data.value(forKey: "updated_flag") as! String)
                        
                        results_H![i].setValue(" ", forKey: "updated_flag")
                        results_H![i].setValue(" ", forKey: "qty_option_updated")
                        i = i + 1
                    }
                }
            } catch {
                print("Fetch Failed: \(error)")
            }
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_DETAILS")
            
            fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@ AND updated_flag = %@", inv_Header.business_unit!,inv_Header.inv_cart_id!, "Y")
            
            do {
                let results = try context.fetch(fetchRequest) as? [NSManagedObject]
                
                if results!.count != 0 { // Atleast one was returned
                    var i = 0
                    for data in results as! [NSManagedObject]{
                        //   print(data.value(forKey: "updated_flag") as! String)
                        
                        results![i].setValue(" ", forKey: "updated_flag")
                        results![i].setValue("0", forKey: "cart_count_qty")
                        i = i + 1
                    }
                }
            } catch {
                print("Fetch Failed: \(error)")
            }
            
            do {
                try context.save()
                // print("Saved!")
            }
            catch {
                
                print("Saving Core Data Failed 3: \(error)")
            }
            
            complete(true)
        }
        return action
    }
}

extension UITableView {
    func scrollToTop(animated: Bool) {
        self.setContentOffset(CGPoint.zero, animated: animated);
    }
}
