//
//  BusinessCartTableViewCell.swift
//  PeopleSoft Inventory 2018 V1
//
//  Created by Ariraj Shanmugavelu on 2/28/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import UIKit
import IBAnimatable

//protocol QuantityOptionDelegate {
//    func didTabCountQty(_ cell: BusinessCartTableViewCell)
//    func didTabRequestQty(_ cell: BusinessCartTableViewCell)
//}
class BusinessCartTableViewCell: UITableViewCell {

//    var delegate: QuantityOptionDelegate?
  
    @IBOutlet weak var CartView: UIView!
    
    @IBOutlet weak var businessUnitLbl: UILabel?
    @IBOutlet weak var locationLbl: UILabel?
    
    @IBOutlet weak var LocDescrLbl: UILabel!
    @IBOutlet weak var LocGroupLbl: UILabel!
    @IBOutlet weak var ParcountIDLbl: UILabel!
    @IBOutlet weak var StatusLbl: UILabel!
    
    @IBOutlet weak var lastupdatedOprid: UILabel!
    
    @IBOutlet weak var lastupdatedonlyLabel: UILabel!
    
    
 //   @IBOutlet weak var Synced: AnimatableImageView!
    
    @IBOutlet weak var Synced: UIImageView!
        
    override func awakeFromNib() {
        CartView.layer.cornerRadius = 10
        CartView.layer.masksToBounds = true
        CartView.layer.shadowOpacity = 0.25
        CartView.layer.shadowRadius = 5
    }
    
    override func prepareForReuse() {
        Synced.image = nil
    }
    
}
