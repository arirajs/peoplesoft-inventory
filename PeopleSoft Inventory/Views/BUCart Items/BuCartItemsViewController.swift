//
//  BuCartItemsViewController.swift
//  PeopleSoft Inventory 2018 V1
//
//  Created by Ariraj Shanmugavelu on 9/3/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import UIKit
import CoreData
import SCLAlertView
 
// import CFNotify

protocol reloadonlycurrentrowForParcountID {
    func  reloadtableviewcurrentrowForParcountID(offlineORonlineFlag: String)
}


class BuCartItemsViewController: UIViewController,NSFetchedResultsControllerDelegate,DTDeviceDelegate ,reloadonlycurrentrow {
    
    var parcountdelegate : reloadonlycurrentrowForParcountID!
 
    let calendar = Calendar.current
    let asofdate = Date()
    var newdate:Int = 0
 
    // 12/29/2019
    let lib=DTDevices.sharedDevice() as! DTDevices
    
    var scanActive = false
    @IBOutlet weak var btBattery: UIButton!
    @IBOutlet weak var ivCharging: UIImageView!
    @IBOutlet weak var btScan: UIButton!
    //Scanner
    //let scanner : DTDevices = DTDevices()
    
    // search Bar Variables
    
    @IBOutlet weak var SearchBar: UISearchBar!
    var isSearching = false
    
    //TableView
    @IBOutlet weak var TableView: UITableView!
    
    //NSFetchRequest
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    
    var cartCountHeaderTable:PS_INV_HEADER!
    var PS_INV_DETAILS_val:PS_INV_HEADER!
    
    var count:Int = 0
    var QtyOptionflag:String?="No"
    var countORrequest:String?="No"
    
    var indexPathsToReload = [IndexPath]()
    
    var selectedRowIndex:IndexPath?
    
    var currentOPRID:String = String()
    var currentPWD:String = String()
     
    var offlineORonlineFlag: String = ""
 
    var days_old_retu:Int = 0
    
    var button3 = BadgeBarButtonItem()
    
    override func viewDidLoad() {
        // print("Viewdid Load")
        // Scanner
        //  self.scanner.delegate = self
        // self.scanner.connect()
        // self.scanner.addDelegate(self)
        
        super.viewDidLoad()
        
        // 12/29/2019
        lib.addDelegate(self)
        lib.connect()
        
        
        //TableView
        TableView.delegate = self
        TableView.dataSource = self
        
        // Search Bar
        SearchBar.delegate = self
        //  SearchBar.returnKeyType = UIReturnKeyType.done
        SearchBar.placeholder = "Search by Item ID or Description"
        SearchBar.barTintColor = UIColor.white
        
        //
        
        button3.isEnabled = false
        
        initializeFetchedResultsController()
        displaySummary()
    }
    
    
    /*
     func connectionState(_ state: Int32) {
     
     guard let newState = IPConnectionState(rawValue: state) else {
     return
     }
     
     print("Connection State: \(newState.toString())")
     
     // print("state = \(state)")
     
     //        switch state {
     //        case CONN_DISCONNECTED:
     //            //Disconnected
     //            break
     //        case CONN_CONNECTING:
     //            //Connecting
     //            break
     //        case CONN_CONNECTED:
     //            //Connected
     //            break
     //        default:
     //            break
     //        }
     }
     
     */
    
    //    func updateBarButtton()
    //    {
    //        let button3 = BadgeBarButtonItem(title: "Save", badgeText: "3", target: self, action: #selector(SaveButtonTapped))
    //
    //        navigationItem.rightBarButtonItems = [button3]
    //        button3.badgeFontSize = 15
    //        button3.badgeFontColor = .red
    //        button3.badgeBackgroundColor = .white
    //        button3.badgeText = "0"
    //    }
    
    @objc func SaveButtonTapped()
    {
        
        print("SaveButtonTapped")
      
        let alert = UIAlertController(title: "Upload to PepleSoft?", message: "Press the Save button to upload the cart to PeopleSoft", preferredStyle: UIAlertController.Style.alert)
        
        //CREATING ON BUTTON
        alert.addAction(UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
         
      
        
        
        
        
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd'T'HH:mm"
        let formated_date = formatter.string(from: date)
       // print(formated_date)
        
            self.button3.isEnabled = false
        
        // UpdateCartCountqty()
            self.UpdateCartCountqty {
                self.updateSaveFlag(SaveFlag: "Y")
           // parcountdelegate?.reloadtableviewcurrentrowForParcountID() //Reset current row for Par Count ID
                self.parcountdelegate?.reloadtableviewcurrentrowForParcountID(offlineORonlineFlag: "No") //Reset current row for Par Count ID
            print("After parcountdelegate")
        }
         
            self.performSegueToReturnBack()
        
        
        }))
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            
           
           // print("Cancel")
        }))
        self.present(alert, animated: true, completion: nil)
        
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        //  print("viewDidAppear")
        self.navigationController?.isNavigationBarHidden = false;
        getCountRequestOption()
        if self.QtyOptionflag == "Y"{
            if  self.countORrequest == "01"{
                self.navigationItem.title = cartCountHeaderTable.business_unit! + "/" + cartCountHeaderTable.inv_cart_id! + "  - " + "Count Qty"
             }
            if  self.countORrequest == "02"{
                self.navigationItem.title = cartCountHeaderTable.business_unit! + "/" + cartCountHeaderTable.inv_cart_id! + "  - " + "Request Qty"
             }
        }else{
            self.navigationItem.title = cartCountHeaderTable.business_unit! + "/" + cartCountHeaderTable.inv_cart_id!
        }
        
        
        //print(cartCountHeaderTable.business_unit! + "/" + cartCountHeaderTable.inv_cart_id!)
        
        getCountRequestOption()
        //  print("getRequest = \(self.QtyOptionflag)")
        if self.QtyOptionflag != "Y"{
            
            //         let alert = SCLAlertView()
            //
            //
            //
            //            _ = alert.addButton("Count Qty"){
            //
            //            }
            //            _ = alert.addButton("Second Button") {
            //                print("Second button tapped")
            //            }
            //            _ = alert.showSuccess("kSuccessTitle", subTitle: "kSubtitle")
            //
            createAlert(title: "Do you want to count by?", message: "Quantity Options")
        }
        
    }
     
 
    
    
    func createAlert (title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        //CREATING ON BUTTON
        alert.addAction(UIAlertAction(title: "Count Qty", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
           // print ("Count Qty")
            self.navigationItem.title =  self.navigationItem.title!  + "  - " + "Count Qty"
            self.updateQuantityOptions(countRequestQty: "01")
        }))
        
        alert.addAction(UIAlertAction(title: "Request Qty", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.navigationItem.title =  self.navigationItem.title!  + "  - " + "Request Qty"
            self.updateQuantityOptions(countRequestQty: "02")
          //  print("Request Qty")
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            //   self.updateQuantityOptions(countRequestQty: "02")
            self.performSegueToReturnBack()
           // print("Cancel")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func updateQuantityOptions(countRequestQty:String){
        //   print("updateQuantityOptions")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_HEADER")
        
        fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@  ", cartCountHeaderTable.business_unit!, cartCountHeaderTable.inv_cart_id!)
        
        do {
            let results = try context.fetch(fetchRequest) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                // print("countQtyvalue = \(countQtyvalue)")
                // In my case, I only updated the first item in results
                results![0].setValue(countRequestQty, forKey: "qty_option")
                results![0].setValue("Y", forKey: "qty_option_updated")
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try context.save()
            // print("Saved!")
        }
        catch {
            print("Saving Core Data Failed1: \(error)")
        }
    }
    
    func getCountRequestOption(){
        //  print("getCountRequestOption")
        // DispatchQueue.main.async {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_HEADER")
        
        fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@  ", self.cartCountHeaderTable.business_unit!, self.cartCountHeaderTable.inv_cart_id!)
        
        do {
            let results = try context.fetch(fetchRequest) as? [PS_INV_HEADER]
            if results?.count != 0 {
                for i in results!{
                    self.QtyOptionflag = i.qty_option_updated
                    self.countORrequest = i.qty_option
 
                }
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
    }
    
    func initializeFetchedResultsController() {
        //    print("initializeFetchedResultsController")
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_DETAILS")
        
        let business_unit = NSSortDescriptor(key: "business_unit", ascending: true)
        let inv_cart_id = NSSortDescriptor(key: "inv_cart_id", ascending: true)
        
        
        let count_required = NSSortDescriptor(key: "count_required", ascending: false)
        //let count_order = NSSortDescriptor(key: "count_order", ascending: true)
        request.sortDescriptors = [business_unit, inv_cart_id,count_required]
        
        //request.sortDescriptors = [NSSortDescriptor(key: "count_order", ascending: true)]
        
        //        print(cartCountHeaderTable.business_unit)
        //        print(cartCountHeaderTable.inv_cart_id)
        
        request.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@", cartCountHeaderTable.business_unit!, cartCountHeaderTable.inv_cart_id! )
        let handler =  CoreDataHelper().getContext()
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: handler, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self as NSFetchedResultsControllerDelegate
        
        do {
            try fetchedResultsController.performFetch()
            //  let kRowsCount = fetchedResultsController.sections?[0].numberOfObjects
            //  print(fetchedResultsController.sections?[0].numberOfObjects)
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //   print("prepare")
        
        if segue.identifier == "toItemsDetailsCart" {
            let dest = segue.destination as! PopUPViewController
            dest.cartCountDetailsItemTable = sender as? PS_INV_DETAILS
            
            selectedRowIndex = self.TableView.indexPathForSelectedRow
            // Get tableviewcell object from indexpath and assign it's value to chosenQuestion of another controller.
            //   let cell = yourtableview.cellForRow(at: selectedRowIndex)
            
            
            dest.onSave = onSave
            dest.delegate = self
            
        }
    }
    
    func onSave(_ data:String)-> (){
        //   print("data-Onsave=\(selectedRowIndex)")
        
        //  updateBarButtton()
        displaySummary()
        //TableView.reloadData()
        
    }
    
    func updateSaveFlag(SaveFlag:String){
        print("updateSaveFlag")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_HEADER")
        
        fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@  ", cartCountHeaderTable.business_unit!, cartCountHeaderTable.inv_cart_id!)
        
        do {
            let results = try context.fetch(fetchRequest) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                
                // print("countQtyvalue = \(countQtyvalue)")
                // In my case, I only updated the first item in results
              //  results![0].setValue("Y", forKey: "synced")
                results![0].setValue(SaveFlag, forKey: "synced")
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try context.save()
            // print("Saved!")
        }
        catch {
            //  @objc
            print("Saving Core Data Failed 3: \(error)")
        }
        
    }
    
    
    //MARK: Controls
    
    @IBAction func onScanDown()
    {
        do
        {
            var scanMode = SCAN_MODES.MODE_SINGLE_SCAN
            try lib.barcodeGetScanMode(&scanMode)
            
            if scanMode==SCAN_MODES.MODE_MOTION_DETECT {
                scanActive = !scanActive
                if scanActive {
                    try lib.barcodeStartScan()
                }else {
                    try lib.barcodeStopScan()
                }
            }else
            {
                try lib.barcodeStartScan()
            }
        }catch let error as NSError {
            print("Operation failed with: \(error.localizedDescription)")
        }
    }
    
    @IBAction func onScanUp()
    {
        do
        {
            try lib.barcodeStopScan()
        }catch let error as NSError {
            print("Operation failed with: \(error.localizedDescription)")
        }
    }
    
    func updateBattery()
    {
        do
        {
            let info = try lib.getBatteryInfo()
            
            let v = info.voltage.format(".2")
            btBattery.setTitle("\(info.capacity)% (\(v)v)", for: UIControl.State.normal)
            ivCharging.isHidden = !info.charging
            if info.capacity<10
            {
                btBattery.setBackgroundImage(UIImage(named: "0.png"), for: UIControl.State.normal)
            }else
            {
                if info.capacity<40
                {
                    btBattery.setBackgroundImage(UIImage(named: "25.png"), for: UIControl.State.normal)
                }else
                {
                    if info.capacity<60
                    {
                        btBattery.setBackgroundImage(UIImage(named: "50.png"), for: UIControl.State.normal)
                    }else
                    {
                        if info.capacity<10
                        {
                            btBattery.setBackgroundImage(UIImage(named: "75.png"), for: UIControl.State.normal)
                        }else
                        {
                            btBattery.setBackgroundImage(UIImage(named: "100.png"), for: UIControl.State.normal)
                        }
                    }
                }
            }
            
            var s = ""
            s+="Voltage: "+info.voltage.format(".3")+"v\n"
            s+="Capacity: \(info.capacity)%\n"
            s+="Maximum capacity: \(info.maximumCapacity)mA/h\n"
            if info.health>0
            {
                s+="Health: \(info.health)%\n"
            }
            s+="Charging: \(info.charging)\n"
            if info.extendedInfo != nil
            {
                s+="Extended info: \(String(describing: info.extendedInfo))\n"
            }
            print(s)
        }catch let error as NSError {
            btBattery!.isHidden=true
            print("Operation failed with: \(error.localizedDescription)")
        }
    }
    
    //MARK: DTDevices notifications
    
    //sent when supported device connects or disconnects. always wait for this message or check connstate before attempting communication. calling connect function does not mean that the device will be connected on the next line
    func connectionState(_ state: Int32) {
        var info="SDK: ver \(lib.sdkVersion/100).\(String.init(format: "%02d", lib.sdkVersion%100)) \(DateFormatter.localizedString(from: lib.sdkBuildDate, dateStyle: DateFormatter.Style.medium, timeStyle: DateFormatter.Style.none))\n"
        
        do
        {
            
            if state==CONN_STATES.CONNECTED.rawValue
            {
                let connected = try lib.getConnectedDevicesInfo()
                for device in connected
                {
                    info+="\(device.name!) \(device.model!) connected\nFW Rev: \(device.firmwareRevision!) HW Rev: \(device.hardwareRevision!)\nSerial: \(device.serialNumber!)\n"
                }
                
                if lib.getSupportedFeature(FEATURES.FEAT_BARCODE, error: nil) != FEAT_UNSUPPORTED
                {
                    btScan.isHidden=false
                }
                
                btBattery.isHidden=false
                updateBattery()
            }else {
                //    btScan.isHidden=true
                //    btBattery.isHidden=true
                print("State = \(state)")
            }
        }catch {}
        print(info)
    }
    
    func barcodeData(_ barcode: String!, type: Int32) {
        //      print("barcodeData")
        SearchBar.text = barcode
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_DETAILS")
        
        let business_unit = NSSortDescriptor(key: "business_unit", ascending: true)
        let inv_cart_id = NSSortDescriptor(key: "inv_cart_id", ascending: true)
        let count_order = NSSortDescriptor(key: "count_order", ascending: true)
        request.sortDescriptors = [business_unit, inv_cart_id,count_order]
        request.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@ AND inv_item_id contains[c] %@", cartCountHeaderTable.business_unit!, cartCountHeaderTable.inv_cart_id!,barcode )
        
        
        let handler =   CoreDataHelper().getContext()
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: handler, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self as NSFetchedResultsControllerDelegate
        
        do {
            try fetchedResultsController.performFetch()
            //  let kRowsCount = fetchedResultsController.sections?[0].numberOfObjects
            //  print(fetchedResultsController.sections?[0].numberOfObjects)
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
        TableView.reloadData()
    }
    
    func UpdateCartCountqty(completion:()->Void){
        
        getCountRequestOption()
        
//        print("UpdateCartCountqty")
//        print("self.countORrequest = \(String(describing: self.countORrequest) )")
        var soapReqXML:String = ""
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd'T'HH:mm"
        let formated_date = formatter.string(from: date)
//        print(formated_date)
        
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_DETAILS")
        
        if self.countORrequest! == "01"{
         fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@ ", cartCountHeaderTable.business_unit!, cartCountHeaderTable.inv_cart_id!)
        }

        if self.countORrequest! == "02"{
            
       fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@ AND updated_flag = %@", cartCountHeaderTable.business_unit!, cartCountHeaderTable.inv_cart_id!,"Y")
        }
        
     //   if self.countORrequest! == "02"{
    //        fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@ AND updated_flag = %@", cartCountHeaderTable.business_unit!, cartCountHeaderTable.inv_cart_id!,"Y")
    //    }
        var include_flag = "N"
        let psoprDefnTable = fetchOPRIDDefaults()
        for i in psoprDefnTable!{
            currentOPRID = i.oprid!
            currentPWD = i.pwd!
            do {
                let results = try context.fetch(fetchRequest)
                
                let PS_INV_DETAILS_val = results as! [PS_INV_DETAILS]
                let CallService = UpdateCartCountQtyService()
                for details in PS_INV_DETAILS_val {
                    
                    print(details.cart_count_qty?.length())
                    
                    print("")
                    print(details.business_unit!)
                    print(details.inv_cart_id!)
                    print(details.inv_item_id!)
                    print(details.compartment!)
                    print(details.cart_count_id!)
                    print(details.count_order!)
                    print(details.cart_count_qty!)
                    print(details.include_flg!)
                    print(self.countORrequest!)
                    print("")
                    
                   var itemscounted:Int = Int(details.cart_count_qty!) ?? 0
                    
  //                  guard var itemscounted = Int(details.cart_count_qty!)  else {
  //                            return
 //                          }
 
                    
                    if self.countORrequest! == "01" {
                        include_flag = "Y"
                        if self.countORrequest! == "01" && details.updated_flag == "Y" {
                            itemscounted = Int(details.cart_count_qty!) ?? 0
                        }else{
                          //  itemscounted = ((details.qty_optimal! as NSString).integerValue - (details.cart_count_qty! as NSString).integerValue)
                          //  itemscounted = (details.qty_optimal! as NSString).integerValue
                            include_flag = "N"
                            itemscounted = 0
                          
                        }
                    }
                    
                    if self.countORrequest! == "02" {
                      
                        if self.countORrequest! == "02" && details.updated_flag == "Y" {
                            itemscounted = Int(details.cart_count_qty!) ?? 0
                            include_flag = "Y"
                        }else{
                            //  itemscounted = ((details.qty_optimal! as NSString).integerValue - (details.cart_count_qty! as NSString).integerValue)
                            itemscounted = 0
                            include_flag = "N"
                        }
                    }
//                    print(self.countORrequest!)
//                    print(itemscounted)
                    
//                    var include_flag = "N"
//
//                    if details.updated_flag == "Y"{
//                        print("include_flag = \(include_flag)")
//                        include_flag = "Y"
//                    }
                    
//                      <CART_CT_UPD_VW>
//                               <BUSINESS_UNIT>INV01</BUSINESS_UNIT>
//                               <INV_CART_ID>2E PERSONAL CAR</INV_CART_ID>
//                               <INV_ITEM_ID>000000000000000511</INV_ITEM_ID>
//                               <COMPARTMENT/>
//                               <COUNT_ORDER>1</COUNT_ORDER>
//                               <SUFFICIENT_STOCK>N</SUFFICIENT_STOCK>
//                               <CART_COUNT_QTY>07</CART_COUNT_QTY>
//                               <UNIT_OF_MEASURE>CA</UNIT_OF_MEASURE>
//                               <QTY_OPTIMAL>1</QTY_OPTIMAL>
//                               <AVG_CART_USAGE>0</AVG_CART_USAGE>
//                               <LAST_OPRID>VP5</LAST_OPRID>
//                               <LAST_DTTM_UPDATE>2020-10-12-20.47.59.000000</LAST_DTTM_UPDATE>
//                               <CART_REPLEN_OPT>01</CART_REPLEN_OPT>
//                               <COUNT_REQUIRED>N</COUNT_REQUIRED>
//                               <QTY_MAXIMUM>0</QTY_MAXIMUM>
//                               <INCLUDE_FLG>Y</INCLUDE_FLG>
//                            </CART_CT_UPD_VW>
                    
                    
                    
                    soapReqXML  += "<CART_CT_UPD_VW>"
                    soapReqXML  += "<BUSINESS_UNIT>"
                    soapReqXML  += details.business_unit!
                    soapReqXML  += "</BUSINESS_UNIT>"
                    
                    soapReqXML  += "<INV_CART_ID>"
                    soapReqXML  += details.inv_cart_id!
                    soapReqXML  += "</INV_CART_ID>"
                    
                    soapReqXML  += "<INV_ITEM_ID>"
                    soapReqXML  += details.inv_item_id!
                    soapReqXML  += "</INV_ITEM_ID>"
           
                    if details.compartment! == "null"{
                        soapReqXML  += "<COMPARTMENT/>"
                    }else{
                        soapReqXML  += "<COMPARTMENT>"
                        soapReqXML  += details.compartment!
                        soapReqXML  += "</COMPARTMENT>"
                    }
                         
//                    soapReqXML  += "<CART_COUNT_ID>"
//                    soapReqXML  += details.cart_count_id!
//                    soapReqXML  += "</CART_COUNT_ID>"
                    
//                    soapReqXML  += "<COUNT_ORDER>"
//                    soapReqXML  += details.count_order!
//                    soapReqXML  += "</COUNT_ORDER>"
                    
//                    soapReqXML  += "<SUFFICIENT_STOCK>"
//                    soapReqXML  += details.sufficient_stock!
//                    soapReqXML  += "</SUFFICIENT_STOCK>"
                                        
                    soapReqXML  += "<CART_COUNT_QTY>"
                    soapReqXML  += "\(itemscounted)"
                    soapReqXML  += "</CART_COUNT_QTY>"
                    
//                    soapReqXML  += "<UNIT_OF_MEASURE>"
//                    soapReqXML  += details.unit_of_measure!
//                    soapReqXML  += "</UNIT_OF_MEASURE>"
                    
 
//                    soapReqXML  += "<AVG_CART_USAGE>"
//                    soapReqXML  += details.avg_cart_usage!
//                    soapReqXML  += "</AVG_CART_USAGE>"
                    
                    
//                    soapReqXML  += "<LAST_OPRID>"
//                    soapReqXML  += currentOPRID
//                    soapReqXML  += "</LAST_OPRID>"

                    soapReqXML  += "<LAST_DTTM_UPDATE>"
                    soapReqXML  += "\(formated_date)"
                    soapReqXML  += "</LAST_DTTM_UPDATE>"
                    
//                    soapReqXML  += "<CART_REPLEN_OPT>"
//                    soapReqXML  += details.cart_replen_opt ?? ""
//                    soapReqXML  += "</CART_REPLEN_OPT>"
                    
//                    soapReqXML  += "<COUNT_REQUIRED>"
//                    soapReqXML  += details.count_required!
//                    soapReqXML  += "</COUNT_REQUIRED>"
                    
//                    soapReqXML  += "<QTY_MAXIMUM>"
//                    soapReqXML  += details.qty_maximum!
//                    soapReqXML  += "</QTY_MAXIMUM>"
                    
                    soapReqXML  += "<CART_COUNT_STATUS>"
                    soapReqXML  += details.cart_count_status!
                    soapReqXML  += "</CART_COUNT_STATUS>"
                    
                    
                    soapReqXML  += "<INCLUDE_FLG>"
                    soapReqXML  +=  include_flag
                    soapReqXML  += "</INCLUDE_FLG>"
                    
                    
                    soapReqXML  += "</CART_CT_UPD_VW>"
                    
                    
                    //                    CallService.UpdateCartQty(UserID: currentOPRID,   Password: currentPWD,
                    //                                              businessUnit: details.business_unit!,
                    //                                              CartID: details.inv_cart_id!,
                    //                                              invitemid: details.inv_item_id!,
                    //                                              compartment: details.compartment!,
                    //                                              cartcountid: details.cart_count_id!,
                    //                                              countorder: details.count_order!,
                    //                                              cartcountqty: details.cart_count_qty!,
                    //                                              qtyOption: cartCountHeaderTable.qty_option!,
                    //                                              include_flag:details.include_flg!) { (Flag) in
                    //                                                print("Flag = \(Flag)")
                    //                                                if Flag == "OFFLINE"{
                    //
                    //                                                    let handler =  CoreDataHelper().getContext()
                    //                                                    let returnValue:PS_ITEMS_OFFLINE =   NSEntityDescription.insertNewObject(forEntityName: "PS_ITEMS_OFFLINE", into: handler) as! PS_ITEMS_OFFLINE
                    //
                    //                                                    returnValue.business_unit  = details.business_unit
                    //                                                    returnValue.inv_cart_id  = details.inv_cart_id
                    //                                                    returnValue.inv_item_id = details.inv_item_id
                    //                                                    returnValue.compartment = details.compartment
                    //                                                    returnValue.cart_count_id = details.cart_count_id
                    //                                                    returnValue.count_order = details.count_order
                    //                                                    returnValue.sufficient_stock = details.sufficient_stock
                    //                                                    returnValue.cart_count_qty = details.cart_count_qty
                    //                                                    returnValue.cart_count_qty_reset = details.cart_count_qty_reset
                    //                                                    returnValue.unit_of_measure = details.unit_of_measure
                    //                                                    returnValue.qty_optimal = details.qty_optimal
                    //                                                    returnValue.avg_cart_usage = details.avg_cart_usage
                    //                                                    returnValue.cart_count_status = details.cart_count_status
                    //                                                    returnValue.count_required = details.count_required
                    //                                                    returnValue.syncid = details.syncid
                    //                                                    returnValue.syncdttm = details.syncdttm
                    //                                                    returnValue.descr60 = details.descr60
                    //                                                    returnValue.include_flg = details.include_flg
                    //                                                    returnValue.mfg_id = details.mfg_id
                    //                                                    returnValue.mfg_itm_id = details.mfg_itm_id
                    //                                                    returnValue.last_dttm_update = details.last_dttm_update
                    //                                                    returnValue.qty_maximum = details.qty_maximum
                    //                                                    returnValue.defaultMessage = details.defaultMessage
                    //                                                    returnValue.qtyOption = self.cartCountHeaderTable.qty_option!
                    //
                    //                                                    DispatchQueue.main.async { try! handler.save() }
                    //                                                }
                    //
                    //                    }
                    //
                }
                
                
print("soapReqXML = \(soapReqXML)")
                
                
                CallService.UpdateCartQty(UserID: currentOPRID, Password: currentPWD, businessUnit: cartCountHeaderTable.business_unit!, CartID:  cartCountHeaderTable.inv_cart_id!, qtyOption: cartCountHeaderTable.qty_option!, avg_dlft_option: cartCountHeaderTable.avg_dflt_option!,
                                          round_option: cartCountHeaderTable.round_option!, cart_count_id: cartCountHeaderTable.cart_count_id!,
                                          par_count_id: cartCountHeaderTable.par_count_id!, cart_count_status: cartCountHeaderTable.cart_count_status!,
                                          status_descr: cartCountHeaderTable.status_descr!, allitems: soapReqXML) { (Flag) in
                     print("Flag = \(Flag)")
                    
                    if Flag == "OFFLINE"{
                        self.updateSaveFlag(SaveFlag: "N")
                    }
                    
                    if Flag == "ONLINE"{
                    //Get the NEW CART COUNT ID AFTER THE CART IS SAVED //
                    getNewParCountID().GetNewParcountService(UserID: self.currentOPRID, Password: self.currentPWD, businessUnit: self.cartCountHeaderTable.business_unit!, CartID: self.cartCountHeaderTable.inv_cart_id!) {(completed) in
                        
                   
                        DispatchQueue.main.async {
                       // self.parcountdelegate?.reloadtableviewcurrentrowForParcountID() //Reset current row for Par Count ID
                            print("Returned = \(completed)")
                          self.parcountdelegate?.reloadtableviewcurrentrowForParcountID(offlineORonlineFlag: Flag) //Reset current row for Par Count ID
                        }
                    }
                    }
                 }
               // print("soapReqXML array = \(soapReqXML)")
            }catch let err as NSError {
                print("Upload Error = \(err.debugDescription)")
            }
        }
        completion()
    }
    
    
    func displaySummary(){
//        print("displaySummary")
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_DETAILS")
        fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@ AND updated_flag = %@", cartCountHeaderTable.business_unit!, cartCountHeaderTable.inv_cart_id!,"Y" )
        
        do {
            let count = try context.count(for:fetchRequest)
            
     //       let button3 = BadgeBarButtonItem(title: "Cart", badgeText: "3", target: self, action: #selector(SaveButtonTapped))
            button3 = BadgeBarButtonItem(title: "Cart", badgeText: "3", target: self, action: #selector(SaveButtonTapped))
            
            navigationItem.rightBarButtonItems = [button3]
            button3.badgeFontSize = 15
            button3.badgeFontColor = .red
            button3.badgeBackgroundColor = .white
            button3.badgeText = String(count)
            
          //  print(count)
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
            
        }
    }
    
    //    func fetchOPRIDDefaults() -> [PsoprDefnTable]?{
    //        let CoreHandler = CoreDataHandler()
    //        let handler =  CoreHandler.getContext()
    //        var psoprDefnTable:[PsoprDefnTable]? = nil
    //
    //        do{
    //            psoprDefnTable = try handler.fetch(PsoprDefnTable.fetchRequest())
    //            return psoprDefnTable
    //        }catch{
    //            return psoprDefnTable
    //        }
    //    }
    
    func fetchOPRIDDefaults() -> [PSOPRDEFN]?
    {
        let conttext = CoreDataHelper().getContext()
        let fetchRequest:NSFetchRequest<PSOPRDEFN> = PSOPRDEFN.fetchRequest()
        var psoprdefn:[PSOPRDEFN]? = nil
        
        do{
            psoprdefn = try conttext.fetch(fetchRequest)
            return psoprdefn
        }catch{
            return psoprdefn
        }
    }
    
    func fetchQTYoptions() -> [PSOPRDEFN]?
    {
        let conttext = CoreDataHelper().getContext()
        let fetchRequest:NSFetchRequest<PSOPRDEFN> = PSOPRDEFN.fetchRequest()
        var psoprdefn:[PSOPRDEFN]? = nil
        
        do{
            psoprdefn = try conttext.fetch(fetchRequest)
            return psoprdefn
        }catch{
            return psoprdefn
        }
    }
}


extension BuCartItemsViewController:UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // print("numberOfRowsInSection")
        guard  let sections = fetchedResultsController.sections   else {
            fatalError("No sections in fetchedResultsController")
        }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        // let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 20, 0)
        // cell.layer.transform = transform
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
        // cell.layer.transform = CATransform3DIdentity
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // print("cellForRowAt = \(indexPath)")
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! BuCartitemsTableViewCell
        cell.selectionStyle = .none
        
        
        let CartDetailitem = fetchedResultsController.object(at: indexPath) as! PS_INV_DETAILS
        
        cell.ItemID.text = CartDetailitem.inv_item_id
        cell.itemDesr.text = CartDetailitem.descr60
        cell.HeaderItemDescr.text = CartDetailitem.descr60
        cell.opt_qty.text = CartDetailitem.qty_optimal
        cell.compartment.text = CartDetailitem.compartment
        
        //  print("Days Old = \(CartDetailitem.days_old)  ")
        //if (threatLevel > 5 && officerRank >= 3)
        
//        if (CartDetailitem.days_old > 0 && CartDetailitem.days_old <= 1)
//        {
//            cell.count_Required_UIview.image = UIImage(named:"green")
//        }else
        
        /// ////// / /
        
        
//        if let safelastupdatVal = cell.lastupdatedOprid.text {
//            print("safelastupdatVal.count= \(safelastupdatVal.count)")
//            if safelastupdatVal.count > 1 {
//                cell.Synced.image = UIImage(named: "upload_success")
//            }else{
//                cell.Synced.image = nil
//            }
//        }
        
      
            
//            //     print("3 \(elemName) = \(strVal)")
//        if let safelast_dttm_update = CartDetailitem.last_dttm_update  {
//                let date = safelast_dttm_update
//                let first10 = String(date.prefix(10))
//
//                let dateFormatter = DateFormatter()
//                dateFormatter.dateFormat = "YYYY-MM-dd" //Your date format
//
//                guard let date1 = dateFormatter.date(from: first10) else {
//                    print("Date could you Nil : first10")
//                    fatalError()
//                }
//
//            self.newdate  = self.calendar.dateComponents([.day], from: date1 , to: self.asofdate).day ?? 0
//                print("newdate = \(self.newdate)")
//
//        print("CartDetailitem.last_dttm_update = \(CartDetailitem.last_dttm_update)")
//        }
//
//         print("CartDetailitem.days_old =\(CartDetailitem.days_old )")
//        //////////////////
        
       
        /*
            if (CartDetailitem.days_old > 0 && CartDetailitem.days_old <= 1)
            {
                cell.count_Required_UIview.image = UIImage(named:"green")
            }else
            if (CartDetailitem.days_old > 1 && CartDetailitem.days_old <= 7)
            {
                cell.count_Required_UIview.image = UIImage(named:"yellow")
            }else if (CartDetailitem.days_old > 7){
                cell.count_Required_UIview.image = UIImage(named:"red")
            }else{
                cell.count_Required_UIview.image = nil
                
            }
       */
        
        days_old_retu =  getDaysOLD(BussUnit: CartDetailitem.business_unit!, invcart: CartDetailitem.inv_cart_id!, invitem:CartDetailitem.inv_item_id! )
    
//        print("days_old_retu = \(days_old_retu)")
        if (days_old_retu > 0 && days_old_retu <= 1)
        {
            cell.count_Required_UIview.image = UIImage(named:"green")
        }else
        if (days_old_retu > 1 && days_old_retu <= 7)
        {
            cell.count_Required_UIview.image = UIImage(named:"yellow")
        }else if (days_old_retu > 7){
            cell.count_Required_UIview.image = UIImage(named:"red")
        }else{
            cell.count_Required_UIview.image = nil
        }

        
        
        
        //Counted Items
        //  print("CartDetailitem.updated_flag = \(String(describing: CartDetailitem.updated_flag))")
        let countedItems:String = CartDetailitem.updated_flag ?? "N"
        
        if countedItems == "Y"{
            //   cell.descrUIView.backgroundColor = UIColor.red
            //  tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.top)
            cell.itemsCountedImage.image = UIImage(named: "itemcounted")
            cell.count_Required_UIview.image = UIImage(named:"green")
        }
        
        // Required Icon Code Start
        let count_required:String = CartDetailitem.count_required!
        
        if count_required == "Y"   {
            cell.count_Required.image = UIImage(named:"count_Required")
           
        }  else{
            cell.count_Required.image = nil
        }
        // Required Icon Code End
        
        // UICell Color and rounding also check the UICell code
        cell.contentView.layer.cornerRadius = 10
        cell.contentView.layer.shadowColor = UIColor.black.cgColor
        cell.contentView.layer.shadowRadius = 3
        cell.contentView.layer.shadowOpacity = 1.0
        cell.contentView.clipsToBounds = true
        
        return cell
    }
    
    
    
    func getDaysOLD(BussUnit: String, invcart: String , invitem:String) -> (Int){
 //      print("BussUnit = \(BussUnit) and Cart = \(invcart)")
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_CART_COUNT_D_HIST")
        var days_old_value:Int? = 0
     //   let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_CART_COUNT_H_HIST")
       
        
        fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@ AND inv_item_id = %@",BussUnit,invcart,invitem)
        
        do {
          
         let fetchhdrDetails = try context.fetch(fetchRequest) // as! [PS_INV_HEADER]
            
            for data in fetchhdrDetails as! [NSManagedObject]{
                days_old_value = data.value(forKey: "days_old") as! Int
             }
             
         } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
//        print("days_old = \(String(describing: days_old_value))")
        return days_old_value as! Int ?? 0
    }
    
    
    
 
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        
        //        do{
        //            try  scanner.barcodeStartScan()
        //        }catch{
        //            print("Scanner not connected")
        //        }
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_DETAILS")
        
        let business_unit = NSSortDescriptor(key: "business_unit", ascending: true)
        let inv_cart_id = NSSortDescriptor(key: "inv_cart_id", ascending: true)
        let count_required = NSSortDescriptor(key: "count_required", ascending: true)
        // let count_order = NSSortDescriptor(key: "count_order", ascending: true)
        // request.sortDescriptors = [business_unit, inv_cart_id,count_required,count_order]
        request.sortDescriptors = [business_unit, inv_cart_id,count_required]
        
        if searchText.isEmpty{
            request.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@ ", cartCountHeaderTable.business_unit!, cartCountHeaderTable.inv_cart_id! )
        }else
        {
            request.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@ AND (inv_item_id contains[c] %@ OR descr60 contains[c] %@) ", cartCountHeaderTable.business_unit!, cartCountHeaderTable.inv_cart_id!,searchText,searchText )
        }
        
        let handler =  CoreDataHelper().getContext()
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: handler, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self as NSFetchedResultsControllerDelegate
        
        do {
            try fetchedResultsController.performFetch()
            //  let kRowsCount = fetchedResultsController.sections?[0].numberOfObjects
            //  print(fetchedResultsController.sections?[0].numberOfObjects)
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
        
        TableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // print("didSelectRowAt")
        indexPathsToReload = [indexPath]
        let FetchedCartheaderitem = fetchedResultsController.object(at: indexPath) // as! CartCountHeaderTable
        performSegue(withIdentifier: "toItemsDetailsCart", sender: FetchedCartheaderitem)
        // print("return")
    }
    
    func reloadtableviewcurrentrow() {
        print("reloadtableviewcurrentrow")
        TableView.reloadRows(at: indexPathsToReload, with: .automatic)
    }
}

// MARK: - BadgeBarButtonItem

public class BadgeBarButtonItem: UIBarButtonItem {
    
    private(set) lazy var badgeLabel: UILabel = {
        let label = UILabel()
        
        label.text = badgeText?.isEmpty == false ? " \(badgeText!) " : nil
        label.isHidden = badgeText?.isEmpty != false
        label.textColor = badgeFontColor
        label.backgroundColor = badgeBackgroundColor
        
        label.font = .systemFont(ofSize: badgeFontSize)
        label.layer.cornerRadius = badgeFontSize * CGFloat(0.6)
        label.clipsToBounds = true
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.addConstraint(
            NSLayoutConstraint(
                item: label,
                attribute: .width,
                relatedBy: .greaterThanOrEqual,
                toItem: label,
                attribute: .height,
                multiplier: 1,
                constant: 0
            )
        )
        
        return label
    }()
    
    var badgeButton: UIButton? {
        return customView as? UIButton
    }
    
    var badgeText: String? {
        didSet {
            badgeLabel.text = badgeText?.isEmpty == false ? " \(badgeText!) " : nil
            badgeLabel.isHidden = badgeText?.isEmpty != false
            badgeLabel.sizeToFit()
        }
    }
    
    var badgeBackgroundColor: UIColor = .red {
        didSet { badgeLabel.backgroundColor = badgeBackgroundColor }
    }
    
    var badgeFontColor: UIColor = .white {
        didSet { badgeLabel.textColor = badgeFontColor }
    }
    
    var badgeFontSize: CGFloat = UIFont.smallSystemFontSize {
        didSet {
            badgeLabel.font = .systemFont(ofSize: badgeFontSize)
            badgeLabel.layer.cornerRadius = badgeFontSize * CGFloat(0.6)
            badgeLabel.sizeToFit()
        }
    }
}

public extension BadgeBarButtonItem {
    
    convenience init(button: UIButton, badgeText: String? = nil, target: AnyObject?, action: Selector) {
        self.init(customView: button)
        
        self.badgeText = badgeText
        
        button.addTarget(target, action: action, for: .touchUpInside)
        button.sizeToFit()
        button.addSubview(badgeLabel)
        
        button.addConstraint(
            NSLayoutConstraint(
                item: badgeLabel,
                attribute: button.currentTitle?.isEmpty == false ? .top : .centerY,
                relatedBy: .equal,
                toItem: button,
                attribute: .top,
                multiplier: 1,
                constant: 0
            )
        )
        
        button.addConstraint(
            NSLayoutConstraint(
                item: badgeLabel,
                attribute: .centerX,
                relatedBy: .equal,
                toItem: button,
                attribute: .trailing,
                multiplier: 1,
                constant: 0
            )
        )
    }
}

public extension BadgeBarButtonItem {
    
    convenience init(image: UIImage, badgeText: String? = nil, target: AnyObject?, action: Selector) {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        button.setBackgroundImage(image, for: .normal)
        
        self.init(button: button, badgeText: badgeText, target: target, action: action)
    }
    
    convenience init(title: String, badgeText: String? = nil, target: AnyObject?, action: Selector) {
        let button = UIButton(type: .system)
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: UIFont.buttonFontSize)
        
        self.init(button: button, badgeText: badgeText, target: target, action: action)
    }
    
    convenience init?(image: String, badgeText: String? = nil, target: AnyObject?, action: Selector) {
        guard let image = UIImage(named: image) else { return nil }
        self.init(image: image, badgeText: badgeText, target: target, action: action)
    }
}

// MARK : performSegueToReturnBack
extension UIViewController {
    func performSegueToReturnBack()  {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
            print("popViewController")
        } else {
            print("elsepopViewController")
            self.dismiss(animated: true, completion: nil)
        }
    }
}
