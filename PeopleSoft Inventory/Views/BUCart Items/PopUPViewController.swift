//
//  PopUPViewController.swift
//  PeopleSoft Inventory 2018 V1
//
//  Created by Ariraj Shanmugavelu on 9/3/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import UIKit
import CoreData

protocol reloadonlycurrentrow {
    func  reloadtableviewcurrentrow()
}

class PopUPViewController: UIViewController {
     
    var delegate : reloadonlycurrentrow?
    
    @IBOutlet weak var businessuniLbl: UILabel!
    @IBOutlet weak var parlocidLbl: UILabel!
    @IBOutlet weak var itemIDlbl: UILabel!
    @IBOutlet weak var compartmentLbl: UILabel!
    @IBOutlet weak var ItemsDescr: UILabel!
    @IBOutlet weak var itemDescLbl: UILabel!
    @IBOutlet weak var UOMLbl: UILabel!
    @IBOutlet weak var averageUsageLbl: UILabel!
    @IBOutlet weak var LastChangesDtmLbl: UILabel!
    @IBOutlet weak var optimalqtyLbl: UILabel!
    @IBOutlet weak var manufacturer_desc: UILabel!
    @IBOutlet weak var VendorID_Lbl: UILabel!
    // @IBOutlet weak var countQty: UITextView!
    @IBOutlet weak var countQty: UITextField?
    @IBOutlet weak var count_Required: UIImageView!
    @IBOutlet weak var popUpView: UIView!
    
    // VVV Important onSave pass by Function
    var onSave: ((_ data:String) -> ())?
    
    var cartCountDetailsItemTable:PS_INV_DETAILS!
    var TextFieldChangedFlag:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popUpView.layer.cornerRadius = 15
        popUpView.layer.masksToBounds = true
        
        setValuestoItemsDetails()
        
    }
    
    func setValuestoItemsDetails()  {
        
        ItemsDescr.text = cartCountDetailsItemTable.descr60
        // itemDescLbl.text = cartCountDetailsItemTable.descr60
        businessuniLbl.text = cartCountDetailsItemTable.business_unit
        parlocidLbl.text = cartCountDetailsItemTable.inv_cart_id
        itemIDlbl.text = cartCountDetailsItemTable.inv_item_id
        compartmentLbl.text = cartCountDetailsItemTable.compartment
        ItemsDescr.text = cartCountDetailsItemTable.descr60
        UOMLbl.text = cartCountDetailsItemTable.unit_of_measure
        averageUsageLbl.text = cartCountDetailsItemTable.avg_cart_usage
        //LastChangesDtmLbl.text = cartCountDetailsItemTable.last_dttm_update
        optimalqtyLbl.text = cartCountDetailsItemTable.qty_optimal
        
         countQty?.text = cartCountDetailsItemTable.cart_count_qty
       // countQty?.text = "0"
        
        //  itemIDlbl1.text = cartCountDetailsItemTable.inv_item_id
        manufacturer_desc.text = cartCountDetailsItemTable.mfg_id
        VendorID_Lbl.text = cartCountDetailsItemTable.mfg_itm_id
        
        // self.countQty.textContainer.maximumNumberOfLines = 1
        
        if cartCountDetailsItemTable.cart_count_qty?.toInt() == 0 {
            self.countQty?.text = nil
        }
        
        let count_required:String = cartCountDetailsItemTable.count_required!
        
        if count_required == "Y"   {
            self.count_Required.image = UIImage(named:"count_Required")
           // self.count_Required.image = nil //REMOVED
        }  else{
            self.count_Required.image = nil
        }
    }
    
    @IBAction func countQtyChanged(_ sender: Any) {
        TextFieldChangedFlag = true
//        print("changes = \(TextFieldChangedFlag)")
    }
    
    @IBAction func closePopupButton(_ sender: Any) {
         delegate?.reloadtableviewcurrentrow()
         dismiss(animated: true, completion:nil)
    }
        
    @IBAction func closePopup(_ sender: Any) {
     //   print("Close = \(TextFieldChangedFlag)")
        
        guard let countQty_safe = self.countQty?.text  else {
           
            return
        }
        print("countQty_safe = \(countQty_safe)")
        if countQty_safe.isEmpty  {
            print("countQty_safe")
        }
            
        
        
        if TextFieldChangedFlag {
            cartCountQty(countQtyvalue:(self.countQty?.text)!)
            onSave?("Save")
            print("popSave")
        }
        delegate?.reloadtableviewcurrentrow()
        dismiss(animated: true, completion:nil)
    }
    
    func cartCountQty(countQtyvalue:String){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_DETAILS")
                
        fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@ AND inv_item_id = %@ AND compartment =  %@", businessuniLbl.text!, parlocidLbl.text!, itemIDlbl.text!,compartmentLbl.text!)
        
        do {
            let results = try context.fetch(fetchRequest) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                // print("countQtyvalue = \(countQtyvalue)")
                // In my case, I only updated the first item in results
                results![0].setValue(countQtyvalue, forKey: "cart_count_qty")
                results![0].setValue("Y", forKey: "updated_flag")
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try context.save()
            // print("Saved!")
        }
        catch {
            print("Saving Core Data Failed 2: \(error)")
        }
        
        // Header Update flag = y
        
       let fetchRequesthdr = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_HEADER")
               
               fetchRequesthdr.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@", businessuniLbl.text!, parlocidLbl.text!)
               
               do {
                   let results = try context.fetch(fetchRequesthdr) as? [NSManagedObject]
                   if results?.count != 0 { // Atleast one was returned
                       // print("countQtyvalue = \(countQtyvalue)")
                       // In my case, I only updated the first item in results
                       results![0].setValue("Y", forKey: "updated_flag")
                   }
               } catch {
                   print("Fetch Failed: \(error)")
               }
               
               do {
                   try context.save()
                   // print("Saved!")
               }
               catch {
                   print("Saving Core Data Failed 2: \(error)")
               }
        
        
    }
    
}
