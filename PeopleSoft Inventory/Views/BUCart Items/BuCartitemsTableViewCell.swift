//
//  BuCartitemsTableViewCell.swift
//  PeopleSoft Inventory 2018 V1
//
//  Created by Ariraj Shanmugavelu on 9/3/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import UIKit

class BuCartitemsTableViewCell: UITableViewCell {

    @IBOutlet weak var ItemID: UILabel!
    
    @IBOutlet weak var itemDesr: UILabel!
    @IBOutlet weak var HeaderItemDescr: UILabel!
    
    @IBOutlet weak var CartView: UIView!
    
    @IBOutlet weak var count_Required: UIImageView!
    
    @IBOutlet weak var count_Required_UIview: UIImageView!
    
    @IBOutlet weak var descrUIView: UIView!
    
    @IBOutlet weak var itemsCountedImage: UIImageView!
    
    
    @IBOutlet weak var opt_qty: UILabel!
    
    @IBOutlet weak var compartment: UILabel!
    
    override func awakeFromNib() {
        CartView.layer.cornerRadius = 10
        CartView.layer.masksToBounds = true
        CartView.layer.shadowOpacity = 0.25
        CartView.layer.shadowRadius = 5
    }
    
    override func prepareForReuse() {
        itemsCountedImage.image = nil
        count_Required_UIview.image = nil
        count_Required.image = nil
    }
}
