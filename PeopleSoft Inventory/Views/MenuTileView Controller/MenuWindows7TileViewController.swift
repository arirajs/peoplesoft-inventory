//
//  MenuWindows7TileViewController.swift
//  PeopleSoft Inventory 2018 V1
//
//  Created by Ariraj Shanmugavelu on 8/19/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import UIKit
import IBAnimatable

class MenuWindows7TileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var ParButton: AnimatableButton!
    
    @IBOutlet weak var cycleCount: AnimatableButton!
    
    @IBOutlet weak var delivery: AnimatableButton!
    
    @IBOutlet weak var issue: AnimatableButton!
    
    @IBOutlet weak var receive: AnimatableButton!
    
    @IBOutlet weak var pick: AnimatableButton!
    
    @IBOutlet weak var label: AnimatableButton!
    
    @IBOutlet weak var barItems: UIView!
    
    @IBOutlet weak var onLineORoffline: UIImageView!
    
    
    @IBOutlet weak var profileImage: AnimatableImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let onLinestatus = self.fetchSYSONLINEOFFLINE()
        for i in onLinestatus! {
            if i.onoroff {
                
                onLineORoffline.image =  UIImage(named:"icons8-cloud_access" )
                
            }
            else {
                
                onLineORoffline.image =  UIImage(named:"icons8-cloud_access-red" )
            }
        }
        applyShadow()
        
        //Decode Default Profile Image
        guard let data = UserDefaults.standard.object(forKey: "profileImage") as? NSData else {return}
        profileImage.image = UIImage(data: data as Data)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true;
        self.navigationItem.hidesBackButton = true
    }
    
    @IBAction func ExitPressed(_ sender: Any) {
        exit(0)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func applyShadow() {
        ParButton.layer.shadowColor = UIColor.gray.cgColor
        ParButton.layer.shadowOffset = CGSize(width: 5, height: 5)
        ParButton.layer.shadowRadius = 5
        ParButton.layer.shadowOpacity = 1.0
        
        cycleCount.layer.shadowColor = UIColor.gray.cgColor
        cycleCount.layer.shadowOffset = CGSize(width: 5, height: 5)
        cycleCount.layer.shadowRadius = 5
        cycleCount.layer.shadowOpacity = 1.0
        
        delivery.layer.shadowColor = UIColor.gray.cgColor
        delivery.layer.shadowOffset = CGSize(width: 5, height: 5)
        delivery.layer.shadowRadius = 5
        delivery.layer.shadowOpacity = 1.0
        
        issue.layer.shadowColor = UIColor.gray.cgColor
        issue.layer.shadowOffset = CGSize(width: 5, height: 5)
        issue.layer.shadowRadius = 5
        issue.layer.shadowOpacity = 1.0
        
        receive.layer.shadowColor = UIColor.gray.cgColor
        receive.layer.shadowOffset = CGSize(width: 5, height: 5)
        receive.layer.shadowRadius = 5
        receive.layer.shadowOpacity = 1.0
        
        pick.layer.shadowColor = UIColor.gray.cgColor
        pick.layer.shadowOffset = CGSize(width: 5, height: 5)
        pick.layer.shadowRadius = 5
        pick.layer.shadowOpacity = 1.0
        
        label.layer.shadowColor = UIColor.gray.cgColor
        label.layer.shadowOffset = CGSize(width: 5, height: 5)
        label.layer.shadowRadius = 5
        label.layer.shadowOpacity = 1.0
        
        //       barItems.layer.shadowColor = UIColor.gray.cgColor
        //       barItems.layer.shadowOffset = CGSize(width: 5, height: 5)
        //       barItems.layer.shadowRadius = 5
        //       barItems.layer.shadowOpacity = 1.0
        //
        //        onLineORoffline.layer.shadowColor = UIColor.gray.cgColor
        //        onLineORoffline.layer.shadowOffset = CGSize(width: 5, height: 5)
        //        onLineORoffline.layer.shadowRadius = 5
        //        onLineORoffline.layer.shadowOpacity = 1.0
        
    }
    
    @IBAction func insertProfilePhoto(_ sender: Any) {
        print("Insert Photo")
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = false
        
        self.present(image,animated: true){
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            profileImage.image = image
            
            //    let imageData:NSData = UIImageJPEGRepresentation(profileImage.image!, 0.75)! as NSData
            guard let imageData = image.jpegData(compressionQuality: 0.75) else { return }
            
            UserDefaults.standard.set(imageData, forKey: "profileImage")
            
        }else{
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func fetchSYSONLINEOFFLINE() -> [SYSONLINEOFFLINE]?
    {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate?.persistentContainer.viewContext
        let onlineorOff = try! managedContext!.fetch(SYSONLINEOFFLINE.fetchRequest()) as [SYSONLINEOFFLINE]
        
        return onlineorOff
    }
    
    @IBAction func ParbuttonPressed(_ sender: Any) {
//        print("Pressed")
       // prepare(for: "Cartitemsonly", sender: nil)
        performSegue(withIdentifier: "Cartitemsonly", sender: self)
    }
    
    @IBAction func ExitButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


