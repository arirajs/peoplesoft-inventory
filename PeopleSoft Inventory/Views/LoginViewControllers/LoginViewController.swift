//
//  LoginViewController.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 10/21/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import UIKit
import SCLAlertView
import CoreData
import IBAnimatable
import SystemConfiguration
import LocalAuthentication
 
class LoginViewController: UIViewController {
 
    private let loginwsdlReachable = SCNetworkReachabilityCreateWithName(nil,Urldefinations().Url_CI_USERMAINT_SELF)
    var hostname:String? = ""
    
    @IBOutlet weak var oprid: UITextField!
    
    @IBOutlet weak var pwd: UITextField!
    var businessUnit:String = ""
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var versionLabel: UILabel!
    
    @IBOutlet weak var loginView: AnimatableStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        versionLabel.text = "Easi App version: " + getAppCurrentVersionNumber() + "v"
        
        // loginwsdlReachable = SCNetworkReachabilityCreateWithName(nil,urlclass.Url_CI_USERMAINT_SELF) as! String
        // print("loginwsdlReachable = \(loginwsdlReachable)")
        self.checkReachable()
    }
    
    private func checkReachable() {
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(self.loginwsdlReachable!, &flags)
        
        if(isNetworkRechable(with: flags))
        {
            print("flags = \(flags)")
            if flags.contains(.isWWAN){
                print("Via Mobile")
                return
            }
            print("Via Wifi")
        }
        else if (!isNetworkRechable(with: flags)){
            print("NoConnects")
            return
        }
    }
    
    private func isNetworkRechable(with flags : SCNetworkReachabilityFlags) -> Bool{
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        let canConnectAutomatically = flags.contains(.connectionAutomatic) || flags.contains(.connectionOnTraffic)
        let canConnecntwithoutUserInteraction = canConnectAutomatically && !flags.contains(.interventionRequired)
        print("isReachable = \(isReachable)")
        print("needsConnection = \(needsConnection)")
        print("canConnectAutomatically = \(canConnectAutomatically)")
        print("canConnecntwithoutUserInteraction = \(canConnecntwithoutUserInteraction)")
        
        return isReachable && (!needsConnection || canConnecntwithoutUserInteraction)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true;
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func TouchOrFaceIDAuth(_ sender: Any) {
        let context = LAContext()
        var error:NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error){
            let reason = "Identify yourself!"
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { [weak self] success,authenticationError in
                DispatchQueue.main.async {
                    if success{
                        //self?.unlockSecretMessage()
                        self?.LogintoPeopleSoft()
                        
                    }else{
                        let ac = UIAlertController(title: "Authentication Failed", message: "You could not be verified.Please try again", preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        self?.present(ac, animated: true)
                    }
                }
            }
        }else{
            let ac = UIAlertController(title: "Biometry unavaiable", message: "Your device is not configure for biometric", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    
    
    @IBAction func LoginButtonPressed(_ sender: Any) {
        print("LoginPressed")
        getHostDetails()

        guard let safeHostinfo = self.hostname else {
            return
        }
        
        if safeHostinfo.isEmpty{
            SCLAlertView().showInfo("Important", subTitle: "Please click the LOCK icon and enter PeopleSoft Environment")
        }else{
          self.LogintoPeopleSoft()
        }
        
      //  self.pwd.text = ""
      //  self.oprid.text = ""
    }
    
    func LogintoPeopleSoft(){
        
        guard let usernametext = oprid.text, let passwordText = pwd.text ,let safeHostinfo = self.hostname else {
            return
        }
              
        // Modified 22/Jan/21
        var usernametexttrimmer = usernametext.trimmingCharacters(in: .whitespacesAndNewlines)
        
        
        if safeHostinfo.isEmpty{
            SCLAlertView().showInfo("Important", subTitle: "Please provide the HOST inforamtion")
        }
        
        // Modified 22/Jan/21
    //    if usernametext.isEmpty || passwordText.isEmpty {
        if usernametexttrimmer.isEmpty || passwordText.isEmpty {
            SCLAlertView().showInfo("Important", subTitle: "User ID or Password misssing")
            // UIViewController.alert.close()
            print("User ID password misssing")
        } else {
            
            // ProgressBar Code
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            
            let alert = SCLAlertView(appearance: appearance).showWait("Downloading...", subTitle: "Processing...", closeButtonTitle: nil, timeout: nil, colorStyle: 0x2A6EBC, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: SCLAnimationStyle.topToBottom)
            
            
            // Delete All userID
            self.deleteAllData(entity: "PSOPRDEFN")
            AuthWebServices().GetUserProfileDefault(UserID: oprid.text!, Password:  pwd.text!, completion: { (err,errorFlag) in
                
//                print("err1 = \(err)")
//                print("errorFlag = \(errorFlag)")
                
                DispatchQueue.main.async {
                    
                    if err == "User password failed." || err == "User password failed. (158,45)" {
                        SCLAlertView().showInfo("Important", subTitle: err)
                        alert.close()
                        return
                    }
                    // if peopleSoft is Down or Network Down
                    if err == "Could not connect to the server."{
                        //    _ = SCLAlertView().showError("PeopleSoft Server Error!", subTitle:err , closeButtonTitle:"OK")
                        self.updateONlineOFFLine(onlineOffline: false)
                        alert.close()
                        self.performSegue(withIdentifier: "TileButton", sender: self)
                        //return
                    }
                    print("LoginPressed1")
                    
                    if err == ""{
                        
                        // Delete All OLD DATA
                        self.deleteAllData(entity: "PS_BUSINESS_UNIT")
                        self.deleteAllData(entity: "PS_OPR_DEF_TBL_FS")
                        self.deleteAllData(entity: "PS_INV_HEADER")
                        self.deleteAllData(entity: "PS_CART_CT_INF_INV")
                        self.deleteAllData(entity: "PS_CART_COUNT_H_HIST")
                        self.deleteAllData(entity: "PS_CART_COUNT_D_HIST")
                        
                        
                        getCartLastUpdatedHeaderService().getCartLastUpdatedHEADERServiceFunc(UserID: usernametexttrimmer, Password: passwordText) {
                           // print("All last updated UserIDs downloaded")
                        }
                                                                         
                       
                        print("No Error")
                        //    _ = getUserPreferenceWebServices().GetUserProfileDefault(UserID: self.oprid.text!, Password: self.pwd.text!)
                        
                        let oprdDefn = fetchOPRIDDefaults()
                        for i in oprdDefn! {
                           // print("User = \(i.oprid!)")
                        }
                        
                        let oprdefault = self.fetchoprDefaults()
                        
                        for j in oprdefault!{
                        //    print("j.business_unit = \(j.business_unit)")
                            self.businessUnit = j.business_unit ?? "NoBusinessUnit"
                        }
                        print("No Error1")
                        var i = 0
                        
                        if self.businessUnit != "NoBusinessUnit"{
                            GetBusinessUnitsWebServices().GetBUINVCartIDOnly(UserID: self.oprid.text!, Password: self.pwd.text!, business_unit: self.businessUnit)
                            // print("No Error4")
                            let buCarts = self.fetchBUCarts()
                            // print(buCarts?.count)
                            if buCarts!.count == 0{
                                SCLAlertView().showInfo("Important", subTitle: "Please check with User Profile for BU")
                                alert.close()
                            }
                            for k in buCarts!{
                                //                _ = GetBUCartitemsHistoryWebServices().GetBUINVCartIDHistoryService(UserID: self.oprid.text!, Password: self.pwd.text!, businessUnit: k.businessUnit!, CartID: k.inv_cart_id!, completion: {
                                //                                   // print("All History Downloaded")
                                //                                })
                                //       print("No Error5")
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                    alert.setSubTitle("Business Unit:\(k.businessUnit!)/ \(k.inv_cart_id!)")
                                }
                                //  print("No Error2")
                                getCartLastUpdatedDetailsService().getCartLastUpdatedDetailsServiceFunc(UserID: usernametexttrimmer, Password: passwordText, business_unit:k.businessUnit!, inv_cart_id: k.inv_cart_id!) {
                                   // print("All last updated UserIDs downloaded ITEMS")
                                }
                                GetBUCartitemsWebServices().GetBUINVCartIDService(UserID: self.oprid.text!, Password: self.pwd.text!, businessUnit:k.businessUnit!, CartID: k.inv_cart_id!, completion: {
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                        alert.setSubTitle("Business Unit:\(k.businessUnit!)/ \(k.inv_cart_id!)")
                                    }
                                    //   print("No Error3 ")
                                    print("Business Unit:\(k.businessUnit!)/ \(k.inv_cart_id!)")
                                    i = i + 1
                                    if i == buCarts?.count{
                                        DispatchQueue.main.async {
                                            self.updateONlineOFFLine(onlineOffline: true)
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                                alert.setSubTitle("Business Unit:\(k.businessUnit!)/ \(k.inv_cart_id!)")
                                            }
                                            alert.close()
                                            
                                              self.pwd.text = ""
                                              self.oprid.text = ""
                                            
                                            self.performSegue(withIdentifier: "TileButton", sender: self)
                                        }
                                    }
                                })
                            }
                        }
                    }else{
                        // Get the PeopleSoft Error like Password not Correct etc
                        //  _ = SCLAlertView().showError("PeopleSoft error!", subTitle:err , closeButtonTitle:"OK")
                        
                        if err == "The request timed out."{
                            self.updateONlineOFFLine(onlineOffline: false)
                            alert.close()
                            self.performSegue(withIdentifier: "TileButton", sender: self)
                        }
                        
                        if err == "The Internet connection appears to be offline"{
                            self.updateONlineOFFLine(onlineOffline: false)
                            alert.close()
                            self.performSegue(withIdentifier: "TileButton", sender: self)
                        }
                        
                        // return
                    }
                }
                
            })
        }
    }
    
    
    func showWait() {
        
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        
        let alert = SCLAlertView(appearance: appearance).showWait("Downloading...", subTitle: "Processing...", closeButtonTitle: nil, timeout: nil, colorStyle: 0x2A6EBC, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: SCLAnimationStyle.topToBottom)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            alert.setSubTitle("Progress: 10%")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                alert.setSubTitle("Progress: 30%")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    alert.setSubTitle("Progress: 50%")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        alert.setSubTitle("Progress: 70%")
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            alert.setSubTitle("Progress: 90%")
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                alert.close()
                                
                                self.performSegue(withIdentifier: "TileButton", sender: self)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func deleteAllData(entity: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedContext.delete(managedObjectData)
            }
        } catch let error as NSError {
            print("Delete all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
    
    /*   func fetchOPRIDDefaults() -> [PSOPRDEFN]?
     {
     let conttext = CoreDataHelper().getContext()
     let fetchRequest:NSFetchRequest<PSOPRDEFN> = PSOPRDEFN.fetchRequest()
     var psoprdefn:[PSOPRDEFN]? = nil
     
     do{
     psoprdefn = try conttext.fetch(fetchRequest)
     return psoprdefn
     }catch{
     return psoprdefn
     }
     }
     */
    func fetchBUCarts() -> [PS_BUSINESS_UNIT]?
    {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate?.persistentContainer.viewContext
        let businessunit = try! managedContext!.fetch(PS_BUSINESS_UNIT.fetchRequest()) as [PS_BUSINESS_UNIT]
        
        return businessunit
    }
    
    func fetchoprDefaults() -> [PS_OPR_DEF_TBL_FS]?
    {
        let conttext = CoreDataHelper().getContext()
        let fetchRequest:NSFetchRequest<PS_OPR_DEF_TBL_FS> = PS_OPR_DEF_TBL_FS.fetchRequest()
        var oprdefault:[PS_OPR_DEF_TBL_FS]? = nil
        
        do{
            oprdefault = try conttext.fetch(fetchRequest)
            return oprdefault
        }catch{
            return oprdefault
        }
    }
    
    func updateONlineOFFLine(onlineOffline:Bool){
        
        self.deleteAllData(entity: "SYSONLINEOFFLINE")
        
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //Now let’s create an entity and new user records.
        let userEntity = NSEntityDescription.entity(forEntityName: "SYSONLINEOFFLINE", in: managedContext)!
        
        //final, we need to add some data to our newly created record for each keys using
        
        let user = NSManagedObject(entity: userEntity, insertInto: managedContext)
        user.setValue(onlineOffline, forKeyPath: "onoroff")
        
        //Now we have set all the values. The next step is to save them inside the Core Data
        
        do {
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
    }
    
    func applyShadow() {
        loginView.layer.shadowColor = UIColor.black.cgColor
        loginView.layer.shadowOffset = CGSize(width: 5, height: 5)
        loginView.layer.shadowRadius = 5
        loginView.layer.shadowOpacity = 1.0
    }
    
    func getAppCurrentVersionNumber() -> String {
        let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
        return nsObject as! String
    }
    
    @IBAction func setupbuttonPressed(_ sender: Any) {
        print("setupbuttonPressed")
        self.performSegue(withIdentifier: "setupPage", sender: self)
    }
    func getHostDetails (){
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //Prepare the request of type NSFetchRequest  for the entity
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_HOST_DETAILS")
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            
            for data in result as! [NSManagedObject] {
                hostname = (data.value(forKey: "host") as! String)
            }
             
          
        } catch {
            print("Failed")
        }
        
    }
}


