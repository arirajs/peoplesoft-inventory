//
//  SetupViewController.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 10/20/20.
//  Copyright © 2020 Ariraj Shanmugavelu. All rights reserved.
//

import UIKit
import SCLAlertView

class SetupViewController: UIViewController {

    @IBOutlet weak var AdminUsername: UITextField!
    
    @IBOutlet weak var AdminPwd: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion:nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func ValidateAdminCredentails(_ sender: Any) {
        guard let usernametext = AdminUsername.text, let passwordText = AdminPwd.text else {
            return
        }
        
        if usernametext.isEmpty || passwordText.isEmpty {
            SCLAlertView().showInfo("Important", subTitle: "User ID or Password misssing")
            // UIViewController.alert.close()
            print("User ID password misssing")
        }else{
            if usernametext == "Admin" && passwordText == "Meyer2020!"{
                print("\(usernametext) = \(passwordText)")
                AdminUsername.text = ""
                AdminPwd.text = ""
                self.performSegue(withIdentifier: "HostPage", sender: self)
               // dismiss(animated: true, completion:nil)
            }
        }
    }
}
