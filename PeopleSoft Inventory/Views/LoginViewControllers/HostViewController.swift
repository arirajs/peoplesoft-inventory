//
//  HostViewController.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 10/20/20.
//  Copyright © 2020 Ariraj Shanmugavelu. All rights reserved.
//

import UIKit
import CoreData
import SCLAlertView

class HostViewController: UIViewController {
    
    @IBOutlet weak var hostTextfield: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_HOST_DETAILS")
        
        do {
            let results = try context.fetch(fetchRequest) as? [NSManagedObject]
            print("results.count = \(results!.count)")
            if results!.count != 0 { // Atleast one was returned
                retrieveData()
            }else{
                
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
                
                let managedContext = appDelegate.persistentContainer.viewContext
                
                let userEntity = NSEntityDescription.entity(forEntityName: "PS_HOST_DETAILS", in: managedContext)!
                
                let hostrecord = NSManagedObject(entity: userEntity, insertInto: managedContext)
                
                hostrecord.setValue("http://psoft92web1-tst.holyredeemer.local:8080", forKey: "host")
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try context.save()
            // print("Saved!")
        }
        catch {
            print("Saving Core Data Failed1: \(error)")
        }
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        
        let customViewController = self.presentingViewController as? SetupViewController
        dismiss(animated: true, completion:nil)
        self.dismiss(animated: true) {
            customViewController?.dismiss(animated: true, completion: nil)
         }
         
    }
    
    
    @IBAction func SaveHostButton(_ sender: Any) {
        
        
        guard let hosttext = hostTextfield.text else {
            return
        }
        
        if hosttext.isEmpty  {
            print("isEmpty")
        }else{
            updateData(hostname: hostTextfield.text!)
            SCLAlertView().showInfo("Important", subTitle: "Saved...!")
        }
    }
     
    func updateData(hostname:String){
        
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "PS_HOST_DETAILS")
        //  fetchRequest.predicate = NSPredicate(format: "username = %@", "Ankur1")
        do
            {
                let fetchreq = try managedContext.fetch(fetchRequest)
                
                let objectUpdate = fetchreq[0] as! NSManagedObject
                objectUpdate.setValue(hostTextfield.text, forKey: "host")
                
                do{
                    try managedContext.save()
                }
                catch
                {
                    print(error)
                }
            }
        catch
        {
            print(error)
        }
        
    }
    
    func retrieveData() {
        
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //Prepare the request of type NSFetchRequest  for the entity
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_HOST_DETAILS")
        
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            
            for data in result as! [NSManagedObject] {
                hostTextfield.text  = (data.value(forKey: "host") as! String)
            }
            
        } catch {
            
            print("Failed")
        }
    }
}


//    func insertData(){
//
//        //   print("updateQuantityOptions")
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_HOST_DETAILS")
//
//        //    fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@  ", cartCountHeaderTable.business_unit!, cartCountHeaderTable.inv_cart_id!)
//
//        do {
//            let results = try context.fetch(fetchRequest) as? [NSManagedObject]
//            if results?.count != 0 { // Atleast one was returned
//                // print("countQtyvalue = \(countQtyvalue)")
//                // In my case, I only updated the first item in results
//                results![0].setValue(hostTextfield.text! , forKey: "host")
//
////            }else{
////
////                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
////
////                let managedContext = appDelegate.persistentContainer.viewContext
////
////                let userEntity = NSEntityDescription.entity(forEntityName: "PS_HOST_DETAILS", in: managedContext)!
////
////
////                let hostrecord = NSManagedObject(entity: userEntity, insertInto: managedContext)
////
////                hostrecord.setValue("http://psoft92web1-tst.holyredeemer.local:8080", forKey: "host")
////            }
//        } catch {
//            print("Fetch Failed: \(error)")
//        }
//
//        do {
//            try context.save()
//            // print("Saved!")
//        }
//        catch {
//            print("Saving Core Data Failed1: \(error)")
//        }
//
//    }
//
//
//
//}
