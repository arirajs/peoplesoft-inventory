//
//  Extension.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 4/14/19.
//  Copyright © 2019 Ariraj Shanmugavelu. All rights reserved.
//

import Foundation


extension Date {
    static func changeDaysBy(days : Int) -> Date {
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.day = days
        return Calendar.current.date(byAdding: dateComponents, to: currentDate)!
    }
}


