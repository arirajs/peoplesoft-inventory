//
//  CoreDataHelper.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 1/29/19.
//  Copyright © 2019 Ariraj Shanmugavelu. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataHelper{
    func getContext() -> NSManagedObjectContext
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
}
