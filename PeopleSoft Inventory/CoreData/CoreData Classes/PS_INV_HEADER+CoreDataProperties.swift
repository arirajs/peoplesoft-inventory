//
//  PS_INV_HEADER+CoreDataProperties.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 3/17/19.
//  Copyright © 2019 Ariraj Shanmugavelu. All rights reserved.
//
//

import Foundation
import CoreData


extension PS_INV_HEADER {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PS_INV_HEADER> {
        return NSFetchRequest<PS_INV_HEADER>(entityName: "PS_INV_HEADER")
    }

    @NSManaged public var avg_dflt_option: String?
    @NSManaged public var business_unit: String?
    @NSManaged public var cart_count_id: String?
    @NSManaged public var cart_count_status: String?
    @NSManaged public var cart_group: String?
    @NSManaged public var defaultMessage: String?
    @NSManaged public var descr: String?
    @NSManaged public var flex_field_c1_a: String?
    @NSManaged public var flex_field_c1_b: String?
    @NSManaged public var flex_field_c1_c: String?
    @NSManaged public var flex_field_c1_d: String?
    @NSManaged public var flex_field_c2: String?
    @NSManaged public var flex_field_c4: String?
    @NSManaged public var flex_field_c6: String?
    @NSManaged public var flex_field_c8: String?
    @NSManaged public var flex_field_c10_a: String?
    @NSManaged public var flex_field_c10_b: String?
    @NSManaged public var flex_field_c10_c: String?
    @NSManaged public var flex_field_c10_d: String?
    @NSManaged public var flex_field_c30_a: String?
    @NSManaged public var flex_field_c30_b: String?
    @NSManaged public var flex_field_c30_c: String?
    @NSManaged public var flex_field_c30_d: String?
    @NSManaged public var flex_field_n12_a: String?
    @NSManaged public var flex_field_n12_b: String?
    @NSManaged public var flex_field_n12_c: String?
    @NSManaged public var flex_field_n12_d: String?
    @NSManaged public var flex_field_n15_a: String?
    @NSManaged public var flex_field_n15_b: String?
    @NSManaged public var flex_field_n15_c: String?
    @NSManaged public var flex_field_n15_d: String?
    @NSManaged public var flex_field_s15_a: String?
    @NSManaged public var flex_field_s15_b: String?
    @NSManaged public var flex_field_s15_c: String?
    @NSManaged public var flex_field_s15_d: String?
    @NSManaged public var inv_cart_id: String?
    @NSManaged public var oprid: String?
    @NSManaged public var par_count_id: String?
    @NSManaged public var qty_option: String?
    @NSManaged public var qty_option_updated: String?
    @NSManaged public var round_option: String?
    @NSManaged public var status_descr: String?
    @NSManaged public var syncdttm: String?
    @NSManaged public var synced: String?
    @NSManaged public var syncid: String?
    @NSManaged public var days_old: String?
   // @NSManaged public var cartDetails: NSSet?
    @NSManaged public var cartDetails: [PS_INV_DETAILS]
}

// MARK: Generated accessors for cartDetails
extension PS_INV_HEADER {

    @objc(addCartDetailsObject:)
    @NSManaged public func addToCartDetails(_ value: PS_INV_DETAILS)

    @objc(removeCartDetailsObject:)
    @NSManaged public func removeFromCartDetails(_ value: PS_INV_DETAILS)

    @objc(addCartDetails:)
    @NSManaged public func addToCartDetails(_ values: NSSet)

    @objc(removeCartDetails:)
    @NSManaged public func removeFromCartDetails(_ values: NSSet)

}
