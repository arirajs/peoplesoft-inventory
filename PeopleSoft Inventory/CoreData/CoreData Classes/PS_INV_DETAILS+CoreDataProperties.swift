//
//  PS_INV_DETAILS+CoreDataProperties.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 4/16/19.
//  Copyright © 2019 Ariraj Shanmugavelu. All rights reserved.
//
//

import Foundation
import CoreData


extension PS_INV_DETAILS {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PS_INV_DETAILS> {
        return NSFetchRequest<PS_INV_DETAILS>(entityName: "PS_INV_DETAILS")
    }

    @NSManaged public var avg_cart_usage: String?
    @NSManaged public var business_unit: String?
    @NSManaged public var cart_count_id: String?
    @NSManaged public var cart_count_qty: String?
    @NSManaged public var cart_count_qty_reset: String?
    @NSManaged public var cart_count_status: String?
    @NSManaged public var cart_replen_opt: String?
    @NSManaged public var compartment: String?
    @NSManaged public var count_order: String?
    @NSManaged public var count_required: String?
    @NSManaged public var defaultMessage: String?
    @NSManaged public var descr60: String?
    @NSManaged public var include_flg: String?
    @NSManaged public var inv_cart_id: String?
    @NSManaged public var inv_item_id: String?
    @NSManaged public var last_dttm_update: String?
    @NSManaged public var last_oprid: String?
    @NSManaged public var mfg_id: String?
    @NSManaged public var mfg_itm_id: String?
    @NSManaged public var qty_maximum: String?
    @NSManaged public var qty_optimal: String?
    @NSManaged public var sufficient_stock: String?
    @NSManaged public var syncdttm: String?
    @NSManaged public var syncid: String?
    @NSManaged public var unit_of_measure: String?
    @NSManaged public var updated_flag: String?
    @NSManaged public var days_old: Int16
    @NSManaged public var cartHeader: PS_INV_HEADER?

}
