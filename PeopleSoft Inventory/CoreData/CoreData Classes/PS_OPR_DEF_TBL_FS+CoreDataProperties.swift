//
//  PS_OPR_DEF_TBL_FS+CoreDataProperties.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 12/4/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//
//

import Foundation
import CoreData


extension PS_OPR_DEF_TBL_FS {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PS_OPR_DEF_TBL_FS> {
        return NSFetchRequest<PS_OPR_DEF_TBL_FS>(entityName: "PS_OPR_DEF_TBL_FS")
    }

    @NSManaged public var alt_char_enabled: String?
    @NSManaged public var as_of_date: NSDate?
    @NSManaged public var business_unit: String?
    @NSManaged public var dr_cr_visible_flg: String?
    @NSManaged public var lc_cntry: String?
    @NSManaged public var ledger: String?
    @NSManaged public var ledger_group: String?
    @NSManaged public var name1: String?
    @NSManaged public var oprid: String?
    @NSManaged public var setid: String?
    @NSManaged public var source: String?

}
