//
//  PS_CART_CT_INF_INV+CoreDataProperties.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 2/23/19.
//  Copyright © 2019 Ariraj Shanmugavelu. All rights reserved.
//
//

import Foundation
import CoreData


extension PS_CART_CT_INF_INV {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PS_CART_CT_INF_INV> {
        return NSFetchRequest<PS_CART_CT_INF_INV>(entityName: "PS_CART_CT_INF_INV")
    }

    @NSManaged public var avg_cart_usage: String?
    @NSManaged public var business_unit: String?
    @NSManaged public var cart_count_id: String?
    @NSManaged public var cart_count_qty: String?
    @NSManaged public var cart_count_status: String?
    @NSManaged public var cart_replen_opt: String?
    @NSManaged public var compartment: String?
    @NSManaged public var count_order: String?
    @NSManaged public var count_required: String?
    @NSManaged public var defaultMessage: String?
    @NSManaged public var descr60: String?
    @NSManaged public var include_flg: String?
    @NSManaged public var inv_cart_id: String?
    @NSManaged public var inv_item_id: String?
    @NSManaged public var last_dttm_update: String?
    @NSManaged public var last_oprid: String?
    @NSManaged public var mfg_itm_id: String?
    @NSManaged public var qty_maximum: String?
    @NSManaged public var qty_optimal: String?
    @NSManaged public var sufficient_stock: String?
    @NSManaged public var unit_of_measure: String?
    @NSManaged public var transfer_cost: String?
    @NSManaged public var price_markup_pct: String?
    @NSManaged public var charge_code: String?
    @NSManaged public var process_instance: String?
    @NSManaged public var transfer_calc_type: String?

}
