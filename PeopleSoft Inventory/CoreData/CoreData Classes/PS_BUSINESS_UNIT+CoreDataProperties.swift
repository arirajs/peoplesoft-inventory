//
//  PS_BUSINESS_UNIT+CoreDataProperties.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 12/4/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//
//

import Foundation
import CoreData


extension PS_BUSINESS_UNIT {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PS_BUSINESS_UNIT> {
        return NSFetchRequest<PS_BUSINESS_UNIT>(entityName: "PS_BUSINESS_UNIT")
    }

    @NSManaged public var businessUnit: String?
    @NSManaged public var cart_group: String?
    @NSManaged public var inv_cart_id: String?
    @NSManaged public var items_count: String?

}
