//
//  SYSONLINEOFFLINE+CoreDataProperties.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 2/4/19.
//  Copyright © 2019 Ariraj Shanmugavelu. All rights reserved.
//
//

import Foundation
import CoreData


extension SYSONLINEOFFLINE {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SYSONLINEOFFLINE> {
        return NSFetchRequest<SYSONLINEOFFLINE>(entityName: "SYSONLINEOFFLINE")
    }

    @NSManaged public var onoroff: Bool

}
