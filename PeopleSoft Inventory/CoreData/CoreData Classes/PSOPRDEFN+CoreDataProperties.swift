//
//  PSOPRDEFN+CoreDataProperties.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 12/4/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//
//

import Foundation
import CoreData


extension PSOPRDEFN {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PSOPRDEFN> {
        return NSFetchRequest<PSOPRDEFN>(entityName: "PSOPRDEFN")
    }

    @NSManaged public var defaultMessage: String?
    @NSManaged public var oprdefndesc: String?
    @NSManaged public var oprid: String?
    @NSManaged public var pwd: String?

}
