//
//  GetBUCartitemsWebServices.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 12/1/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//



import Foundation
import CoreData
import UIKit

public class GetBUCartitemsWebServices {
     
   // private var appDelegate = UIApplication.shared.delegate as! AppDelegate
   // private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
 
    
    // var currentHeader: PS_INV_HEADER?
    
    var  allitems = [PS_INV_HEADER]()
    
    let urlclass = Urldefinations()
    
    // Date Compare
    
    let calendar = Calendar.current
    let asofdate = Date()
    var newdate:Int = 0
    
    public func GetBUCartItemsServiceFromXMLString(xmlToParse:String) //->  PS_INV_HEADER
    {
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate //else { return }
        
        let mainQueueContext = appDelegate!.persistentContainer.viewContext
        let privateQueueCOntext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        
        privateQueueCOntext.parent = mainQueueContext
            
        let xml = SWXMLHash.lazy(xmlToParse)
        let xmlRoot = xml.children.first
        _ = xmlRoot?.children.last
        let xmlResponse: XMLIndexer? = xml.children.first // ?.children.first //?.children.first
        let xmlResult0: XMLIndexer?  = xmlResponse?.children.last
        var strVal = ""
        var elemName = ""
 
        
       // privateQueueCOntext.perform {
            privateQueueCOntext.performAndWait {
          
     //     var appDelegate = UIApplication.shared.delegate as! AppDelegate
    //      let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
       
       let returnValue = PS_INV_HEADER (context: privateQueueCOntext)
        
        if elemName == "" {
            // Property name :
            let itemCount1: Int = (xmlResult0?.children.count)!
            for i1 in 0 ..< itemCount1 {
                let xmlResult1:XMLIndexer? = xmlResult0?.children[i1]
                let elem1: XMLElement? =  xmlResult1!.element
                strVal = ""
                if elem1?.children.first is TextElement {
                    let elemText1:TextElement = elem1?.children.first as! TextElement
                    strVal = elemText1.text
                }
                elemName = elem1!.name
                //print("1  \(elemName) = \(strVal)")
                if elemName == "Get__CompIntfc__CART_COUNT_INVResponse" {
                    // Property name : From
                    let itemCount2: Int = (xmlResult1?.children.count)!
                    for i2 in 0 ..< itemCount2 {
                        let xmlResult2:XMLIndexer? = xmlResult1?.children[i2]
                        let elem2: XMLElement? =  xmlResult2!.element
                        strVal = ""
                        if elem2?.children.first is TextElement {
                            let elemText2:TextElement = elem2?.children.first as! TextElement
                            strVal = elemText2.text
                        }
                        elemName = elem2!.name
                        // print("2 MCG_CART_COUNT \(elemName) = \(strVal)")
                        if elemName == "BUSINESS_UNIT"        {returnValue.business_unit = strVal }
                        else if elemName == "INV_CART_ID"          {returnValue.inv_cart_id = strVal }
                        else if elemName == "DESCR"                {if strVal.length()==0 {strVal = "null"}; returnValue.descr = strVal  }
                        else if elemName == "CART_GROUP"           {if strVal.length()==0 {strVal = "null"}; returnValue.cart_group = strVal}
                        else if elemName == "SYNCID"               {if strVal.length()==0 {strVal = "null"}; returnValue.syncid = strVal}
                        else if elemName == "SYNCDTTM"             {if strVal.length()==0 {strVal = "null"}; returnValue.syncdttm = strVal}
                        else if elemName == "AVG_DFLT_OPTION"      {if strVal.length()==0 {strVal = "null"};  returnValue.avg_dflt_option = strVal}
                        else if elemName == "ROUND_OPTION"         {if strVal.length()==0 {strVal = "null"};  returnValue.round_option = strVal}
                        else if elemName == "QTY_OPTION"           {if strVal.length()==0 {strVal = "null"};  returnValue.qty_option = strVal}
                        else if elemName == "CART_COUNT_ID"        {if strVal.length()==0 {strVal = "null"};  returnValue.cart_count_id = strVal}
                        else if elemName == "PAR_COUNT_ID"         {if strVal.length()==0 {strVal = "null"};  returnValue.par_count_id = strVal}
                        else if elemName == "CART_COUNT_STATUS"    {if strVal.length()==0 {strVal = "null"};  returnValue.cart_count_status = strVal}
                        else if elemName == "STATUS_DESCR"         {if strVal.length()==0 {strVal = "null"};  returnValue.status_descr = strVal}
                        else if elemName == "OPRID"                {if strVal.length()==0 {strVal = "null"};  returnValue.oprid = strVal }
                            
                        else if elemName == "FLEX_FIELD_C30_A"     {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c30_a = strVal}
                        else if elemName == "FLEX_FIELD_C30_B"     {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c30_b = strVal}
                        else if elemName == "FLEX_FIELD_C30_C"     {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c30_c = strVal}
                        else if elemName == "FLEX_FIELD_C30_D"     {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c30_d = strVal}
                        else if elemName == "FLEX_FIELD_C1_A"      {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c1_a = strVal}
                        else if elemName == "FLEX_FIELD_C1_B"      {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c1_b = strVal}
                        else if elemName == "FLEX_FIELD_C1_C"      {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c1_c = strVal}
                        else if elemName == "FLEX_FIELD_C1_D"      {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c1_d = strVal}
                        else if elemName == "FLEX_FIELD_C10_A"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_c10_a = strVal}
                        else if elemName == "FLEX_FIELD_C10_B"     {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c10_b = strVal}
                        else if elemName == "FLEX_FIELD_C10_C"     {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c10_c = strVal}
                        else if elemName == "FLEX_FIELD_C10_D"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_c10_d = strVal}
                        else if elemName == "FLEX_FIELD_C2"        {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_c2 = strVal}
                        else if elemName == "FLEX_FIELD_C4"        {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_c4 = strVal}
                        else if elemName == "FLEX_FIELD_C6"        {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_c6 = strVal}
                        else if elemName == "FLEX_FIELD_C8"        {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_c8 = strVal}
                        else if elemName == "FLEX_FIELD_N12_A"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n12_a = strVal}
                        else if elemName == "FLEX_FIELD_N12_B"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n12_b = strVal}
                        else if elemName == "FLEX_FIELD_N12_C"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n12_c = strVal}
                        else if elemName == "FLEX_FIELD_N12_D"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n12_d = strVal}
                        else if elemName == "FLEX_FIELD_N15_A"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n15_a = strVal}
                        else if elemName == "FLEX_FIELD_N15_B"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n15_b = strVal}
                        else if elemName == "FLEX_FIELD_N15_C"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n15_c = strVal}
                        else if elemName == "FLEX_FIELD_N15_D"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n15_d = strVal}
                        else if elemName == "FLEX_FIELD_S15_A"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_s15_a = strVal}
                        else if elemName == "FLEX_FIELD_S15_B"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_s15_b = strVal}
                        else if elemName == "FLEX_FIELD_S15_C"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_s15_c = strVal}
                        else if elemName == "FLEX_FIELD_S15_D"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_s15_d = strVal}
                            
                        else if elemName == "DefaultMessage"       {returnValue.defaultMessage = strVal }
                        
                        if elemName == "CART_CT_UPD_VW" {
                            
                            let returnValue1 = PS_INV_DETAILS(context: privateQueueCOntext)
                            
                            let itemCount3: Int = (xmlResult2?.children.count)!
                            for i3 in 0 ..< itemCount3 {
                                let xmlResult3:XMLIndexer? = xmlResult2?.children[i3]
                                let elem3: XMLElement? =  xmlResult3!.element
                                strVal = ""
                                if elem3?.children.first is TextElement {
                                    let elemText3:TextElement = elem3?.children.first as! TextElement
                                    strVal = elemText3.text
                                }
                                elemName = elem3!.name
                                // ALWAYS ASSIGN THE BU FOR CHILD TABLE FROM HEADER 9.0 FIX //
                             //   returnValue1.business_unit = returnValue.business_unit!
                                
                                if elemName == "LAST_DTTM_UPDATE"{
                                    
                                    //     print("3 \(elemName) = \(strVal)")
                                    
                                    let date = strVal
                                    let first10 = String(date.prefix(10))
                                    
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "yyyy-MM-dd" // "YYYY-MM-dd" //Your date format
                                    dateFormatter.timeZone = TimeZone.current
                                    
                                    guard let date1 = dateFormatter.date(from: first10) else {
//                                        print("Date could you Nil : first10")
                                        fatalError()
                                    }
                                    
                                  //  let date_cur = Date()
                                    let format = DateFormatter()
                                    format.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    format.timeZone = TimeZone.current
                                    let formattedDate =  format.string(from: asofdate)
                                    format.timeZone = TimeZone.current
                                    let formattedDate1 = format.date(from: formattedDate)
                          //          self.newdate  = self.calendar.dateComponents([.day], from: date1 , to: self.asofdate).day ?? 0
                                    
                                 //   print("formattedDate = \(formattedDate)")
                                 //   print("formattedDate1 = \(formattedDate1!)")
                                    
                                    self.newdate  = self.calendar.dateComponents([.day], from: date1 , to:formattedDate1!).day ?? 0
//                                    print("\(date1) - \(formattedDate1!) = \(self.newdate)")
                                  
//                                    let newdate = "\(date1)"
//                                    if self.newdate == 0{
//                                        print("first10 = \(first10) dateString = \(dateString)")
//                                        if newdate == dateString{
//                                            self.newdate = 1
//                                            print("newdate1 = \(newdate)")
//                                        }
//                                    }
                                 }
                          
                                //
                
                                
                               //     print("4 \(elemName) = \(strVal)")
                                     
                                if elemName == "BUSINESS_UNIT"             {if strVal.length()==0 {strVal = "null"}; returnValue1.business_unit = strVal}
                                else if elemName == "INV_CART_ID"          {if strVal.length()==0 {strVal = "null"}; returnValue1.inv_cart_id = strVal  }
                                else if elemName == "INV_ITEM_ID"          {if strVal.length()==0 {strVal = "null"}; returnValue1.inv_item_id = strVal  }
                                else if elemName == "COMPARTMENT"          {if strVal.length()==0 {strVal = "null"}; returnValue1.compartment = strVal}
                                else if elemName == "CART_COUNT_ID"        {if strVal.length()==0 {strVal = "null"}; returnValue1.cart_count_id = strVal }
                                else if elemName == "COUNT_ORDER"          {if strVal.length()==0 {strVal = "null"}; returnValue1.count_order = strVal  }
                                else if elemName == "SUFFICIENT_STOCK"     {if strVal.length()==0 {strVal = "null"}; returnValue1.sufficient_stock = strVal  }
                                else if elemName == "CART_COUNT_QTY"       {if strVal.length()==0 {strVal = "null"}; returnValue1.cart_count_qty = strVal
                                                                                                                     returnValue1.cart_count_qty_reset = strVal }
                                else if elemName == "UNIT_OF_MEASURE"      {if strVal.length()==0 {strVal = "null"}; returnValue1.unit_of_measure = strVal  }
                                else if elemName == "QTY_OPTIMAL"          {if strVal.length()==0 {strVal = "null"}; returnValue1.qty_optimal = strVal  }
                                else if elemName == "AVG_CART_USAGE"       {if strVal.length()==0 {strVal = "null"}; returnValue1.avg_cart_usage = strVal  }
                                else if elemName == "CART_COUNT_STATUS"    {if strVal.length()==0 {strVal = "null"}; returnValue1.cart_count_status = strVal  }
                                else if elemName == "COUNT_REQUIRED"       {if strVal.length()==0 {strVal = "null"}; returnValue1.count_required = strVal  }
                                else if elemName == "SYNCID"               {if strVal.length()==0 {strVal = "null"}; returnValue1.syncid = strVal  }
                                else if elemName == "SYNCDTTM"             {if strVal.length()==0 {strVal = "null"}; returnValue1.syncdttm = strVal}
                                else if elemName == "DESCR60"              {if strVal.length()==0 {strVal = "null"}; returnValue1.descr60 = strVal }
                                else if elemName == "INCLUDE_FLG"          {if strVal.length()==0 {strVal = "null"}; returnValue1.include_flg = strVal  }
                                else if elemName == "MFG_ID"               {if strVal.length()==0 {strVal = "null"}; returnValue1.mfg_id = strVal  }
                                else if elemName == "MFG_ITM_ID"           {if strVal.length()==0 {strVal = "null"}; returnValue1.mfg_itm_id = strVal  }
                                    
                                else if elemName == "LAST_DTTM_UPDATE"     {if strVal.length()==0 {strVal = "null"};  returnValue1.last_dttm_update  = strVal
                                    returnValue1.days_old = Int16(self.newdate)
                                }
                                else if elemName == "LAST_OPRID"     {if strVal.length()==0 {strVal = "null"}; returnValue1.last_oprid  = strVal}
                                    
                                else if elemName == "QTY_MAXIMUM"          {if strVal.length()==0 {strVal = "null"}; returnValue1.qty_maximum  = strVal}
                                    
                                else if elemName == "DefaultMessage"       {if strVal.length()==0 {strVal = "null"}; returnValue1.defaultMessage = strVal  }
                            }
                            
                          //  print(returnValue1) if strVal.length()==0 {strVal = "null23 "};
                            DispatchQueue.main.async {
                            returnValue.addToCartDetails(returnValue1)
                            }
                     
                        }
                       // print(returnValue.cartDetails)
                    }
                }
            }
        }
    
  
        DispatchQueue.main.async { do {
        
        //    try self.appDelegate.saveContext()
              try privateQueueCOntext.save()
       //     self.allitems.append(returnValue)
        
//                print("Saved...! All Items")
        } catch let error as NSError {
            if error.domain == NSCocoaErrorDomain && (error.code == NSValidationNumberTooLargeError ||
            error.code == NSValidationNumberTooSmallError) {
            } else {
                print("Could not save \(error), \(error.userInfo)") }}}
       
      //  return returnValue
        }
    }
    
    public func GetBUCartItemsServiceFromXML(data: Data) //-> PS_INV_HEADER
    {
        
        let xmlToParse   = String.init(data: data, encoding: String.Encoding.utf8)!
        // print(xmlToParse)
            return self.GetBUCartItemsServiceFromXMLString( xmlToParse : xmlToParse)

        }
    
    public func GetBUINVCartIDService(UserID:String,Password:String,businessUnit:String,CartID:String,completion :@escaping ()->Void ){
        
        
        //        <soapenv:Envelope xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsa="http://schemas.xmlsoap.org/ws/2003/03/addressing/" xmlns:xsd="http://www.w3.org/2001/XMLSchema/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance/">
        //        <soapenv:Header>
        //        <wsse:Security soap:mustUnderstand="1" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
        //        <wsse:UsernameToken wsu:Id="UsernameToken-1" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
        //        <wsse:Username>VP1</wsse:Username>
        //        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">VP1</wsse:Password>
        //        </wsse:UsernameToken>
        //        </wsse:Security>
        //        </soapenv:Header>
        //        <soapenv:Body>
        //        <Get__CompIntfc__MCG_CART_COUNT_INV xmlns="http://xmlns.oracle.com/Enterprise/Tools/schemas/M935215.V1">
        //        <BUSINESS_UNIT>US015</BUSINESS_UNIT>
        //        <INV_CART_ID>MS-1</INV_CART_ID>
        //        </Get__CompIntfc__MCG_CART_COUNT_INV>
        //        </soapenv:Body>
        //        </soapenv:Envelope>
        
        //  var soapReqXML:String = "<?xml version=\"1.0\"?>"
      /*  var soapReqXML:String = "<soapenv:Envelope xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "<soapenv:Header xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "</soapenv:Header>"
        soapReqXML  += "<soapenv:Body xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        //       soapReqXML  += "<Get__CompIntfc__MCG_CART_COUNT_INV xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/M935215.V1\">"
        soapReqXML  += "<Get__CompIntfc__CART_COUNT_INV xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/M935215.V1\">"
        soapReqXML  += "<BUSINESS_UNIT>"
        soapReqXML  += businessUnit
        soapReqXML  += "</BUSINESS_UNIT>"
        soapReqXML  += "<INV_CART_ID>"
        soapReqXML  += CartID
        soapReqXML  += "</INV_CART_ID>"
        //       soapReqXML  += "</Get__CompIntfc__MCG_CART_COUNT_INV>"
        soapReqXML  += "</Get__CompIntfc__CART_COUNT_INV>"
        soapReqXML  += "</soapenv:Body>"
        soapReqXML  += "</soapenv:Envelope>"
        */
        
        var soapReqXML:String = "<?xml version=\"1.0\"?>"
        soapReqXML  += "<soapenv:Envelope xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "<soapenv:Header xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "</soapenv:Header>"
        soapReqXML  += "<soapenv:Body xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<Get__CompIntfc__CART_COUNT_INV xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/M471410259.V1\">"
        soapReqXML  += "<BUSINESS_UNIT>"
        soapReqXML  += businessUnit
        soapReqXML  += "</BUSINESS_UNIT>"
        soapReqXML  += "<INV_CART_ID>"
        soapReqXML  += CartID
        soapReqXML  += "</INV_CART_ID>"
        soapReqXML  += "</Get__CompIntfc__CART_COUNT_INV>"
        soapReqXML  += "</soapenv:Body>"
        soapReqXML  += "</soapenv:Envelope>"
        
        
        
        
          //   print(soapReqXML)
        // let responseData:Data = SoapHttpClient.callSoapService(Host : urlclass.Host,WebServiceUrl:urlclass.Url_CI_CART_COUNT_INV,SoapAction:urlclass.soapAction_CI_CART_COUNT_INV_G,SoapMessage:soapReqXML)
        
        SoapHttpClient.callWSAsync(Host: urlclass.Host, WebServiceUrl: urlclass.Url_CI_CART_COUNT_INV, SoapAction: urlclass.soapAction_CI_CART_COUNT_INV_G, SoapMessage: soapReqXML) { (err, responseData, str) in
           // print("Errorerr = \(err)")
           // print("ResponseData = \(responseData)")
            // print("str = \(str)")
            
             self.GetBUCartItemsServiceFromXML(data : responseData!)
            completion()
        }
        
    }
    
}



/*
import Foundation
import CoreData
import UIKit

public class GetBUCartitemsWebServices {
    
    

    //    deinit {
    //        print("deinit - GetBUCartitemsWebServices")
    //    }
    
    // var managedContext: NSManagedObjectContext!
    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // var currentHeader: PS_INV_HEADER?
    
    var  allitems = [PS_INV_HEADER]()
    
    let urlclass = Urldefinations()
    
    // Date Compare
    
    let calendar = Calendar.current
    let asofdate = Date()
    var newdate:Int = 0
    
    public func GetBUCartItemsServiceFromXMLString(xmlToParse:String) ->  PS_INV_HEADER
    {
        


//        appDelegate.persistentContainer.performBackgroundTask { (context) in {
//
//            }
        
//        let persistentContainer = NSPersistentContainer(name: "MyDatabaseName")
//        persistentContainer.loadPersistentStores { (_, error) in
//            if let error = error {
//                fatalError("Failed to load Core Data stack: \(error)")
//            }
//        }
//
//        // Creates a task with a new background context created on the fly
//        persistentContainer.performBackgroundTask { (context) in
//            // Iterates the array
//            dogsName.forEach { name in
//                // Creates a new entry inside the context `context` and assign the array element `name` to the dog's name
//                let dog = Dog(context: context)
//                dog.name = name
//            }
//
//            do {
//                // Saves the entries created in the `forEach`
//                try context.save()
//            } catch {
//                fatalError("Failure to save context: \(error)")
//            }
//        }
            
            
        let xml = SWXMLHash.lazy(xmlToParse)
        let xmlRoot = xml.children.first
        _ = xmlRoot?.children.last
        let xmlResponse: XMLIndexer? = xml.children.first // ?.children.first //?.children.first
        let xmlResult0: XMLIndexer?  = xmlResponse?.children.last
        var strVal = ""
        var elemName = ""
        // let returnValue:PS_INV_HEADER  = PS_INV_HEADER ()
        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
        
        
        //var managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        
        
        //    let returnValue:PS_INV_HEADER  =  NSEntityDescription.insertNewObject(forEntityName: "PS_INV_HEADER ", into: context) as! PS_INV_HEADER
        
//        if Thread.isMainThread {
//            print("Main Queue")
//        }else{
//            print("off MainQueue")
//        }
//        let returnValue = [PS_INV_HEADER]()

       
       let returnValue = PS_INV_HEADER (context: context)
        
        if elemName == "" {
            // Property name :
            let itemCount1: Int = (xmlResult0?.children.count)!
            for i1 in 0 ..< itemCount1 {
                let xmlResult1:XMLIndexer? = xmlResult0?.children[i1]
                let elem1: XMLElement? =  xmlResult1!.element
                strVal = ""
                if elem1?.children.first is TextElement {
                    let elemText1:TextElement = elem1?.children.first as! TextElement
                    strVal = elemText1.text
                }
                elemName = elem1!.name
                //print("1  \(elemName) = \(strVal)")
                if elemName == "Get__CompIntfc__CART_COUNT_INVResponse" {
                    // Property name : From
                    let itemCount2: Int = (xmlResult1?.children.count)!
                    for i2 in 0 ..< itemCount2 {
                        let xmlResult2:XMLIndexer? = xmlResult1?.children[i2]
                        let elem2: XMLElement? =  xmlResult2!.element
                        strVal = ""
                        if elem2?.children.first is TextElement {
                            let elemText2:TextElement = elem2?.children.first as! TextElement
                            strVal = elemText2.text
                        }
                        elemName = elem2!.name
                        //                 print("2 MCG_CART_COUNT \(elemName) = \(strVal)")
                        if elemName == "BUSINESS_UNIT"        {returnValue.business_unit = strVal }
                        else if elemName == "INV_CART_ID"          {returnValue.inv_cart_id = strVal }
                        else if elemName == "DESCR"                {if strVal.length()==0 {strVal = "null"}; returnValue.descr = strVal  }
                        else if elemName == "CART_GROUP"           {if strVal.length()==0 {strVal = "null"}; returnValue.cart_group = strVal}
                        else if elemName == "SYNCID"               {if strVal.length()==0 {strVal = "null"}; returnValue.syncid = strVal}
                        else if elemName == "SYNCDTTM"             {if strVal.length()==0 {strVal = "null"}; returnValue.syncdttm = strVal}
                        else if elemName == "AVG_DFLT_OPTION"      {if strVal.length()==0 {strVal = "null"};  returnValue.avg_dflt_option = strVal}
                        else if elemName == "ROUND_OPTION"         {if strVal.length()==0 {strVal = "null"};  returnValue.round_option = strVal}
                        else if elemName == "QTY_OPTION"           {if strVal.length()==0 {strVal = "null"};  returnValue.qty_option = strVal}
                        else if elemName == "CART_COUNT_ID"        {if strVal.length()==0 {strVal = "null"};  returnValue.cart_count_id = strVal}
                        else if elemName == "PAR_COUNT_ID"         {if strVal.length()==0 {strVal = "null"};  returnValue.par_count_id = strVal}
                        else if elemName == "CART_COUNT_STATUS"    {if strVal.length()==0 {strVal = "null"};  returnValue.cart_count_status = strVal}
                        else if elemName == "STATUS_DESCR"         {if strVal.length()==0 {strVal = "null"};  returnValue.status_descr = strVal}
                        else if elemName == "OPRID"                {if strVal.length()==0 {strVal = "null"};  returnValue.oprid = strVal }
                            
                        else if elemName == "FLEX_FIELD_C30_A"     {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c30_a = strVal}
                        else if elemName == "FLEX_FIELD_C30_B"     {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c30_b = strVal}
                        else if elemName == "FLEX_FIELD_C30_C"     {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c30_c = strVal}
                        else if elemName == "FLEX_FIELD_C30_D"     {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c30_d = strVal}
                        else if elemName == "FLEX_FIELD_C1_A"      {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c1_a = strVal}
                        else if elemName == "FLEX_FIELD_C1_B"      {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c1_b = strVal}
                        else if elemName == "FLEX_FIELD_C1_C"      {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c1_c = strVal}
                        else if elemName == "FLEX_FIELD_C1_D"      {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c1_d = strVal}
                        else if elemName == "FLEX_FIELD_C10_A"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_c10_a = strVal}
                        else if elemName == "FLEX_FIELD_C10_B"     {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c10_b = strVal}
                        else if elemName == "FLEX_FIELD_C10_C"     {if strVal.length()==0 {strVal = "null"};  returnValue.flex_field_c10_c = strVal}
                        else if elemName == "FLEX_FIELD_C10_D"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_c10_d = strVal}
                        else if elemName == "FLEX_FIELD_C2"        {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_c2 = strVal}
                        else if elemName == "FLEX_FIELD_C4"        {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_c4 = strVal}
                        else if elemName == "FLEX_FIELD_C6"        {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_c6 = strVal}
                        else if elemName == "FLEX_FIELD_C8"        {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_c8 = strVal}
                        else if elemName == "FLEX_FIELD_N12_A"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n12_a = strVal}
                        else if elemName == "FLEX_FIELD_N12_B"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n12_b = strVal}
                        else if elemName == "FLEX_FIELD_N12_C"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n12_c = strVal}
                        else if elemName == "FLEX_FIELD_N12_D"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n12_d = strVal}
                        else if elemName == "FLEX_FIELD_N15_A"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n15_a = strVal}
                        else if elemName == "FLEX_FIELD_N15_B"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n15_b = strVal}
                        else if elemName == "FLEX_FIELD_N15_C"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n15_c = strVal}
                        else if elemName == "FLEX_FIELD_N15_D"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_n15_d = strVal}
                        else if elemName == "FLEX_FIELD_S15_A"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_s15_a = strVal}
                        else if elemName == "FLEX_FIELD_S15_B"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_s15_b = strVal}
                        else if elemName == "FLEX_FIELD_S15_C"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_s15_c = strVal}
                        else if elemName == "FLEX_FIELD_S15_D"     {if strVal.length()==0 {strVal = "null"}; returnValue.flex_field_s15_d = strVal}
                            
                        else if elemName == "DefaultMessage"       {returnValue.defaultMessage = strVal }
                        
                        if elemName == "CART_CT_UPD_VW" {
                            
                             
                             let returnValue1 = PS_INV_DETAILS(context: context)
                            
                            let itemCount3: Int = (xmlResult2?.children.count)!
                            for i3 in 0 ..< itemCount3 {
                                let xmlResult3:XMLIndexer? = xmlResult2?.children[i3]
                                let elem3: XMLElement? =  xmlResult3!.element
                                strVal = ""
                                if elem3?.children.first is TextElement {
                                    let elemText3:TextElement = elem3?.children.first as! TextElement
                                    strVal = elemText3.text
                                }
                                elemName = elem3!.name
                                // ALWAYS ASSIGN THE BU FOR CHILD TABLE FROM HEADER 9.0 FIX //
                             //   returnValue1.business_unit = returnValue.business_unit!
                                
                                
                              
                                if elemName == "LAST_DTTM_UPDATE"{
                                    
                               //     print("3 \(elemName) = \(strVal)")
                                    
                                    let date = strVal
                                    let first10 = String(date.prefix(10))
                                    
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "YYYY-MM-dd" //Your date format
                                   
                                    guard let date1 = dateFormatter.date(from: first10) else {
                                        print("Date could you Nil : first10")
                                        fatalError()
                                        
                                    }
                                    newdate  = calendar.dateComponents([.day], from: date1 , to: asofdate).day ?? 0
                                    print("newdate = \(newdate)")
                                }
                      
                               //     print("4 \(elemName) = \(strVal)")
                                     
                                if elemName == "BUSINESS_UNIT"             {if strVal.length()==0 {strVal = "null"}; returnValue1.business_unit = strVal}
                                else if elemName == "INV_CART_ID"          {if strVal.length()==0 {strVal = "null"}; returnValue1.inv_cart_id = strVal  }
                                else if elemName == "INV_ITEM_ID"          {if strVal.length()==0 {strVal = "null"}; returnValue1.inv_item_id = strVal  }
                                else if elemName == "COMPARTMENT"          {if strVal.length()==0 {strVal = "null"}; returnValue1.compartment = strVal}
                                else if elemName == "CART_COUNT_ID"        {if strVal.length()==0 {strVal = "null"}; returnValue1.cart_count_id = strVal }
                                else if elemName == "COUNT_ORDER"          {if strVal.length()==0 {strVal = "null"}; returnValue1.count_order = strVal  }
                                else if elemName == "SUFFICIENT_STOCK"     {if strVal.length()==0 {strVal = "null"}; returnValue1.sufficient_stock = strVal  }
                                else if elemName == "CART_COUNT_QTY"       {if strVal.length()==0 {strVal = "null"}; returnValue1.cart_count_qty = strVal
                                                                                                                     returnValue1.cart_count_qty_reset = strVal
                                }
                                else if elemName == "UNIT_OF_MEASURE"      {if strVal.length()==0 {strVal = "null"}; returnValue1.unit_of_measure = strVal  }
                                else if elemName == "QTY_OPTIMAL"          {if strVal.length()==0 {strVal = "null"}; returnValue1.qty_optimal = strVal  }
                                else if elemName == "AVG_CART_USAGE"       {if strVal.length()==0 {strVal = "null"}; returnValue1.avg_cart_usage = strVal  }
                                else if elemName == "CART_COUNT_STATUS"    {if strVal.length()==0 {strVal = "null"}; returnValue1.cart_count_status = strVal  }
                                else if elemName == "COUNT_REQUIRED"       {if strVal.length()==0 {strVal = "null"}; returnValue1.count_required = strVal  }
                                else if elemName == "SYNCID"               {if strVal.length()==0 {strVal = "null"}; returnValue1.syncid = strVal  }
                                else if elemName == "SYNCDTTM"             {if strVal.length()==0 {strVal = "null"}; returnValue1.syncdttm = strVal}
                                else if elemName == "DESCR60"              {if strVal.length()==0 {strVal = "null"}; returnValue1.descr60 = strVal }
                                else if elemName == "INCLUDE_FLG"          {if strVal.length()==0 {strVal = "null"}; returnValue1.include_flg = strVal  }
                                else if elemName == "MFG_ID"               {if strVal.length()==0 {strVal = "null"}; returnValue1.mfg_id = strVal  }
                                else if elemName == "MFG_ITM_ID"           {if strVal.length()==0 {strVal = "null"}; returnValue1.mfg_itm_id = strVal  }
                                    
                                else if elemName == "LAST_DTTM_UPDATE"     {if strVal.length()==0 {strVal = "null"};  returnValue1.last_dttm_update  = strVal
                                    returnValue1.days_old = Int16(self.newdate)
                                }
                                else if elemName == "LAST_OPRID"     {if strVal.length()==0 {strVal = "null"}; returnValue1.last_oprid  = strVal}
                                    
                                else if elemName == "QTY_MAXIMUM"          {if strVal.length()==0 {strVal = "null"}; returnValue1.qty_maximum  = strVal}
                                    
                                else if elemName == "DefaultMessage"       {if strVal.length()==0 {strVal = "null"}; returnValue1.defaultMessage = strVal  }
                            }
                            
                          //  print(returnValue1) if strVal.length()==0 {strVal = "null23 "};
                            DispatchQueue.main.async {
                            returnValue.addToCartDetails(returnValue1)
                            }
                     
                        }
                       // print(returnValue.cartDetails)
                    }
                }
            }
        }
    
        
        DispatchQueue.main.async { do {
        
        //    try self.appDelegate.saveContext()
              try self.context.save()
       //     self.allitems.append(returnValue)
        
                print("Saved...! All Items")
        } catch let error as NSError {
            if error.domain == NSCocoaErrorDomain && (error.code == NSValidationNumberTooLargeError ||
            error.code == NSValidationNumberTooSmallError) {
            } else {
                print("Could not save \(error), \(error.userInfo)") }}}
       
        return returnValue
     
    }
    
    public func GetBUCartItemsServiceFromXML(data: Data)-> PS_INV_HEADER
    {
        
        let xmlToParse   = String.init(data: data, encoding: String.Encoding.utf8)!
        // print(xmlToParse)
            return self.GetBUCartItemsServiceFromXMLString( xmlToParse : xmlToParse)

        }
    
    public func GetBUINVCartIDService(UserID:String,Password:String,businessUnit:String,CartID:String,completion :@escaping ()->Void ){
        
        
        //        <soapenv:Envelope xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsa="http://schemas.xmlsoap.org/ws/2003/03/addressing/" xmlns:xsd="http://www.w3.org/2001/XMLSchema/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance/">
        //        <soapenv:Header>
        //        <wsse:Security soap:mustUnderstand="1" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
        //        <wsse:UsernameToken wsu:Id="UsernameToken-1" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
        //        <wsse:Username>VP1</wsse:Username>
        //        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">VP1</wsse:Password>
        //        </wsse:UsernameToken>
        //        </wsse:Security>
        //        </soapenv:Header>
        //        <soapenv:Body>
        //        <Get__CompIntfc__MCG_CART_COUNT_INV xmlns="http://xmlns.oracle.com/Enterprise/Tools/schemas/M935215.V1">
        //        <BUSINESS_UNIT>US015</BUSINESS_UNIT>
        //        <INV_CART_ID>MS-1</INV_CART_ID>
        //        </Get__CompIntfc__MCG_CART_COUNT_INV>
        //        </soapenv:Body>
        //        </soapenv:Envelope>
        
        //  var soapReqXML:String = "<?xml version=\"1.0\"?>"
      /*  var soapReqXML:String = "<soapenv:Envelope xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "<soapenv:Header xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "</soapenv:Header>"
        soapReqXML  += "<soapenv:Body xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        //       soapReqXML  += "<Get__CompIntfc__MCG_CART_COUNT_INV xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/M935215.V1\">"
        soapReqXML  += "<Get__CompIntfc__CART_COUNT_INV xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/M935215.V1\">"
        soapReqXML  += "<BUSINESS_UNIT>"
        soapReqXML  += businessUnit
        soapReqXML  += "</BUSINESS_UNIT>"
        soapReqXML  += "<INV_CART_ID>"
        soapReqXML  += CartID
        soapReqXML  += "</INV_CART_ID>"
        //       soapReqXML  += "</Get__CompIntfc__MCG_CART_COUNT_INV>"
        soapReqXML  += "</Get__CompIntfc__CART_COUNT_INV>"
        soapReqXML  += "</soapenv:Body>"
        soapReqXML  += "</soapenv:Envelope>"
        */
        
        var soapReqXML:String = "<?xml version=\"1.0\"?>"
        soapReqXML  += "<soapenv:Envelope xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "<soapenv:Header xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "</soapenv:Header>"
        soapReqXML  += "<soapenv:Body xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<Get__CompIntfc__CART_COUNT_INV xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/M471410259.V1\">"
        soapReqXML  += "<BUSINESS_UNIT>"
        soapReqXML  += businessUnit
        soapReqXML  += "</BUSINESS_UNIT>"
        soapReqXML  += "<INV_CART_ID>"
        soapReqXML  += CartID
        soapReqXML  += "</INV_CART_ID>"
        soapReqXML  += "</Get__CompIntfc__CART_COUNT_INV>"
        soapReqXML  += "</soapenv:Body>"
        soapReqXML  += "</soapenv:Envelope>"
        
        
        
        
          //   print(soapReqXML)
        // let responseData:Data = SoapHttpClient.callSoapService(Host : urlclass.Host,WebServiceUrl:urlclass.Url_CI_CART_COUNT_INV,SoapAction:urlclass.soapAction_CI_CART_COUNT_INV_G,SoapMessage:soapReqXML)
        
        SoapHttpClient.callWSAsync(Host: urlclass.Host, WebServiceUrl: urlclass.Url_CI_CART_COUNT_INV, SoapAction: urlclass.soapAction_CI_CART_COUNT_INV_G, SoapMessage: soapReqXML) { (err, responseData, str) in
           // print("Errorerr = \(err)")
           // print("ResponseData = \(responseData)")
            // print("str = \(str)")
            
            _ = self.GetBUCartItemsServiceFromXML(data : responseData!)
            completion()
        }
        
    }
    
}

*/


 
 
