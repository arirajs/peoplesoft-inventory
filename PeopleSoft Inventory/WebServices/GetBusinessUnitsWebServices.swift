//
//  GetBusinessUnitsWebServices.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 11/27/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import Foundation
import CoreData
import UIKit

public class GetBusinessUnitsWebServices {
    
    //    deinit {
    //        print("deinit - GetBusinessUnitsWebServices")
    //    }
    
    
    let urlclass = Urldefinations()
    
    var  businessUnit = ""
    var  inv_cart_id = ""
    var  cart_group = ""
    
    /* ADDED 10/11/2021 */
    public func GetBUINVCartIDOnlyFromXMLString(xmlToParse:String)
    {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        let context = appDelegate!.persistentContainer.viewContext
        
    
        print(xmlToParse)
        let xml = SWXMLHash.lazy(xmlToParse)
        let xmlRoot = xml.children.first
        _ = xmlRoot?.children.last
        let xmlResponse: XMLIndexer? = xml.children.first?.children.first //?.children.first
        let xmlResult0: XMLIndexer?  = xmlResponse?.children.last
        var strVal = ""
        var elemName = ""
 
           
        
        if elemName == "" {
            // Property name :
            let itemCount1: Int = (xmlResult0?.children.count)!
            for i1 in 0 ..< itemCount1 {
                let returnValue = NSEntityDescription.insertNewObject(forEntityName: "PS_BUSINESS_UNIT", into: context) as! PS_BUSINESS_UNIT

                
                let xmlResult1:XMLIndexer? = xmlResult0?.children[i1]
                let elem1: XMLElement? =  xmlResult1!.element
                strVal = ""
                if elem1?.children.first is TextElement {
                    let elemText1:TextElement = elem1?.children.first as! TextElement
                    strVal = elemText1.text
                }
                elemName = elem1!.name
             print("1  \(elemName) = \(strVal)")
                if elemName == "CART_COUNT_INV" {
                    // Property name : From
             
                    let itemCount2: Int = (xmlResult1?.children.count)!
                    for i2 in 0 ..< itemCount2 {

                        let xmlResult2:XMLIndexer? = xmlResult1?.children[i2]
                        let elem2: XMLElement? =  xmlResult2!.element
                        strVal = ""
                        if elem2?.children.first is TextElement {
                            let elemText2:TextElement = elem2?.children.first as! TextElement
                            strVal = elemText2.text
                        }
                        elemName = elem2!.name
                      
                        print("2 \(elemName) and \(strVal)" )
                        
                        if elemName == "BUSINESS_UNIT"        {returnValue.businessUnit = strVal }
                        else if elemName == "INV_CART_ID"          {returnValue.inv_cart_id = strVal }
                        else if elemName == "CART_GROUP"       {returnValue.cart_group = strVal }
                          
                     }
                   }
                
                
                do{
                    try context.save()
                }catch let createError{
                    print("Error = \(createError)")
                }
             }
             
          }
         
     }
    
    /* Commented on 10/8/21
    public func GetBUINVCartIDOnlyFromXMLString(xmlToParse:String) {
        
        let xml = SWXMLHash.lazy(xmlToParse)
        let xmlResponse: XMLIndexer? = xml.children.first?.children.first
        let xmlResult0: XMLIndexer?  = xmlResponse?.children.last
        var strVal = ""
        var elemName = ""
        
        //        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //        let context = appDelegate.persistentContainer.viewContext
        //        let returnValue = PS_BUSSINESS_UNIT(context: context)
        
        
     //   var FMDBCartAudit:CartAuditModel = CartAuditModel()
        
        if elemName == "" {
            // Property name :
            let itemCount1: Int = (xmlResult0?.children.count)!
            
            for i1 in 0 ..< itemCount1 {
                
                let xmlResult1:XMLIndexer? = xmlResult0?.children[i1]
                let elem1: XMLElement? =  xmlResult1!.element
                strVal = ""
                if elem1?.children.first is TextElement {
                    let elemText1:TextElement = elem1?.children.first as! TextElement
                    strVal = elemText1.text
                }
                elemName = elem1!.name
                
                //                print("1 \(elemName) and \(strVal)" )
                if elemName == "CART_COUNT_INV" {
                  
                    if elemName == "CART_COUNT_INV" {
                        // if  (returnValue.inv_cart_id != "" && returnValue.cart_group != "" &&   returnValue.businessUnit != ""){
                        if  (inv_cart_id != "" &&   businessUnit != ""){
                            //  DBManager.shared.InsertData(FMDBCartAudit)
                            /* print("inv_cart_id = \(inv_cart_id)")
                            print("cart_group = \(cart_group)")
                            print("businessUnit = \(businessUnit)") */
                            self.savedatatoBusinessUnitCartTable(inv_cart_id: inv_cart_id , cart_group: cart_group , business_unit: businessUnit )
                        }
                    }
                     
                    // Property name : From
                    let itemCount2: Int = (xmlResult1?.children.count)!
                    for i2 in 0 ..< itemCount2 {
                   
                        let xmlResult2:XMLIndexer? = xmlResult1?.children[i2]
                        let elem2: XMLElement? =  xmlResult2!.element
                        strVal = ""
                        if elem2?.children.first is TextElement {
                            let elemText2:TextElement = elem2?.children.first as! TextElement
                            strVal = elemText2.text
                        }
                        elemName = elem2!.name
                        
                        print("2 \(elemName) and \(strVal)" )
                        
                        if elemName == "BUSINESS_UNIT" {
                           // FMDBCartAudit.businessUnit = strVal
                            businessUnit =  strVal
                        }else if elemName == "INV_CART_ID" {
                          //  FMDBCartAudit.inv_cart_id = strVal
                            inv_cart_id =  strVal
                        }else if elemName == "CART_GROUP" {
                            cart_group =  strVal
                        }
                    }
                }
            }
        }
    }
 
    
    public func savedatatoBusinessUnitCartTable(inv_cart_id:String , cart_group:String,business_unit:String ) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "PS_BUSINESS_UNIT", in: context)
        
        let newEntity = NSManagedObject(entity: entity!, insertInto: context)
        
        newEntity.setValue(business_unit, forKey: "businessUnit")
        newEntity.setValue(cart_group, forKey: "cart_group")
        newEntity.setValue(inv_cart_id, forKey: "inv_cart_id")
        
        do{
            try context.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
     */
    public func GetBUINVCartIDOnlyFromXML(data: Data)  {
        let xmlToParse   = String.init(data: data, encoding: String.Encoding.utf8)!
        GetBUINVCartIDOnlyFromXMLString( xmlToParse : xmlToParse)
    }
    
    public func GetBUINVCartIDOnly(UserID:String,Password:String,business_unit:String) {
        
        var soapReqXML  = "<soapenv:Envelope xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "<soapenv:Header xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "</soapenv:Header>"
        soapReqXML  += "<soapenv:Body xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        //        soapReqXML  += "<Find__CompIntfc__MCG_CART_COUNT_INV xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/M263899.V1\">"
        soapReqXML  += "<Find__CompIntfc__CART_COUNT_INV xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/M263899.V1\">"
        soapReqXML  += "<BUSINESS_UNIT>"
        soapReqXML  += business_unit
        soapReqXML  += "</BUSINESS_UNIT>"
        //        soapReqXML  += "</Find__CompIntfc__MCG_CART_COUNT_INV>"
        soapReqXML  += "</Find__CompIntfc__CART_COUNT_INV>"
        soapReqXML  += "</soapenv:Body>"
        soapReqXML  += "</soapenv:Envelope>"
        
        
        //print(soapReqXML)
        // let soapAction :String = "CI_CART_COUNT_INV_F.V1"
     
        let responseData:Data = SoapHttpClient.callWS(Host : urlclass.Host,WebServiceUrl:urlclass.Url_CI_CART_COUNT_INV,SoapAction:urlclass.soapAction_CI_CART_COUNT_INV_F,SoapMessage:soapReqXML)
        GetBUINVCartIDOnlyFromXML(data : responseData)
    }
}
