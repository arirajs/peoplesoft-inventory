//
//  UpdateCartCountQtyService.swift
//  PeopleSoft Inventory 2018 V1
//
//  Created by Ariraj Shanmugavelu on 4/8/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import Foundation
import CoreData

public class UpdateCartCountQtyService {
//
//    
//    deinit {
//        print("deinit - UpdateCartCountQtyService")
//    }
    
    let urlclass = Urldefinations()
    public func dataToBase64(data:NSData)->String{
        
        let result = data.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
        return result;
    }
    public func dataToBase64(data: Data)->String {
        let result = data.base64EncodedString()
        return result
    }
    public func byteArrayToBase64(data:[UInt])->String{
        let nsdata = NSData(bytes: data, length: data.count)
        let data  = Data.init(referencing: nsdata)
        if let str = String.init(data: data, encoding: String.Encoding.utf8){
            return str
        }
        return "";
    }
    public func timeToString(date:Date)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    public func dateToString(date:Date)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    public func base64ToByteArray(base64String: String) -> [UInt8] {
        let data = Data.init(base64Encoded: base64String)
        let dataCount = data!.count
        var bytes = [UInt8].init(repeating: 0, count: dataCount)
        data!.copyBytes(to: &bytes, count: dataCount)
        return bytes
    }
    
   
    func stringArrFromXMLString(xmlToParse :String)->[String?]{
        let xml  = SWXMLHash.lazy(xmlToParse)
        let xmlRoot  = xml.children.first
        let xmlBody = xmlRoot?.children.last
        let xmlResponse : XMLIndexer? =  xmlBody?.children.first
        let xmlResult : XMLIndexer?  = xmlResponse?.children.last
        
        var strList = [String?]()
        let childs = xmlResult!.children
        for child in childs {
            let text = child.element?.text
            strList.append(text)
        }
        
        return strList
    }
    
    func stringArrFromXML(data:Data)->[String?]{
        let xmlToParse :String? = String.init(data: data, encoding: String.Encoding.utf8)
        if xmlToParse == nil {
            return [String?]()
        }
        if xmlToParse?.count==0 {
            return [String?]()
        }
        
        let  stringVal = stringArrFromXMLString(xmlToParse:  xmlToParse!)
        return stringVal
    }
    
    func base64ToByteArray(base64String: String) -> [UInt8]? {
        if let data = Data.init(base64Encoded: base64String){
            var bytes = [UInt8](repeating: 0, count: data.count)
            data.copyBytes(to: &bytes, count: data.count)
            return bytes;
        }
        return nil // Invalid input
    }
    
    public func UpdateCartQtyFromXMLString(xmlToParse:String) -> PS_INV_HEADER
    {
        let xml = SWXMLHash.lazy(xmlToParse)
        let xmlRoot = xml.children.first
        _ = xmlRoot?.children.last
//        let xmlResponse: XMLIndexer? = xml.children.first?.children.first?.children.first
//        let xmlResult0: XMLIndexer?  = xmlResponse?.children.last
//        var strVal = ""
//        var elemName = ""
        // let returnValue:CartCountHeaderTable = CartCountHeaderTable()
       // let CoreHand = CoreDataHandler()
        let handler =  CoreDataHelper().getContext()
        let returnValue:PS_INV_HEADER =  NSEntityDescription.insertNewObject(forEntityName: "PS_INV_HEADER", into: handler) as! PS_INV_HEADER
                         
        return returnValue
    }
    
    public func UpdateCartQtyFromXML(data: Data)-> PS_INV_HEADER {
        
        let xmlToParse   = String.init(data: data, encoding: String.Encoding.utf8)!
        //  print(xmlToParse)
        return UpdateCartQtyFromXMLString( xmlToParse : xmlToParse)
    }
    
  //  public func UpdateCartQty(UserID:String,Password:String,businessUnit:String,CartID:String,invitemid:String,compartment:String,cartcountid:String,countorder:String,cartcountqty:String,qtyOption:String,include_flag:String,completion :@escaping (String)->Void ){
    public func UpdateCartQty(UserID:String,Password:String,businessUnit:String,CartID:String,qtyOption:String,
 avg_dlft_option:String, round_option:String,   cart_count_id:String, par_count_id:String, cart_count_status:String,
 status_descr:String, allitems:String , completion :@escaping (String)->Void ){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd'T'HH:mm"
        let formated_date = formatter.string(from: date)
    //    print(formated_date)
        
        
   /*     var soapReqXML:String =  "<soapenv:Envelope xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "<soapenv:Header xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "</soapenv:Header>"
        soapReqXML  += "<soapenv:Body xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<Updatedata__CompIntfc__CART_COUNT_INV xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/M208890041.V1\">"
      
         
        var soapReqXML:String = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"http://xmlns.oracle.com/Enterprise/FSCM/schema/M452061.V1\">"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "<soapenv:Header/>"
        soapReqXML  += "<soapenv:Body>"
        soapReqXML  += "<Updatedata__CompIntfc__CART_COUNT_INV>"
         */
        
//   10/15/2020 IMPORTANT     <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:m20="http://xmlns.oracle.com/Enterprise/Tools/schemas/M208890041.V1">
//           <soapenv:Header/>
//           <soapenv:Body>
//              <Updatedata__CompIntfc__CART_COUNT_INV xmlns="http://xmlns.oracle.com/Enterprise/Tools/schemas/M208890041.V1">
//                 <BUSINESS_UNIT>INV01</BUSINESS_UNIT>
//                 <INV_CART_ID>2E PERSONAL CAR</INV_CART_ID>
//                 <DESCR>WOODLAND PINE</DESCR>
//                 <CART_GROUP>HRHS</CART_GROUP>
//                 <SYNCID>93</SYNCID>
//                 <SYNCDTTM>2012-07-10T11.20.29.453000</SYNCDTTM>
//                 <AVG_DFLT_OPTION>N</AVG_DFLT_OPTION>
//                 <ROUND_OPTION>02</ROUND_OPTION>
//                 <QTY_OPTION>02</QTY_OPTION>
//                 <CART_COUNT_ID>0</CART_COUNT_ID>
//                 <PAR_COUNT_ID>NEXT</PAR_COUNT_ID>
//                 <CART_COUNT_STATUS>1</CART_COUNT_STATUS>
//                 <STATUS_DESCR>Unprocessed</STATUS_DESCR>
//                 <CART_CT_UPD_VW>
//                    <BUSINESS_UNIT>INV01</BUSINESS_UNIT>
//                    <INV_CART_ID>2E PERSONAL CAR</INV_CART_ID>
//                    <INV_ITEM_ID>000000000000000511</INV_ITEM_ID>
//                    <COMPARTMENT/>
//                    <COUNT_ORDER>1</COUNT_ORDER>
//                    <SUFFICIENT_STOCK>N</SUFFICIENT_STOCK>
//                    <CART_COUNT_QTY>07</CART_COUNT_QTY>
//                    <UNIT_OF_MEASURE>CA</UNIT_OF_MEASURE>
//                    <QTY_OPTIMAL>1</QTY_OPTIMAL>
//                    <AVG_CART_USAGE>0</AVG_CART_USAGE>
//                    <LAST_OPRID>VP5</LAST_OPRID>
//                    <LAST_DTTM_UPDATE>2020-10-12-20.47.59.000000</LAST_DTTM_UPDATE>
//                    <CART_REPLEN_OPT>01</CART_REPLEN_OPT>
//                    <COUNT_REQUIRED>N</COUNT_REQUIRED>
//                    <QTY_MAXIMUM>0</QTY_MAXIMUM>
//                    <INCLUDE_FLG>Y</INCLUDE_FLG>
//                 </CART_CT_UPD_VW>
//              </Updatedata__CompIntfc__CART_COUNT_INV>
//           </soapenv:Body>
//        </soapenv:Envelope>
        
        
      var soapReqXML:String = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"http://xmlns.oracle.com/Enterprise/FSCM/schema/M452061.V1\">"
        soapReqXML  += "<soapenv:Header/>"
        soapReqXML  += "<soapenv:Body>"
        // soapReqXML  += "<Updatedata__CompIntfc__MCG_CART_COUNT_INV>"
        soapReqXML  += "<Updatedata__CompIntfc__CART_COUNT_INV>"
         
    
        soapReqXML  += "<BUSINESS_UNIT>"
        soapReqXML  += businessUnit
        soapReqXML  += "</BUSINESS_UNIT>"
        
        soapReqXML  += "<INV_CART_ID>"
        soapReqXML  += CartID
        soapReqXML  += "</INV_CART_ID>"
        
        soapReqXML  += "<CART_GROUP>HRHS</CART_GROUP>"
        //OCT15
        // soapReqXML  += "<CART_COUNT_ID>NEXT</CART_COUNT_ID>"
        // soapReqXML  += "<CART_COUNT_STATUS>1</CART_COUNT_STATUS>"
                
       // OCT16
        soapReqXML  += "<AVG_DFLT_OPTION>"
        soapReqXML  += avg_dlft_option
        soapReqXML  += "</AVG_DFLT_OPTION>"
 
        soapReqXML  += "<ROUND_OPTION>"
        soapReqXML  += round_option
        soapReqXML  += "</ROUND_OPTION>"
        
        soapReqXML  += "<PAR_COUNT_ID>"
        soapReqXML  += par_count_id
        soapReqXML  += "</PAR_COUNT_ID>"
        
        soapReqXML  += "<QTY_OPTION>"
        soapReqXML  += qtyOption
        soapReqXML  += "</QTY_OPTION>"
        
//        soapReqXML  += "<CART_COUNT_ID>"
//        soapReqXML  += cart_count_id
//        soapReqXML  += "</CART_COUNT_ID>"
        
        soapReqXML  += "<OPRID>"
        soapReqXML  += UserID
        soapReqXML  += "</OPRID>"
        
        soapReqXML  += "<CART_COUNT_STATUS>"
        soapReqXML  += cart_count_status
        soapReqXML  += "</CART_COUNT_STATUS>"
        
//        soapReqXML  += "<STATUS_DESCR>"
//        soapReqXML  += status_descr
//        soapReqXML  += "</STATUS_DESCR>"
        
        

        
        soapReqXML  += allitems
        
        /*
        soapReqXML  += "<CART_CT_UPD_VW>"
        soapReqXML  += "<BUSINESS_UNIT>"
        soapReqXML  += businessUnit
        soapReqXML  += "</BUSINESS_UNIT>"
        soapReqXML  += "<INV_CART_ID>"
        soapReqXML  += CartID
        soapReqXML  += "</INV_CART_ID>"
        soapReqXML  += "<INV_ITEM_ID>"
        soapReqXML  += invitemid
        soapReqXML  += "</INV_ITEM_ID>"
        soapReqXML  += "<CART_COUNT_QTY>"
        soapReqXML  += cartcountqty
        soapReqXML  += "</CART_COUNT_QTY>"
        
        
        soapReqXML  += "<LAST_OPRID>"
        soapReqXML  += UserID
        soapReqXML  += "</LAST_OPRID>"
        soapReqXML  += "<LAST_DTTM_UPDATE>"
        soapReqXML  += "\(formated_date)"
        soapReqXML  += "</LAST_DTTM_UPDATE>"
        
        
        soapReqXML  += "<INCLUDE_FLG>"
        soapReqXML  += include_flag
        soapReqXML  += "</INCLUDE_FLG>"
        soapReqXML  += "</CART_CT_UPD_VW>"
        */
        
     //   soapReqXML  += "</Updatedata__CompIntfc__MCG_CART_COUNT_INV>"
        
        soapReqXML  += "</Updatedata__CompIntfc__CART_COUNT_INV>"
        soapReqXML  += "</soapenv:Body>"
        soapReqXML  += "</soapenv:Envelope>"
        
        
        // let soapAction :String = "CI_CART_COUNT_INV_UD.V1"
        // let soapAction :String = "CI_ZZ_CART_COUNT_INV_UD.V2"
        
//        let responseData:Data = SoapHttpClient.callSoapService(Host : urlclass.Host,WebServiceUrl:urlclass.Url_CI_CART_COUNT_INV,SoapAction:urlclass.soapAction_CI_ZZ_CART_COUNT_INV_UD,SoapMessage:soapReqXML)
//
//        let returnValue:CIcartcountHeaderTable = UpdateCartQtyFromXML(data : responseData)
//
//        return returnValue
        
// print("soapReqXML1 = \(soapReqXML)")
        SoapHttpClient.callWSAsync(Host: urlclass.Host, WebServiceUrl: urlclass.Url_CI_CART_COUNT_INV, SoapAction: urlclass.soapAction_CI_ZZ_CART_COUNT_INV_UD, SoapMessage: soapReqXML) { (err, responseData, str) in
            
            if  err == nil {
                _ = self.UpdateCartQtyFromXML(data : responseData!)
                
                completion("ONLINE")
            }   else{

//                print("Error = \(err)")
//                print("responseData = \(responseData)")
//                print("str = \(str)")
//                print("local = \(err?.localizedDescription)")
                completion("OFFLINE")
            }
            
            
//            self.UpdateCartQtyFromXML(data : responseData!)
//                      completion()
//
      }
    }
}
