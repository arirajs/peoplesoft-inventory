//
//  getNewCartCountID.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 11/28/20.
//  Copyright © 2020 Ariraj Shanmugavelu. All rights reserved.
//

import Foundation
import CoreData


public class getNewParCountID {
    
    var allitems = [PS_INV_HEADER]()
    let urlclass = Urldefinations()
    
    // Date Compare
    
    let calendar = Calendar.current
    let asofdate = Date()
    var newdate:Int = 0
    
    
    public func getNewParCountIDFromXMLString(xmlToParse:String)
    {
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        let mainQueueContext = appDelegate!.persistentContainer.viewContext
        let privateQueueCOntext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        
        privateQueueCOntext.parent = mainQueueContext
        
        let xml = SWXMLHash.lazy(xmlToParse)
        let xmlRoot = xml.children.first
        _ = xmlRoot?.children.last
        let xmlResponse: XMLIndexer? = xml.children.first // ?.children.first //?.children.first
        let xmlResult0: XMLIndexer?  = xmlResponse?.children.last
        var strVal = ""
        var elemName = ""
        var currentBU = ""
        var currentINVcartid = ""
        
   
            
        //    let returnValue = PS_INV_HEADER (context: privateQueueCOntext)
            
            if elemName == "" {
                // Property name :
                let itemCount1: Int = (xmlResult0?.children.count)!
                for i1 in 0 ..< itemCount1 {
                    let xmlResult1:XMLIndexer? = xmlResult0?.children[i1]
                    let elem1: XMLElement? =  xmlResult1!.element
                    strVal = ""
                    if elem1?.children.first is TextElement {
                        let elemText1:TextElement = elem1?.children.first as! TextElement
                        strVal = elemText1.text
                    }
                    elemName = elem1!.name
                    if elemName == "Get__CompIntfc__CART_COUNT_INVResponse" {
                        // Property name : From
                        let itemCount2: Int = (xmlResult1?.children.count)!
                        for i2 in 0 ..< itemCount2 {
                            let xmlResult2:XMLIndexer? = xmlResult1?.children[i2]
                            let elem2: XMLElement? =  xmlResult2!.element
                            strVal = ""
                            if elem2?.children.first is TextElement {
                                let elemText2:TextElement = elem2?.children.first as! TextElement
                                strVal = elemText2.text
                            }
                            elemName = elem2!.name

                            if elemName == "BUSINESS_UNIT"             {if strVal.length()==0 {strVal = "null"}; currentBU = strVal}
                            else if elemName == "INV_CART_ID"          {if strVal.length()==0 {strVal = "null"}; currentINVcartid = strVal}
                            else if elemName == "DESCR"                {if strVal.length()==0 {strVal = "null"}; }
                            else if elemName == "CART_GROUP"           {if strVal.length()==0 {strVal = "null"}; }
                            else if elemName == "SYNCID"               {if strVal.length()==0 {strVal = "null"}; }
                            else if elemName == "SYNCDTTM"             {if strVal.length()==0 {strVal = "null"}; }
                            else if elemName == "AVG_DFLT_OPTION"      {if strVal.length()==0 {strVal = "null"}; }
                            else if elemName == "ROUND_OPTION"         {if strVal.length()==0 {strVal = "null"}; }
                            else if elemName == "QTY_OPTION"           {if strVal.length()==0 {strVal = "null"}; }
                            else if elemName == "CART_COUNT_ID"        {if strVal.length()==0 {strVal = "null"}; updateNewCartcountid(newParcountid: strVal, business_unit: currentBU, inv_cart_id: currentINVcartid) }
                            else if elemName == "PAR_COUNT_ID"         {if strVal.length()==0 {strVal = "null"}; }
                            else if elemName == "CART_COUNT_STATUS"    {if strVal.length()==0 {strVal = "null"}; }
                            else if elemName == "STATUS_DESCR"         {if strVal.length()==0 {strVal = "null"}; }
                            else if elemName == "OPRID"                {if strVal.length()==0 {strVal = "null"}; }
                            
                        }
                    }
                }
            }
            
            
        
    }
    
    public func getNewParCountIDFromXML(data: Data)
    {
        let xmlToParse   = String.init(data: data, encoding: String.Encoding.utf8)!
        return self.getNewParCountIDFromXMLString( xmlToParse : xmlToParse)
        
    }
    
    public func GetNewParcountService(UserID:String,Password:String,businessUnit:String,CartID:String,completion :@escaping (String)->Void){
        
        var soapReqXML:String = "<?xml version=\"1.0\"?>"
        soapReqXML  += "<soapenv:Envelope xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "<soapenv:Header xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "</soapenv:Header>"
        soapReqXML  += "<soapenv:Body xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<Get__CompIntfc__CART_COUNT_INV xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/M471410259.V1\">"
        soapReqXML  += "<BUSINESS_UNIT>"
        soapReqXML  += businessUnit
        soapReqXML  += "</BUSINESS_UNIT>"
        soapReqXML  += "<INV_CART_ID>"
        soapReqXML  += CartID
        soapReqXML  += "</INV_CART_ID>"
        soapReqXML  += "</Get__CompIntfc__CART_COUNT_INV>"
        soapReqXML  += "</soapenv:Body>"
        soapReqXML  += "</soapenv:Envelope>"
        
        SoapHttpClient.callWSAsync(Host: urlclass.Host, WebServiceUrl: urlclass.Url_CI_CART_COUNT_INV, SoapAction: urlclass.soapAction_CI_CART_COUNT_INV_G, SoapMessage: soapReqXML) { (err, responseData, str) in
            
        self.getNewParCountIDFromXML(data : responseData!)
       completion("NEWPARID")
        }
    }
    
    func updateNewCartcountid(newParcountid:String, business_unit:String, inv_cart_id:String ){
        //   print("updateQuantityOptions")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_INV_HEADER")
        
        fetchRequest.predicate = NSPredicate(format: "business_unit = %@ AND inv_cart_id = %@  ", business_unit, inv_cart_id)
        
        do {
            let results = try context.fetch(fetchRequest) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned
                results![0].setValue(newParcountid, forKey: "par_count_id")
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try context.save()
        }
        catch {
            print("Saving Core Data Failed1: \(error)")
        }
    }
    
}
