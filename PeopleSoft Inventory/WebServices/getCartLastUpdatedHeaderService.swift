//
//  getCartLastUpdatedHeaderService.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 1/27/21.
//  Copyright © 2021 Ariraj Shanmugavelu. All rights reserved.
//

import Foundation 
import CoreData
import UIKit

public class getCartLastUpdatedHeaderService {
    
    let urlclass = Urldefinations()
    
    // Date Compare
    
    let calendar = Calendar.current
    let asofdate = Date()
    var newdate:Int = 0
    
    public func getCartLastUpdatedHEADERServiceFromXMLString(xmlToParse:String)
    {
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        let mainQueueContext = appDelegate!.persistentContainer.viewContext
        let privateQueueCOntext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        
        privateQueueCOntext.parent = mainQueueContext
            
        let xml = SWXMLHash.lazy(xmlToParse)
        let xmlRoot = xml.children.first
        _ = xmlRoot?.children.last
        let xmlResponse: XMLIndexer? = xml.children.first?.children.first?.children.first
        let xmlResult0: XMLIndexer?  = xmlResponse?.children.last
        var strVal = ""
        var elemName = ""
 
        
       privateQueueCOntext.performAndWait {
          
       
        
        if elemName == "" {
 
            let itemCount1: Int = (xmlResult0?.children.count)!
            for i1 in 0 ..< itemCount1 {
                let xmlResult1:XMLIndexer? = xmlResult0?.children[i1]
                let elem1: XMLElement? =  xmlResult1!.element
                strVal = ""
                if elem1?.children.first is TextElement {
                    let elemText1:TextElement = elem1?.children.first as! TextElement
                    strVal = elemText1.text
                }
                elemName = elem1!.name
             //   print("1  \(elemName) = \(strVal)")
                if elemName == "row" {
                    // Property name : From
                    let itemCount2: Int = (xmlResult1?.children.count)!
                    let returnValue = PS_CART_COUNT_H_HIST (context: privateQueueCOntext)

                    for i2 in 0 ..< itemCount2 {
                        let xmlResult2:XMLIndexer? = xmlResult1?.children[i2]
                        let elem2: XMLElement? =  xmlResult2!.element
                        strVal = ""
                        if elem2?.children.first is TextElement {
                            let elemText2:TextElement = elem2?.children.first as! TextElement
                            strVal = elemText2.text
                        }
                        elemName = elem2!.name
                        //   print("2 \(elemName) = \(strVal)")
                        if elemName == "BUSINESS_UNIT"        {returnValue.business_unit = strVal }
                        else if elemName == "INV_CART_ID"          {returnValue.inv_cart_id = strVal }
                        else if elemName == "LAST_OPRID"                {if strVal.length()==0 {strVal = "null"}; returnValue.last_oprid = strVal  }
                        else if elemName == "DEMAND_DATE"           {if strVal.length()==0 {strVal = "Pending"}; returnValue.demand_date = strVal}
                        else if elemName == "CART_COUNT_ID"           {if strVal.length()==0 {strVal = "null"}; returnValue.cart_count_id = strVal}
                      
                       // print(returnValue.cartDetails)
                    }
                }
            }
        }
    
  
        DispatchQueue.main.async { do {
        
        //    try self.appDelegate.saveContext()
              try privateQueueCOntext.save()
       //     self.allitems.append(returnValue)
        
//                print("Saved...! All Items")
        } catch let error as NSError {
            if error.domain == NSCocoaErrorDomain && (error.code == NSValidationNumberTooLargeError ||
            error.code == NSValidationNumberTooSmallError) {
            } else {
                print("Could not save \(error), \(error.userInfo)") }}}
       
      //  return returnValue
        }
    }
    
    public func getCartLastUpdatedHEADERServiceFromXML(data: Data)
    {
        
        let xmlToParse   = String.init(data: data, encoding: String.Encoding.utf8)!
        // print(xmlToParse)
            return self.getCartLastUpdatedHEADERServiceFromXMLString( xmlToParse : xmlToParse)

        }
    
    public func getCartLastUpdatedHEADERServiceFunc(UserID:String,Password:String,completion :@escaping ()->Void ){
        
     /*   var soapReqXML:String = "<?xml version=\"1.0\"?>"
        soapReqXML  += "<soapenv:Envelope xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "<soapenv:Header xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "</soapenv:Header>"
        soapReqXML  += "<soapenv:Body xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<Get__CompIntfc__CART_COUNT_INV xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/M471410259.V1\">"
        soapReqXML  += "<BUSINESS_UNIT>"
        soapReqXML  += businessUnit
        soapReqXML  += "</BUSINESS_UNIT>"
        soapReqXML  += "<INV_CART_ID>"
        soapReqXML  += CartID
        soapReqXML  += "</INV_CART_ID>"
        soapReqXML  += "</Get__CompIntfc__CART_COUNT_INV>"
        soapReqXML  += "</soapenv:Body>"
        soapReqXML  += "</soapenv:Envelope>"
        */
        
        /*
        var soapReqXML:String = "<?xml version=\"1.0\"?>"
        soapReqXML  += "<soapenv:Envelope xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "<soapenv:Header xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "</soapenv:Header>"
        soapReqXML  += "<soapenv:Body xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<QAS_EXEQRY_SYNC_REQ_MSG xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/QAS_EXEQRY_SYNC_REQ_MSG.VERSION_1\">"
        soapReqXML  += "<QAS_EXEQRY_SYNC_REQ xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/QAS_EXEQRY_SYNC_REQ.VERSION_1\">"
        soapReqXML  += "<QueryName>ZZ_CART_CT_INF_INV</QueryName>"
        soapReqXML  += "<isConnectedQuery>N</isConnectedQuery>"
        soapReqXML  += "<OwnerType>PUBLIC</OwnerType>"
        soapReqXML  += "<BlockSizeKB>0</BlockSizeKB>"
        soapReqXML  += "<MaxRow>500</MaxRow>"
        soapReqXML  += "<OutResultType>XMLP</OutResultType>"
        soapReqXML  += "<OutResultFormat>NONFILE</OutResultFormat>"
        soapReqXML  += "</QAS_EXEQRY_SYNC_REQ>"
        soapReqXML  += "</QAS_EXEQRY_SYNC_REQ_MSG>"
        soapReqXML  += "</soapenv:Body>"
        soapReqXML  += "</soapenv:Envelope>"
        */
        
        var soapReqXML:String = "<?xml version=\"1.0\"?>"
        soapReqXML  += "<soapenv:Envelope xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "<soapenv:Header>"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "</soapenv:Header>"
        soapReqXML  += "<soapenv:Body>"
        soapReqXML  += "<QAS_EXEQRY_SYNC_REQ_MSG xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/QAS_EXEQRY_SYNC_REQ_MSG.VERSION_1\">"
        soapReqXML  += "<QAS_EXEQRY_SYNC_REQ xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/QAS_EXEQRY_SYNC_REQ.VERSION_1\">"
        soapReqXML  += "<QueryName>MCG_CART_COUNT_H_HIST</QueryName>"
        soapReqXML  += "<isConnectedQuery>N</isConnectedQuery>"
        soapReqXML  += "<OwnerType>PUBLIC</OwnerType>"
        soapReqXML  += "<BlockSizeKB>0</BlockSizeKB>"
        soapReqXML  += "<MaxRow>500</MaxRow>"
        soapReqXML  += "<OutResultType>XMLP</OutResultType>"
        soapReqXML  += "<OutResultFormat>NONFILE</OutResultFormat>"
        soapReqXML  += "</QAS_EXEQRY_SYNC_REQ>"
        soapReqXML  += "</QAS_EXEQRY_SYNC_REQ_MSG>"
        soapReqXML  += "</soapenv:Body>"
        soapReqXML  += "</soapenv:Envelope>"
        /*
       print(urlclass.Host)
        print(urlclass.Url_QAS_QRY_SERVICE)
         print(urlclass.soapAction_QAS_EXECUTEQRYSYNC_OPER)
         print("soapReqXML = \(soapReqXML) ")
        */
        
       
        SoapHttpClient.callWSAsync(Host: urlclass.Host, WebServiceUrl: urlclass.Url_QAS_QRY_SERVICE, SoapAction: urlclass.soapAction_QAS_EXECUTEQRYSYNC_OPER, SoapMessage: soapReqXML) { (err, responseData, str) in
          
             self.getCartLastUpdatedHEADERServiceFromXML(data : responseData!)
            completion()
        }
        
    }
    
}
