//
//  getUserPreferenceWebServices.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 11/25/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import Foundation
import CoreData
import UIKit

public class getUserPreferenceWebServices {
    
//    deinit {
//        print("deinit - getUserPreferenceWebServices")
//    }
    
    let urlclass = Urldefinations()
    
    public func UserProfileFromXMLString(xmlToParse:String)-> PS_OPR_DEF_TBL_FS  {
        
        let xml = SWXMLHash.lazy(xmlToParse)
        
        let xmlResponse: XMLIndexer? = xml.children.first?.children.first
        let xmlResult0: XMLIndexer?  = xmlResponse?.children.last
        var strVal = ""
        var elemName = ""
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let returnValue = PS_OPR_DEF_TBL_FS(context: context)
        
        if elemName == "" {
            // Property name :
            let itemCount1: Int = (xmlResult0?.children.count)!
            
            //   print(itemCount1)
            // print(xmlResponse)
            
            for i1 in 0 ..< itemCount1 {
                //    for elem1 in xml["MIN_CCI_HDR_DVW"].all
                
                // print(elem1["DESCR"].element!.text!)
                
                let xmlResult1:XMLIndexer? = xmlResult0?.children[i1]
                let elem1: XMLElement? =  xmlResult1!.element
                strVal = ""
                if elem1?.children.first is TextElement {
                    let elemText1:TextElement = elem1?.children.first as! TextElement
                    strVal = elemText1.text
                    
                    //  print(strVal)
                    
                }
                elemName = elem1!.name
 
               //  print("\(elemName) = \(strVal)")
                
                if elemName == "OPRID" {
                    returnValue.oprid =  strVal
                }else if elemName == "BUSINESS_UNIT" {
                    returnValue.business_unit =  strVal
                }else if elemName == "NAME1" {
                    returnValue.name1 =  strVal
                }else if elemName == "SETID" {
                    returnValue.setid =  strVal
                }else if elemName == "AS_OF_DATE" {
                    //returnValue.as_of_date =  strVal.toDate()!
                }
            }
        }
        
        DispatchQueue.main.async { do {
            print("Saved...!@UserProfileFromXMLString")
            try context.save()
        } catch {
            let nserror = error as NSError
            print("Unresolved error \(nserror), \(nserror.userInfo)")
            // print("Failed saving")
            } }
        return returnValue
    }
    
    
    public func UserProfileFromXML(data: Data)-> PS_OPR_DEF_TBL_FS {
        
        let xmlToParse   = String.init(data: data, encoding: String.Encoding.utf8)!
        // print(xmlToParse)
        return UserProfileFromXMLString( xmlToParse : xmlToParse)
    }
    
    public func GetUserProfileDefault(UserID:String,Password:String)->PS_OPR_DEF_TBL_FS{
        
        var soapReqXML:String = "<?xml version=\"1.0\"?>"
        soapReqXML  += "<soapenv:Envelope xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "<soapenv:Header xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "</soapenv:Header>"
        soapReqXML  += "<soapenv:Body xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<Get__CompIntfc__OPR_DEFAULT_FIN xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/M1042975.V1\">"
        soapReqXML  += "<OPRID>"
        soapReqXML  += UserID
        soapReqXML  += "</OPRID>"
        soapReqXML  += "</Get__CompIntfc__OPR_DEFAULT_FIN>"
        soapReqXML  += "</soapenv:Body>"
        soapReqXML  += "</soapenv:Envelope>"
        
        // let soapAction :String = "CI_OPR_DEFAULT_FIN_G.V1"
        
        let responseData:Data = SoapHttpClient.callWS(Host : urlclass.Host,WebServiceUrl:urlclass.Url_CI_OPR_DEFAULT_FIN,SoapAction:urlclass.soapAction_CI_OPR_DEFAULT_FIN_G,SoapMessage:soapReqXML)
        
        let returnValue:PS_OPR_DEF_TBL_FS = UserProfileFromXML(data : responseData)
        
        return returnValue
    }
    
}

