//
//  GetBUCartitemsHistoryWebServices.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 12/1/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import Foundation
import CoreData
import UIKit

public class GetBUCartitemsHistoryWebServices {
    
//    deinit {
//        print("deinit - GetBUCartitemsHistoryWebServices")
//    }
    
    
    // var managedContext: NSManagedObjectContext!
    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var currentHeader: PS_CART_CT_INF_INV?
    
    let urlclass = Urldefinations()
    
    var avg_cart_usage = ""
    var business_unit = ""
    var cart_count_id = ""
    var cart_count_qty = ""
    var cart_count_status = ""
    var cart_replen_opt = ""
    var compartment = ""
    var count_order = ""
    var count_required = ""
    var defaultMessage = ""
    var descr60 = ""
    var include_flg = ""
    var inv_cart_id = ""
    var inv_item_id = ""
    var last_dttm_update = ""
    var last_oprid = ""
    var mfg_itm_id = ""
    var qty_maximum = ""
    var qty_optimal = ""
    var sufficient_stock = ""
    var unit_of_measure = ""
    var transfer_cost = ""
    var price_markup_pct = ""
    var charge_code = ""
    var process_instance = ""
    var transfer_calc_type = ""
    
    public func GetBUCartItemsHistoryServiceFromXMLString(xmlToParse:String) ->  PS_CART_CT_INF_INV
    {
        
        let xml = SWXMLHash.lazy(xmlToParse)
        let xmlRoot = xml.children.first
        _ = xmlRoot?.children.last
        let xmlResponse: XMLIndexer? = xml.children.first?.children.first//?.children.first
        let xmlResult0: XMLIndexer?  = xmlResponse?.children.last
        var strVal = ""
        var elemName = ""
        // let returnValue:PS_CART_CT_INF_INV  = PS_CART_CT_INF_INV ()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        //    let returnValue:PS_CART_CT_INF_INV  =  NSEntityDescription.insertNewObject(forEntityName: "PS_CART_CT_INF_INV ", into: context) as! PS_CART_CT_INF_INV
        let returnValue = PS_CART_CT_INF_INV(context: context)
        
        
        if elemName == "" {
            // Property name :
            let itemCount1: Int = (xmlResult0?.children.count)!
            for i1 in 0 ..< itemCount1 {
                let xmlResult1:XMLIndexer? = xmlResult0?.children[i1]
                let elem1: XMLElement? =  xmlResult1!.element
                strVal = ""
                if elem1?.children.first is TextElement {
                    let elemText1:TextElement = elem1?.children.first as! TextElement
                    strVal = elemText1.text
                }
                elemName = elem1!.name
                
//                print("1  \(elemName) = \(strVal)")
//               if elemName == "CART_CT_INF_VW" {
//
                   if elemName == "CART_CT_INF_VW" {

                                  if  (inv_cart_id != " "  &&   business_unit != " "){
                            
//                            print("-------------------")
//                            print(inv_cart_id)
//                            print(business_unit)
//                            print("-------------------")
                            self.savedatatoBusinessUnitCartHistoryTable(inv_cart_id: inv_cart_id, business_unit: business_unit)
                        }
                    }
            
                    // Property name : From
                    let itemCount2: Int = (xmlResult1?.children.count)!
                    for i2 in 0 ..< itemCount2 {
                        let xmlResult2:XMLIndexer? = xmlResult1?.children[i2]
                        let elem2: XMLElement? =  xmlResult2!.element
                        strVal = ""
                        if elem2?.children.first is TextElement {
                            let elemText2:TextElement = elem2?.children.first as! TextElement
                            strVal = elemText2.text
                        }
                        elemName = elem2!.name
                       // print("2 \(elemName) = \(strVal)")
                        //  print(elemName)
                        if elemName == "BUSINESS_UNIT"        { business_unit = strVal }
                        else if elemName == "INV_CART_ID"          {inv_cart_id = strVal }
                        else if elemName == "CART_COUNT_ID"                {cart_count_id = strVal }
                        else if elemName == "INV_ITEM_ID"               {inv_item_id = strVal}
                        else if elemName == "COMPARTMENT"             { compartment = strVal}
                        else if elemName == "CART_COUNT_QTY"      { cart_count_qty = strVal}
                        else if elemName == "UNIT_OF_MEASURE"         { unit_of_measure = strVal}
                        else if elemName == "QTY_OPTIMAL"           { qty_optimal = strVal}
                        else if elemName == "DISTRIB_TYPE"        {cart_count_id = strVal}
                        else if elemName == "TRANSFER_COST"         { transfer_cost = strVal}
                        else if elemName == "PRICE_MARKUP_PCT"    { price_markup_pct = strVal}
                        else if elemName == "CHARGE_CODE"         {charge_code = strVal}
                        else if elemName == "LAST_OPRID"                {last_oprid = strVal }
                        else if elemName == " CART_COUNT_STATUS"                {cart_count_status = strVal }
                        else if elemName == " PROCESS_INSTANCE"                {process_instance = strVal }
                        else if elemName == " SUFFICIENT_STOCK"                {sufficient_stock = strVal }
                        else if elemName == " CART_REPLEN_OPT"                {cart_replen_opt = strVal }
                        else if elemName == " LAST_DTTM_UPDATE"                {last_dttm_update = strVal }
                        
                    }
                    
//                }
//                DispatchQueue.main.async { do {
//                    print("Saved...!")
//                    try context.save()
//                } catch {
//                    let nserror = error as NSError
//                    print("Unresolved error \(nserror), \(nserror.userInfo)")
//                    // print("Failed saving")
//                    } }
                
            }
        }
        
        
        return returnValue
    }
    
    public func savedatatoBusinessUnitCartHistoryTable(inv_cart_id:String , business_unit:String ) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "PS_CART_CT_INF_INV", in: context)
        
        let newEntity = NSManagedObject(entity: entity!, insertInto: context)
        
       // newEntity.setValue(business_unit, forKey: "business_unit")
       // newEntity.setValue(inv_cart_id, forKey: "inv_cart_id")
        newEntity.setValue(avg_cart_usage     ,forKey:"avg_cart_usage")
        newEntity.setValue(business_unit,forKey:"business_unit")
        newEntity.setValue(cart_count_id,forKey:"cart_count_id")
        newEntity.setValue(cart_count_qty,forKey:"cart_count_qty")
        newEntity.setValue(cart_count_status,forKey:"cart_count_status")
        newEntity.setValue(cart_replen_opt,forKey:"cart_replen_opt")
        newEntity.setValue(compartment,forKey:"compartment")
        newEntity.setValue(count_order,forKey:"count_order")
        newEntity.setValue(count_required,forKey:"count_required")
        newEntity.setValue(defaultMessage,forKey:"defaultMessage")
        newEntity.setValue(descr60,forKey:"descr60")
        newEntity.setValue(include_flg,forKey:"include_flg")
        newEntity.setValue(inv_cart_id,forKey:"inv_cart_id")
        newEntity.setValue(inv_item_id,forKey:"inv_item_id")
        newEntity.setValue(last_dttm_update,forKey:"last_dttm_update")
        newEntity.setValue(last_oprid,forKey:"last_oprid")
        newEntity.setValue(mfg_itm_id,forKey:"mfg_itm_id")
        newEntity.setValue(qty_maximum,forKey:"qty_maximum")
        newEntity.setValue(qty_optimal,forKey:"qty_optimal")
        newEntity.setValue(sufficient_stock,forKey:"sufficient_stock")
        newEntity.setValue(unit_of_measure,forKey:"unit_of_measure")
        newEntity.setValue(transfer_cost,forKey:"transfer_cost")
        newEntity.setValue(price_markup_pct,forKey:"price_markup_pct")
        newEntity.setValue(charge_code,forKey:"charge_code")
        newEntity.setValue(process_instance,forKey:"process_instance")
        newEntity.setValue(transfer_calc_type,forKey:"transfer_calc_type")
        
        
        do{
            try context.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    
    public func GetBUCartItemsHistoryServiceFromXML(data: Data)-> PS_CART_CT_INF_INV
    {
        
        let xmlToParse   = String.init(data: data, encoding: String.Encoding.utf8)!
        // print(xmlToParse)
        return GetBUCartItemsHistoryServiceFromXMLString( xmlToParse : xmlToParse)
    }
    
    public func GetBUINVCartIDHistoryService(UserID:String,Password:String,businessUnit:String,CartID:String,completion :@escaping ()->Void ){
        
        //
        //        <soapenv:Body>
        //        <Get__CompIntfc__CART_CT_INF_INV_CI>
        //        <BUSINESS_UNIT>US015</BUSINESS_UNIT>
        //        <INV_CART_ID>COR-1</INV_CART_ID>
        //        </Get__CompIntfc__CART_CT_INF_INV_CI>
        //        </soapenv:Body>
        
        
        //  var soapReqXML:String = "<?xml version=\"1.0\"?>"
          var soapReqXML:String = "<soapenv:Envelope xmlns=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "<soapenv:Header xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "</soapenv:Header>"
        soapReqXML  += "<soapenv:Body xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<Get__CompIntfc__CART_CT_INF_INV_CI>"
        soapReqXML  += "<BUSINESS_UNIT>"
        soapReqXML  += businessUnit
        soapReqXML  += "</BUSINESS_UNIT>"
        soapReqXML  += "<INV_CART_ID>"
        soapReqXML  += CartID
        soapReqXML  += "</INV_CART_ID>"
        soapReqXML  += "</Get__CompIntfc__CART_CT_INF_INV_CI>"
        soapReqXML  += "</soapenv:Body>"
        soapReqXML  += "</soapenv:Envelope>"
        
        
        //print(soapReqXML)
        SoapHttpClient.callWSAsync(Host: urlclass.Host, WebServiceUrl: urlclass.Url_CI_CART_CT_INF_INV, SoapAction: urlclass.soapAction_CI_CART_CT_INF_INV_CI_G, SoapMessage: soapReqXML) { (err, responseData, str) in
            // print("Errorerr = \(err)")
            
            _ = self.GetBUCartItemsHistoryServiceFromXML(data : responseData!)
            completion()
            
        }
        
    }
    
}

