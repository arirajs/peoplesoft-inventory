//
//  UrlDefinations.swift
//  PeopleSoft Inventory 2018 V1
//
//  Created by Ariraj Shanmugavelu on 4/26/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import Foundation
import CoreData

class Urldefinations {
    
    var hostname:String = ""
    
    public var Host:String = ""
    public var Url_CI_USERMAINT_SELF:String = ""
    public var soapAction_UserMaint = ""
    
    public var Url_CI_OPR_DEFAULT_FIN:String = ""
    public var soapAction_CI_OPR_DEFAULT_FIN_G = ""
    
    public var Url_CI_CART_COUNT_INV:String = ""
    public var soapAction_CI_CART_COUNT_INV_F = ""
    public var soapAction_CI_CART_COUNT_INV_G = ""
    public var soapAction_CI_ZZ_CART_COUNT_INV_UD = ""
    
    public var Url_QAS_QRY_SERVICE = ""
    public var soapAction_QAS_EXECUTEQRYSYNC_OPER = ""
    
    // For 30 days History
    public var Url_CI_CART_CT_INF_INV:String = ""
    public var soapAction_CI_CART_CT_INF_INV_CI_G = ""
    
    init() {
        
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //Prepare the request of type NSFetchRequest  for the entity
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_HOST_DETAILS")
        
         do {
            let result = try managedContext.fetch(fetchRequest)
            
            for data in result as! [NSManagedObject] {
                hostname = (data.value(forKey: "host") as! String)
            }
 
        //   SCLAlertView().showInfo("Important", subTitle: "User ID or Password misssing")
 
            Host = self.hostname
            Url_CI_USERMAINT_SELF =  Host + "/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_USERMAINT_SELF.1.wsdl"
            soapAction_UserMaint = "CI_USERMAINT_SELF_F.V1"
            
            Url_CI_OPR_DEFAULT_FIN = Host + "/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_OPR_DEFAULT_FIN.1.wsdl"
            soapAction_CI_OPR_DEFAULT_FIN_G = "CI_OPR_DEFAULT_FIN_G.V1"
            
            Url_CI_CART_COUNT_INV = Host + "/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_CART_COUNT_INV.1.wsdl"
            soapAction_CI_CART_COUNT_INV_F = "CI_CART_COUNT_INV_F.V1"
            soapAction_CI_CART_COUNT_INV_G = "CI_CART_COUNT_INV_G.V1"
            soapAction_CI_ZZ_CART_COUNT_INV_UD = "CI_CART_COUNT_INV_UD.V1"
            
            
            Url_QAS_QRY_SERVICE = Host + "/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/QAS_QRY_SERVICE.1.wsdl"
            soapAction_QAS_EXECUTEQRYSYNC_OPER = "QAS_EXECUTEQRYSYNC.VERSION_1"
            
            // For 30 days History
            Url_CI_CART_CT_INF_INV = Host + "/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_CART_CT_INF_INV_CI.1.wsdl"
            soapAction_CI_CART_CT_INF_INV_CI_G = "CI_CART_CT_INF_INV_CI_G.V1"
            // print("Url_CI_USERMAINT_SELF = \(Url_CI_USERMAINT_SELF)")
        } catch {
            print("Failed @ Urldefinations")
        }
    }
}






//
//class Urldefinations {
//
//
////    public var Host:String = "fscm92.ps.com:8000"
////    public var Url_CI_USERMAINT_SELF:String = "http://fscm92.ps.com:8000/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_USERMAINT_SELF.1.wsdl"
////    public var Url_CI_OPR_DEFAULT_FIN:String = "http://fscm92.ps.com:8000/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_OPR_DEFAULT_FIN.1.wsdl"
////    public var Url_CI_CART_COUNT_INV:String = "http://fscm92.ps.com:8000/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_CART_COUNT_INV.1.wsdl"
//
////    public var Host:String = "fscm90.ps.com:8080"
////    public var Url_CI_USERMAINT_SELF:String = "http://fscm90.ps.com:8080/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_USERMAINT_SELF.1.wsdl"
////    public var Url_CI_OPR_DEFAULT_FIN:String = "http://fscm90.ps.com:8080/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_OPR_DEFAULT_FIN.1.wsdl"
////    public var Url_CI_CART_COUNT_INV:String = "http://fscm90.ps.com:8080/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_ZZ_CART_COUNT_INV.1.wsdl"
//
//
////    public var Host:String = "192.168.1.28:8080"
////    public var Url_CI_USERMAINT_SELF:String = "http://192.168.1.28:8080/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_USERMAINT_SELF.1.wsdl"
////    public var soapAction_UserMaint = "CI_USERMAINT_SELF_F.V1"
////
////    public var Url_CI_OPR_DEFAULT_FIN:String = "http://192.168.1.28:8080/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_OPR_DEFAULT_FIN.1.wsdl"
////    public var soapAction_CI_OPR_DEFAULT_FIN_G = "CI_OPR_DEFAULT_FIN_G.V1"
////
////    public var Url_CI_CART_COUNT_INV:String = "http://192.168.1.28:8080/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_MCG_CART_COUNT_INV.1.wsdl"
////    public var soapAction_CI_CART_COUNT_INV_F = "CI_MCG_CART_COUNT_INV_F.V1"
////    public var soapAction_CI_CART_COUNT_INV_G = "CI_MCG_CART_COUNT_INV_G.V1"
////    public var soapAction_CI_ZZ_CART_COUNT_INV_UD = "CI_MCG_CART_COUNT_INV_UD.V1"
////
////    // For 30 days History
////    public var Url_CI_CART_CT_INF_INV:String = "http://192.168.1.28:8080/PSIGW/PeopleSoftServiceListeningConnector/CI_CART_CT_INF_INV_CI.1.wsdl"
////    public var soapAction_CI_CART_CT_INF_INV_CI_G = "CI_CART_CT_INF_INV_CI_G.V1"
//
//
////    public var soapAction_UserMaint = "CI_USERMAINT_SELF_F.V1"
////    public var soapAction_CI_OPR_DEFAULT_FIN_G = "CI_OPR_DEFAULT_FIN_G.V1"
////    public var soapAction_CI_CART_COUNT_INV_F = "CI_MCG_CART_COUNT_INV_F.V1"
////    public var soapAction_CI_CART_COUNT_INV_G = "CI_MCG_CART_COUNT_INV_G.V1"
////    public var soapAction_CI_ZZ_CART_COUNT_INV_UD = "CI_MCG_CART_COUNT_INV_UD.V1"
////    public var Host:String = "psoftweb2.holyredeemer.local:8080"
////    public var Url_CI_USERMAINT_SELF:String = "http://psoftweb2.holyredeemer.local:8080/PSIGW/PeopleSoftServiceListeningConnector/CI_USERMAINT_SELF.1.wsdl"
////    public var Url_CI_OPR_DEFAULT_FIN:String = "http://psoftweb2.holyredeemer.local:8080/PSIGW/PeopleSoftServiceListeningConnector/CI_OPR_DEFAULT_FIN.1.wsdl"
////    public var Url_CI_CART_COUNT_INV:String = "http://psoftweb2.holyredeemer.local:8080/PSIGW/PeopleSoftServiceListeningConnector/CI_MCG_CART_COUNT_INV.1.wsdl"
//
//
//   /*
//    public var Host:String = "oc-129-158-69-230.compute.oraclecloud.com:8000"
//    public var Url_CI_USERMAINT_SELF:String = "http://oc-129-158-69-230.compute.oraclecloud.com:8000/PSIGW/PeopleSoftServiceListeningConnector/PS_YR3WWXTIQFES/CI_USERMAINT_SELF.1.wsdl"
//    public var soapAction_UserMaint = "CI_USERMAINT_SELF_F.V1"
//
//    public var Url_CI_OPR_DEFAULT_FIN:String = "http://oc-129-158-69-230.compute.oraclecloud.com:8000/PSIGW/PeopleSoftServiceListeningConnector/PS_YR3WWXTIQFES/CI_OPR_DEFAULT_FIN.1.wsdl"
//    public var soapAction_CI_OPR_DEFAULT_FIN_G = "CI_OPR_DEFAULT_FIN_G.V1"
//
//    public var Url_CI_CART_COUNT_INV:String = "http://oc-129-158-69-230.compute.oraclecloud.com:8000/PSIGW/PeopleSoftServiceListeningConnector/PS_YR3WWXTIQFES/CI_CART_COUNT_INV.1.wsdl"
//    public var soapAction_CI_CART_COUNT_INV_F = "CI_CART_COUNT_INV_F.V1"
//    public var soapAction_CI_CART_COUNT_INV_G = "CI_CART_COUNT_INV_G.V1"
//    public var soapAction_CI_ZZ_CART_COUNT_INV_UD = "CI_CART_COUNT_INV_UD.V1"
//
//        // For 30 days History
//    public var Url_CI_CART_CT_INF_INV:String = "http://oc-129-158-69-230.compute.oraclecloud.com:8000/PSIGW/PeopleSoftServiceListeningConnector/CI_CART_CT_INF_INV_CI.1.wsdl"
//    public var soapAction_CI_CART_CT_INF_INV_CI_G = "CI_CART_CT_INF_INV_CI_G.V1"
//
//
//
//    public var Host:String = "http://psoft92web2-tst.holyredeemer.local:8180"
//    public var Url_CI_USERMAINT_SELF:String = "http://psoft92web2-tst.holyredeemer.local:8180/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_USERMAINT_SELF.1.wsdl"
//    public var soapAction_UserMaint = "CI_USERMAINT_SELF_F.V1"
//
//    public var Url_CI_OPR_DEFAULT_FIN:String = "http://psoft92web2-tst.holyredeemer.local:8180/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_OPR_DEFAULT_FIN.1.wsdl"
//    public var soapAction_CI_OPR_DEFAULT_FIN_G = "CI_OPR_DEFAULT_FIN_G.V1"
//
//    public var Url_CI_CART_COUNT_INV:String = "http://psoft92web2-tst.holyredeemer.local:8180/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_CART_COUNT_INV.1.wsdl"
//    public var soapAction_CI_CART_COUNT_INV_F = "CI_CART_COUNT_INV_F.V1"
//    public var soapAction_CI_CART_COUNT_INV_G = "CI_CART_COUNT_INV_G.V1"
//    public var soapAction_CI_ZZ_CART_COUNT_INV_UD = "CI_CART_COUNT_INV_UD.V1"
//
//        // For 30 days History
//    public var Url_CI_CART_CT_INF_INV:String = "http://psoft92web2-tst.holyredeemer.local:8180/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_CART_CT_INF_INV_CI.1.wsdl"
//    public var soapAction_CI_CART_CT_INF_INV_CI_G = "CI_CART_CT_INF_INV_CI_G.V1"
//     */
//
//
//    public var Host:String = "http://psoft92web1-tst.holyredeemer.local:8080"
//    public var Url_CI_USERMAINT_SELF:String = "http://psoft92web1-tst.holyredeemer.local:8080/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_USERMAINT_SELF.1.wsdl"
//    public var soapAction_UserMaint = "CI_USERMAINT_SELF_F.V1"
//
//    public var Url_CI_OPR_DEFAULT_FIN:String = "http://psoft92web1-tst.holyredeemer.local:8080/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_OPR_DEFAULT_FIN.1.wsdl"
//    public var soapAction_CI_OPR_DEFAULT_FIN_G = "CI_OPR_DEFAULT_FIN_G.V1"
//
//    public var Url_CI_CART_COUNT_INV:String = "http://psoft92web1-tst.holyredeemer.local:8080/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_CART_COUNT_INV.1.wsdl"
//    public var soapAction_CI_CART_COUNT_INV_F = "CI_CART_COUNT_INV_F.V1"
//    public var soapAction_CI_CART_COUNT_INV_G = "CI_CART_COUNT_INV_G.V1"
//    public var soapAction_CI_ZZ_CART_COUNT_INV_UD = "CI_CART_COUNT_INV_UD.V1"
//
//        // For 30 days History
//    public var Url_CI_CART_CT_INF_INV:String = "http://psoft92web1-tst.holyredeemer.local:8080/PSIGW/PeopleSoftServiceListeningConnector/PSFT_EP/CI_CART_CT_INF_INV_CI.1.wsdl"
//    public var soapAction_CI_CART_CT_INF_INV_CI_G = "CI_CART_CT_INF_INV_CI_G.V1"
//  https://peoplesoft-prod.holyredeemer.com
////   func retrieveDatafrom() {
////        //As we know that container is set up in the AppDelegates so we need to refer that container.
////    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
////
////        //We need to create a context from this container
////        let managedContext = appDelegate.persistentContainer.viewContext
////
////        //Prepare the request of type NSFetchRequest  for the entity
////        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PS_HOST_DETAILS")
////
////
////        do {
////            let result = try managedContext.fetch(fetchRequest)
////
////            for data in result as! [NSManagedObject] {
////              public  var  current_Host = (data.value(forKey: "host") as! String)
////            }
////
////        } catch {
////
////            print("Failed")
////        }
////
////  }
//
//}
//
