//
//  AuthWebServices.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 11/3/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import Foundation
import CoreData
import UIKit

public class AuthWebServices {
    
//    deinit {
//        print("deinit - AuthWebServices")
//    }
    
    let urlclass = Urldefinations()
    var ErrorString:String = ""
    var ErrorFound:String = ""
    public func UserAuthFromXMLString(xmlToParse:String , UserID:String,password:String)->PSOPRDEFN {
        
        let xml = SWXMLHash.lazy(xmlToParse)
        let xmlResponse: XMLIndexer? = xml.children.first?.children.first
        let xmlResult0: XMLIndexer?  = xmlResponse?.children.last
        var strVal = ""
        var elemName = ""
        
 
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
   
        let context = appDelegate.persistentContainer.viewContext
        
        
        let returnValue = PSOPRDEFN(context: context)
        
        if elemName == "" {
            // Property name :
            let itemCount1: Int = (xmlResult0?.children.count)!
            for i1 in 0 ..< itemCount1 {
                let xmlResult1:XMLIndexer? = xmlResult0?.children[i1]
                let elem1: XMLElement? =  xmlResult1!.element
                strVal = ""
                if elem1?.children.first is TextElement {
                    let elemText1:TextElement = elem1?.children.first as! TextElement
                    strVal = elemText1.text
                }
                elemName = elem1!.name
                
                if elemName == "faultcode"{
                    // Future Code for Validating fault flag
                }
                // print("\(elemName) = \(strVal)")
                // --------------
                if elemName == "detail" {
                    // Array Property For returnValue.CurrencyList
                    let itemCount2: Int = (xmlResult1?.children.count)!
                    for i2 in 0 ..< itemCount2 {
                        let xmlResult_Parent2:XMLIndexer? = xmlResult1?.children[i2]
                        let childCount2 :Int = (xmlResult_Parent2?.children.count)!
                        for j2 in 0 ..< childCount2 {
                            
                            let xmlResult2: XMLIndexer? =  xmlResult_Parent2?.children[j2]
                            let elem2: XMLElement? =  xmlResult2?.element
                            strVal = ""
                            if elem2?.children.first is TextElement {
                                let elemText:TextElement = elem2?.children.first as! TextElement
                                strVal = elemText.text
                            }
                            elemName = elem2!.name
                           // print("\(elemName) = \(strVal)")
                            if elemName == "DefaultMessage" {
                                ErrorString = strVal
                                returnValue.oprid =  UserID
                                returnValue.pwd = password
                                returnValue.defaultMessage = strVal
                            }
                         }
                    }
                }
                
                
                if elemName == "USERMAINT_SELF" {
                    
                    // Array Property For returnValue.CurrencyList
                    let itemCount21: Int = (xmlResult1?.children.count)!
                    for ii2 in 0 ..< itemCount21 {
                        
                        let xmlResult_Parent21:XMLIndexer? = xmlResult1?.children[ii2]
                        
                        let elem21: XMLElement? =  xmlResult_Parent21?.element
                        strVal = ""
                        if elem21?.children.first is TextElement {
                            let elemText:TextElement = elem21?.children.first as! TextElement
                            strVal = elemText.text
                        }
                        elemName = elem21!.name
                        //print("\(elemName) = \(strVal)")
                        
                        // --------------
                        if elemName == "OPRID" {
                            returnValue.oprid =  strVal
                            returnValue.pwd = password
                        }
                        else if elemName == "OPRDEFNDESC" {
                            returnValue.oprdefndesc = strVal
                        }
                        
                    }
                }
            }
        }
        
        DispatchQueue.main.async { do {
            print("Saved1...!")
            try context.save()
        } catch {
            let nserror = error as NSError
            print("Unresolved error \(nserror), \(nserror.userInfo)")
            // print("Failed saving")
            } }
        
        return returnValue
    }
    
    public func UserAuthFromXML(data: Data, UserID:String,password:String)-> PSOPRDEFN {
        
        let xmlToParse   = String.init(data: data, encoding: String.Encoding.utf8)!
        return UserAuthFromXMLString( xmlToParse : xmlToParse , UserID: UserID,password: password)
    }
    
    
    //    static func readUser(oprid:String,pwd:String, completion:@escaping () -> ()){
    //
    //        if LoginData.loginModel.count == 0{
    //            // _ = AuthWebServices().GetUserProfileDefault(UserID: oprid, Password: pwd)
    //
    //        }
    //        completion()
    //    }
    //
    
    
    // public func GetUserProfileDefault(UserID:String,Password:String, completion :@escaping (String)->Void) //-> PSOPRDEFN
    
    public func GetUserProfileDefault(UserID:String,Password:String) -> PSOPRDEFN
    {
        
        var soapReqXML:String = "<?xml version=\"1.0\"?>"
        soapReqXML  += "<soapenv:Envelope xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "<soapenv:Header xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "</soapenv:Header>"
        soapReqXML  += "<soapenv:Body xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<Find__CompIntfc__USERMAINT_SELF xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/M974628858.V1\">"
        soapReqXML  += "<OPRID>"
        soapReqXML  += UserID
        soapReqXML  += "</OPRID>"
        soapReqXML  += "</Find__CompIntfc__USERMAINT_SELF>"
        soapReqXML  += "</soapenv:Body>"
        soapReqXML  += "</soapenv:Envelope>"
        
        //        SoapHttpClient.callWSAsync(Host: urlclass.Host , WebServiceUrl: urlclass.Url_CI_USERMAINT_SELF, SoapAction: urlclass.soapAction_UserMaint, SoapMessage: soapReqXML) { (err, responseData, str) in
        //
        //            //                        print("======raj========")
        //            //                        print("err = \(err)")
        //            //                        print("responseData = \(responseData)")
        //            //                        print("str = \(str)")
        //            //                        print("======raj========")
        //
        //            if err == nil{
        //
        //                _ = self.UserAuthFromXML(data: responseData!, password: Password)
        //                DispatchQueue.main.async {
        //
        //                    completion("No Error")
        //                }
        //            }
        //            DispatchQueue.main.async {
        //
        //                completion(err?.localizedDescription ?? "No Error")
        //            }
        //        }
        
        let responseData:Data = SoapHttpClient.callWS(Host: urlclass.Host, WebServiceUrl: urlclass.Url_CI_USERMAINT_SELF, SoapAction: urlclass.soapAction_UserMaint, SoapMessage: soapReqXML)
        //print(responseData)
        
        let returnValue:PSOPRDEFN = self.UserAuthFromXML(data: responseData, UserID: UserID,password: Password)
        
       // print("2")
        return returnValue
    }
    
    public func GetUserProfileDefault(UserID:String,Password:String, completion :@escaping (String,String)->Void) //-> PSOPRDEFN
        
    {
        
        var soapReqXML:String = "<?xml version=\"1.0\"?>"
        soapReqXML  += "<soapenv:Envelope xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2003/03/addressing/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\">"
        soapReqXML  += "<soapenv:Header xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<wsse:Security soap:mustUnderstand=\"1\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
        soapReqXML  += "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
        soapReqXML  += "<wsse:Username>"
        soapReqXML  += UserID
        soapReqXML  += "</wsse:Username>"
        soapReqXML  += "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
        soapReqXML  += Password
        soapReqXML  += "</wsse:Password>"
        soapReqXML  += "</wsse:UsernameToken>"
        soapReqXML  += "</wsse:Security>"
        soapReqXML  += "</soapenv:Header>"
        soapReqXML  += "<soapenv:Body xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
        soapReqXML  += "<Find__CompIntfc__USERMAINT_SELF xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/schemas/M974628858.V1\">"
        soapReqXML  += "<OPRID>"
        soapReqXML  += UserID
        soapReqXML  += "</OPRID>"
        soapReqXML  += "</Find__CompIntfc__USERMAINT_SELF>"
        soapReqXML  += "</soapenv:Body>"
        soapReqXML  += "</soapenv:Envelope>"
        
        SoapHttpClient.callWSAsync(Host: urlclass.Host , WebServiceUrl: urlclass.Url_CI_USERMAINT_SELF, SoapAction: urlclass.soapAction_UserMaint, SoapMessage: soapReqXML) { (err, responseData, str) in
            
//                                    print("======raj========")
//                                    print("err = \(err)")
//                                    print("responseData = \(responseData)")
//                                    print("str = \(str)")
//                                    print("======raj========")
            
            
            
            
            if err == nil {
                //pprint(responseData!)
                _ = self.UserAuthFromXML(data: responseData!, UserID: UserID,password: Password)
                DispatchQueue.main.async {
                    completion(self.ErrorString,self.ErrorString)
                }
            }else{
                DispatchQueue.main.async {
                    completion(err?.localizedDescription ?? "No Error","Error")
                }
            }
        }
    }
}
