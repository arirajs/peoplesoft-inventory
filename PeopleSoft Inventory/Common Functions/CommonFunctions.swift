//
//  CommonFunctions.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 9/7/20.
//  Copyright © 2020 Ariraj Shanmugavelu. All rights reserved.
//

import Foundation
import CoreData

func fetchOPRIDDefaults() -> [PSOPRDEFN]?
   {
       let conttext = CoreDataHelper().getContext()
       let fetchRequest:NSFetchRequest<PSOPRDEFN> = PSOPRDEFN.fetchRequest()
       var psoprdefn:[PSOPRDEFN]? = nil
       
       do{
           psoprdefn = try conttext.fetch(fetchRequest)
           return psoprdefn
       }catch{
           return psoprdefn
       }
   }

extension Date {

 static func getCurrentDate() -> String {

        let dateFormatter = DateFormatter()

       dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
     //   dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm"
        return dateFormatter.string(from: Date())

    }
}

extension String
{
    func toDateString( inputDateFormat inputFormat  : String,  ouputDateFormat outputFormat  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = outputFormat
        return dateFormatter.string(from: date!)
    }
    
    
}
