//
//  LoginModel.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 10/21/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import Foundation

class LoginModel {
    var userid:String!
    var pwd:String!
    var defaultMessage:String!
    
    init(userid:String,pwd:String,defaultMessage:String) {
        self.userid = userid
        self.pwd = pwd
        self.defaultMessage = defaultMessage
    }
}
