
//
//  Theme.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 10/23/18.
//  Copyright © 2018 Ariraj Shanmugavelu. All rights reserved.
//

import UIKit

class Theme{
    static let mainFontName = ""
    static let accent = UIColor(named: "Accent")
    static let background = UIColor(named: "Backgroung")
    static let tint = UIColor(named: "Tint")
}
