//
//  PS_CART_COUNT_H_HIST+CoreDataProperties.swift
//  PeopleSoft Inventory
//
//  Created by Ariraj Shanmugavelu on 1/26/21.
//  Copyright © 2021 Ariraj Shanmugavelu. All rights reserved.
//
//

import Foundation
import CoreData


extension PS_CART_COUNT_H_HIST {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PS_CART_COUNT_H_HIST> {
        return NSFetchRequest<PS_CART_COUNT_H_HIST>(entityName: "PS_CART_COUNT_H_HIST")
    }

    @NSManaged public var business_unit: String?
    @NSManaged public var cart_count_id: String?
    @NSManaged public var demand_date: String?
    @NSManaged public var inv_cart_id: String?
    @NSManaged public var last_oprid: String?

}
